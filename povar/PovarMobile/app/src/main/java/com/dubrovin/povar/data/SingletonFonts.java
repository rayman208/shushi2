package com.dubrovin.povar.data;

import android.content.Context;
import android.graphics.Typeface;

/**
 * Created by Dubrovin on 19.12.2017.
 */

public class SingletonFonts {

    private static Typeface font;
    private static volatile SingletonFonts instance;

    private SingletonFonts() {}

    public static SingletonFonts getInstance(Context activity) {
        SingletonFonts localInstance = instance;
        if (localInstance == null) {
            synchronized (SingletonFonts.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new SingletonFonts();
                }
            }
            setFont(Typeface.createFromAsset(activity.getAssets(), "fonts/Font.ttf"));

        }
        return localInstance;
    }

    public Typeface getFont() {
        return font;
    }

    public static void setFont(Typeface font) {
        SingletonFonts.font = font;
    }
}