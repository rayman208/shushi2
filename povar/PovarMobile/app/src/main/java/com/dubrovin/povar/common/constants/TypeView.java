package com.dubrovin.povar.common.constants;

/**
 * Created by Dubrovin on 17.12.2017.
 */

public enum TypeView {
    ORDERS(1),PRODUCT(2),ORDER(3);

    int value;

    TypeView(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }
}
