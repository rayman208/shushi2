package com.dubrovin.povar.common.constants;

/**
 * Created by NikDubrovin on 12/17/2017.
 */

public class Commands {

    // USERS

    // enter in system
    public static final String USERS_SELECT_BY_LOGIN_PASSWORD = "users.select.by_login_password";

    /**
     *  finish work - users.online = 0
     */
    public static final String USERS_UPDATE_FINISH_WORK = "users.update.finish_work";

    /**
     *  start work - users.online = 1
     */
    public static final String USERS_UPDATE_START_WORK = "users.update.start_work";

    //return points of work
    public static final String POINTS_WORK_SELECT_ALL = "points_work.select.all";

    //////////////
    // POINTS_WORK && PRODUCTS

    //return orders
    public static final String ORDERS_SELECT_POVAR_ORDERS = "orders.select.povar_orders";

    //add new order to queue
    public static final String POVAR_WORK_INSERT_ADD_ORDER_TO_QUEUE = "povars_work.insert.add_order_to_queue";

    //return id products for order
    public static final String ORDER_PRODUCTS_SELECT_BY_ID_ORDER = "order_products.select.by_id_order";

    //return list of products
    public static final String PRODUCTS_SELECT_ALL = "products.select.all";

    //////////////
    // POVARS_WORK

    //start new order
    public static final String POVARS_WORK_UPDATE_ORDER_START = "povars_work.update.order_start";

    public static final String POVARS_WORK_UPDATE_ORDER_FINISH = "povars_work.update.order_finish";

    ////////////
    // ORDERS

    /**
     *  update `on_work` = 1
     */
    public static final String ORDERS_UPDATE_ON_WORK_START = "orders.update.on_work_start";

    /**
     *  set `on_work` = 0 <p>
     *  set `id_order_state` = 4,
     */
    public static final String ORDERS_UPDATE_FINISH_ORDER = "orders.update.finish_order";

    public static final String ORDERS_UPDATE_MOVE_ORDER_TO_UPAKOV = "orders.update.move_order_to_upakov";

    // UPAKOVS_WORK
    //start new order
    public static final String UPAKOVS_WORK_UPDATE_ORDER_START = "upakovs_work.update.order_start";

    public static final String UPAKOVS_WORK_INSERT_ADD_ORDER_TO_QUEUE = "upakovs_work.insert.add_order_to_queue";

    //finish working order
    public static final String UPAKOVS_WORK_UPDATE_ORDER_FINISH = "upakovs_work.update.order_finish";

    //add order to kurer
    public static final String ORDERS_UPDATE_MOVE_ORDER_TO_KURER = "orders.update.move_order_to_kurer";
}
