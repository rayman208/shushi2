package com.dubrovin.povar.ui.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.dd.processbutton.FlatButton;
import com.dubrovin.povar.R;
import com.dubrovin.povar.common.config.MainConfig;
import com.dubrovin.povar.common.constants.Commands;
import com.dubrovin.povar.common.constants.Constants;
import com.dubrovin.povar.common.utils.GLog;
import com.dubrovin.povar.common.utils.NetworkUtil;
import com.dubrovin.povar.common.utils.OutUtils;
import com.dubrovin.povar.common.utils.VersionUtil;
import com.dubrovin.povar.data.StorageClass;
import com.dubrovin.povar.data.models.PointWork;
import com.dubrovin.povar.data.models.Product;
import com.dubrovin.povar.data.models.Status;
import com.dubrovin.povar.data.models.User;
import com.dubrovin.povar.data.network.RetrofitSushi;
import com.wang.avi.AVLoadingIndicatorView;

import org.angmarch.views.NiceSpinner;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.annotation.ElementType;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AuthActivity extends AppCompatActivity {
    // View
    private EditText mUsername;
    private EditText mPassword;
    private FlatButton mButtonSignIn;
    private FlatButton mButtonCloseApp;
    private NiceSpinner niceSpinner;
    private AVLoadingIndicatorView mAVLoadingIndicatorViewSign;
    private AVLoadingIndicatorView mAVLoadingIndicatorViewSpinner;
    private TextView SpinnerTextView;

    // Other
    private StorageClass mStorageClass = StorageClass.getInstance();
    private List<PointWork> mPointWorks;
    private int IdChoosePointWork = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);

        // info
        startProject();

        //UI
        loadUI();

        int getPointWorksReq = getPointsWork();

        if(getPointWorksReq != 1){
            Toasty.error(getApplication(), Constants.ERROR_POINT_WORKS, Toast.LENGTH_SHORT, true).show();
             getPointsWork();
        }
    }

    private void loadUI() {
        GLog.i("onCreateView AuthFragment");
        mStorageClass.setState("auth");

        // reference on view
        niceSpinner = (NiceSpinner) findViewById(R.id.spinner_points_work);
        mUsername = (EditText) findViewById(R.id.username);
        mPassword = (EditText) findViewById(R.id.password);
        mButtonSignIn = findViewById(R.id.button_enter);
        mButtonCloseApp = findViewById(R.id.button_closeapp);
        final TextView UserNameTextView = findViewById(R.id.fragment_auth_error_user_text_view);
        final TextView PasswordTextView = findViewById(R.id.fragment_auth_error_password_text_view);
        SpinnerTextView = findViewById(R.id.activity_auth_error_spinner_point_work_text_view);
        mAVLoadingIndicatorViewSign = findViewById(R.id.activity_auth_loader_sign);
        mAVLoadingIndicatorViewSpinner = findViewById(R.id.activity_auth_loader_spinner);

        // Listeners
        niceSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                IdChoosePointWork = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });

        mButtonCloseApp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                android.os.Process.killProcess(android.os.Process.myPid());
            }
        });

        mButtonSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String username = mUsername.getText().toString();
                String pass = mPassword.getText().toString();

                if (OutUtils.isStrNull(username))
                    UserNameTextView.setVisibility(View.VISIBLE);
                else
                    UserNameTextView.setVisibility(View.GONE);

                if (OutUtils.isStrNull(pass))
                    PasswordTextView.setVisibility(View.VISIBLE);
                else
                    PasswordTextView.setVisibility(View.GONE);

                if(mPointWorks == null)
                    SpinnerTextView.setVisibility(View.VISIBLE);
                else
                    SpinnerTextView.setVisibility(View.GONE);


                if(NetworkUtil.isNetworkAvailable(getApplication())) {
                    if (!TextUtils.isEmpty(username) && !TextUtils.isEmpty(pass))
                        signIn(username, pass);
                    else
                        Toasty.error(getApplication(), "логин или пароль не введён", Toast.LENGTH_SHORT, true).show();
                }
                else
                    Toasty.error(getApplication(), Constants.ERROR_NET_WORK, Toast.LENGTH_SHORT, true).show();

            }
        });
    }

    //region Klava utils
    public static void hideKeyboard(Context context) {
        try {
            ((Activity) context).getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
            if ((((Activity) context).getCurrentFocus() != null) && (((Activity) context).getCurrentFocus().getWindowToken() != null)) {
                ((InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(((Activity) context).getCurrentFocus().getWindowToken(), 0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void showKeyboard(Context context) {
        ((InputMethodManager) (context).getSystemService(Context.INPUT_METHOD_SERVICE)).toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
    }
    // endregion

    /**
     * get list of working points
     * @see StorageClass list pointworks
     */
    private int getPointsWork() {
        loaderSpinnerShow();
        SpinnerTextView.setVisibility(View.GONE);
//        RetrofitSushi.getApi().getPointsWork(Commands.POINTS_WORK_SELECT_ALL, MainConfig.KEY).enqueue(new Callback<List<PointWork>>() {
//            @Override
//            public void onResponse(Call<List<PointWork>> call, Response<List<PointWork>> response) {
//                try {
//                    loaderSpinnerHide();
//                    niceSpinner.attachDataSource(OutUtils.getListNameSpinner(response.body()));
//                    mPointWorks = response.body();
//                    SpinnerTextView.setVisibility(View.GONE);
//                }
//                catch (Exception e) {
//                    loaderSpinnerHide();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<List<PointWork>> call, Throwable t) {
//                t.printStackTrace();
//                if (NetworkUtil.isNetworkAvailable(getApplication()))
//                    Toasty.error(getApplication(), Constants.ERROR_NET_WORK, Toast.LENGTH_SHORT, true).show();
//            }
//        });

        if (!NetworkUtil.isNetworkAvailable(getApplication())) {
            Toasty.error(getApplication(), Constants.ERROR_NET_WORK, Toast.LENGTH_SHORT, true).show();
            loaderSpinnerHide();
            return 0;
        }
        else {
            LoadPointWorks lpw = new LoadPointWorks();
            List<PointWork> pws = null;
            try {
                lpw.execute();
                pws = lpw.get();
                if (pws != null) {
                    if (pws.get(0).getName().equals("test_point_work_asysns_task_error")) {
                        Toasty.error(getApplication(), "ошибка при загрузке точек работы! Перезапустите приложение!", Toast.LENGTH_SHORT, true).show();
                        loaderSpinnerHide();
                    } else {
                        loaderSpinnerHide();
                        niceSpinner.attachDataSource(OutUtils.getListNameSpinner(pws));
                        mPointWorks = pws;
                        SpinnerTextView.setVisibility(View.GONE);
                        return 1;
                    }
                }
                loaderSignHide();
                } catch (InterruptedException | ExecutionException e){
                e.printStackTrace();
                Toasty.error(getApplication(), "ошибка при загрузке точек работы! Перезапустите приложение!", Toast.LENGTH_SHORT, true).show();
                loaderSpinnerHide();
                return 0;
            }
        }
        return 0;
    }

    /**
     * Sign in system :
     * if (pass && login == true) - server return User <p>else
     * server return []
     * <ol>
     * <li> if(mPointWorks == null) - getPintWork()</li>
     * <li> save ChoosePointsWork </li>
     * <li> get Products </li>
     * <li> save User && Toast </li>
     * <li> start CurrentOrdersActivity </li>
     * </ol>
     * @param Login
     * @param Password
     */
    private void signIn(String Login, String Password) {

        //region create jsonObject [login] [password]
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("login", Login)
                    .put("password", Password);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        //endregion

        loaderSignShow();

        RetrofitSushi.getApi().signIn(Commands.USERS_SELECT_BY_LOGIN_PASSWORD, jsonObject, MainConfig.KEY).enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                loaderSignHide();
                if (response.body() != null) {
                    try {
                        if(mPointWorks == null) {
                            // get points work
                            int getPointWorksReq = getPointsWork();

                            if (getPointWorksReq != 1) {
                                Toasty.error(getApplication(), Constants.ERROR_POINT_WORKS, Toast.LENGTH_SHORT, true).show();
                                Toasty.error(getApplication(), Constants.ERROR_SIGN_IN, Toast.LENGTH_SHORT, true).show();
                                // getPointsWork();
                            }
                        }
                        else {
                            // getProducts();
                            int getProducts = getProducts();
                            if (getProducts == 1) {
                                IdChoosePointWork = niceSpinner.getSelectedIndex();
                                mStorageClass.setChoosePointWork(mPointWorks.get(IdChoosePointWork));

                                Toasty.success(getApplication(), Constants.SUCCESS_SIGN_IN, Toast.LENGTH_SHORT, true).show();
                                mStorageClass.setUser(response.body());
                                startWorkUser(response.body().getId(), mPointWorks.get(IdChoosePointWork).getId());
                                startActivity(OutUtils.newIntent(getApplication(), CurrentOrdersActivity.class, "", "", Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                            } else {
                                Toasty.error(getApplication(), Constants.ERROR_SIGN_IN, Toast.LENGTH_SHORT, true).show();
                            }
                        }
                    } catch (Exception e) {
                        if (NetworkUtil.isNetworkAvailable(getApplication()))
                            Toasty.error(getApplication(), Constants.ERROR_SIGN_IN, Toast.LENGTH_SHORT, true).show();
                        else Toasty.error(getApplication(), Constants.ERROR_NET_WORK, Toast.LENGTH_SHORT, true).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                t.printStackTrace();
                loaderSignHide();
                if (NetworkUtil.isNetworkAvailable(getApplication()))
                    Toasty.error(getApplication(), Constants.ERROR_SIGN_IN, Toast.LENGTH_SHORT, true).show();
                else Toasty.error(getApplication(), Constants.ERROR_NET_WORK, Toast.LENGTH_SHORT, true).show();
            }
        });
    }

    /**
     * Start work of user - get class User
     * @see StorageClass status list
     * @param id
     * @param idPointWork
     */
    private void startWorkUser(String id,String idPointWork) {

        //region create jsonObject [login] [password]
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("id",id);
            jsonObject.put("id_point_work",idPointWork);
        } catch (JSONException e) {
            Toasty.error(getApplication(), Constants.ERROR_NET_WORK, Toast.LENGTH_SHORT, true).show();
            return;
        }
        //endregion

        RetrofitSushi.getApi().addLogUpdateStartWork(Commands.USERS_UPDATE_START_WORK,jsonObject.toString(), MainConfig.KEY).enqueue(new Callback<Status>() {
            @Override
            public void onResponse(Call<Status> call, Response<Status> response) {
                try {
                    if(response.body()!=null)
                    {
                        mStorageClass.getStatusList().add(response.body());
                    }
                }
                catch (NullPointerException e)
                {
                    Toasty.warning(getApplication(), Constants.ERROR_OTHER, Toast.LENGTH_SHORT, true).show();
                }
            }

            @Override
            public void onFailure(Call<Status> call, Throwable t) {
                Toasty.warning(getApplication(), Constants.ERROR_OTHER, Toast.LENGTH_SHORT, true).show();
            }
        });
    }

    /**
     *  Get list products
     *  @see StorageClass list Product
     */
    private int getProducts() {
//        RetrofitSushi.getApi().getProducts(Commands.PRODUCTS_SELECT_ALL, MainConfig.KEY).enqueue(new Callback<List<Product>>() {
//            @Override
//            public void onResponse(Call<List<Product>> call, final Response<List<Product>> response) {
//                try {
//                    if(response.body()!=null)
//                    {
//                        mStorageClass.setProductList(response.body());
//                    }
//                }
//                catch (NullPointerException e)
//                {
//                    Toasty.warning(getApplication(), Constants.ERROR_PRODUCTS, Toast.LENGTH_SHORT, true).show();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<List<Product>> call, Throwable t) {
//                Toasty.warning(getApplication(), Constants.ERROR_PRODUCTS, Toast.LENGTH_SHORT, true).show();
//            }
//        });
        loaderSignHide();
        loaderSignShow();

        // check network
        if (!NetworkUtil.isNetworkAvailable(getApplication())) {
            Toasty.error(getApplication(), Constants.ERROR_NET_WORK, Toast.LENGTH_SHORT, true).show();
            loaderSpinnerHide();
            return 0;
        } else {
            LoadProducts lp = new LoadProducts();
            List<Product> prs = null;
            try {
                lp.execute();
                prs = lp.get();
                // check null
                if (prs != null) {
                    if (prs.get(0).getName().equals("test_product_asysns_task_error")) {
                        Toasty.error(getApplication(), "ошибка при загрузке данных! Перезапустите приложение!", Toast.LENGTH_SHORT, true).show();
                        loaderSignHide();
                    } else {
                        mStorageClass.setProductList(prs);
                        loaderSignHide();
                        return 1;
                    }
                }
                loaderSignHide();
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
                Toasty.error(getApplication(), "ошибка при загрузке продуктов! Перезапустите приложение!", Toast.LENGTH_SHORT, true).show();
                loaderSignHide();
                return 0;
            }
        }

        return 0;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        GLog.i("onDestroy AuthFragment");
    }

    //region Spinner show/hide
    private void loaderSpinnerHide()
    {
        mAVLoadingIndicatorViewSpinner.hide();
        mAVLoadingIndicatorViewSpinner.setVisibility(View.GONE);
        niceSpinner.setVisibility(View.VISIBLE);
    }

    private void loaderSpinnerShow()
    {
        mAVLoadingIndicatorViewSpinner.show();
        mAVLoadingIndicatorViewSpinner.setVisibility(View.VISIBLE);
        niceSpinner.setVisibility(View.GONE);
    }

    private void loaderSignHide()
    {
        mAVLoadingIndicatorViewSign.hide();
        mAVLoadingIndicatorViewSign.setVisibility(View.GONE);
        mButtonSignIn.setVisibility(View.VISIBLE);
        mButtonSignIn.setClickable(true);
    }

    private void loaderSignShow()
    {
        mAVLoadingIndicatorViewSign.show();
        mAVLoadingIndicatorViewSign.setVisibility(View.VISIBLE);
        mButtonSignIn.setVisibility(View.GONE);
        mButtonSignIn.setClickable(false);
    }
    // endregion

    private void startProject(){
        GLog.i("Project : " + MainConfig.NOTE + "\n" + "Version code: " + VersionUtil.getVersionName(getApplication()) + "\n" + "Author : " + MainConfig.AUTHOR_NAME);
    }

    private static class LoadProducts extends AsyncTask<String,Void,List<Product>>{

        @Override
        protected List<Product> doInBackground(String... strings) {
            List<Product> products = new ArrayList<>();
            Product p = new Product();
            p.setName("test_product_asysns_task_error");
            products.add(p);
            try {
                products = RetrofitSushi.getApi().getProducts(Commands.PRODUCTS_SELECT_ALL, MainConfig.KEY).execute().body();
            }
            catch(Exception e){e.printStackTrace();}
            return products;
        }
    }

    private static class LoadPointWorks extends AsyncTask<String,Void,List<PointWork>> {
        @Override
        protected List<PointWork> doInBackground(String... strings) {
            List<PointWork> points = new ArrayList<>();
            PointWork p = new PointWork();
            p.setName("test_point_work_asysns_task_error");
            points.add(p);
            try {
                points = RetrofitSushi.getApi().getPointsWork(Commands.POINTS_WORK_SELECT_ALL, MainConfig.KEY).execute().body();
                GLog.i("hello world!");
            }
            catch(Exception e){e.printStackTrace();}
            return points;
        }
    }

}

