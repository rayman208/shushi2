package com.dubrovin.povar.ui;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.widget.Toast;


import com.dubrovin.povar.R;
import com.dubrovin.povar.common.config.MainConfig;
import com.dubrovin.povar.common.constants.Commands;
import com.dubrovin.povar.common.constants.Constants;
import com.dubrovin.povar.common.utils.GLog;
import com.dubrovin.povar.data.StorageClass;
import com.dubrovin.povar.data.models.Order;
import com.dubrovin.povar.data.models.User;
import com.dubrovin.povar.data.network.RetrofitSushi;
import com.dubrovin.povar.data.models.Status;
import com.dubrovin.povar.ui.activities.CurrentOrdersActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.concurrent.TimeUnit;

import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public final class SushiService extends Service {

    private Thread mThread;
    private volatile boolean mFinish = false;
    private StorageClass mStorageClass = StorageClass.getInstance();
    private NotificationManager nm;
    final private static int NOTIFICATION_ID = 124;
    private Notification.Builder builder;

    @Override
    public void onCreate() {
        super.onCreate();
        GLog.i("onCreate SushiService");
        builder = new Notification.Builder(getApplicationContext());
        nm = (NotificationManager) getApplicationContext().getSystemService(NOTIFICATION_SERVICE);
        mainTask();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void mainTask() {
        mThread = new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {

                    // build notification
                    if(!mStorageClass.isOrdersListEmpty())
                        buildNotification();

                    try {
                        TimeUnit.SECONDS.sleep(10);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    GLog.i("SERVICE - getListOrders()");
                    getListOrders();

                    if (mFinish) {
                        mThread.interrupt();
                        return;
                    }
                }
            }
        });

        mThread.start();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        GLog.i("onDestroy SushiService");
        stopService(new Intent(getApplicationContext(), SushiService.class));
        // leaving the loop
        mFinish = true;
        //
        mThread.interrupt();
        // notification cancel
        nm.cancelAll();
    }

    /**
     *  Create Nofification <p>
     *  get list Orders + save their
     */
    private void getListOrders() {
        RetrofitSushi.getApi().getAllOrders(Commands.ORDERS_SELECT_POVAR_ORDERS, mStorageClass.getUser().getId(), MainConfig.KEY).enqueue(new Callback<List<Order>>() {
            @Override
            public void onResponse(Call<List<Order>> call, Response<List<Order>> response) {

                if (response.body() != null && !response.body().isEmpty()) {

                    try {
                        for (Order order : response.body()) {
                            AddOrderToQueue(mStorageClass.getUser(), order);
                        }
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }

                    // Save list Orders
                    mStorageClass.setOrdersList(response.body());
                }
            }

            @Override
            public void onFailure(Call<List<Order>> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private void buildNotification(){

        GLog.i("SushiService Notification Build");
        Intent intent = new Intent(getApplicationContext(), CurrentOrdersActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);

        builder
                .setContentIntent(pendingIntent)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setTicker("ПОВАР, ВАЖНОЕ СООБЩЕНИЕ")
                .setWhen(System.currentTimeMillis())
                .setAutoCancel(true)
                .setContentTitle("Повар, Новые заказы!!!")
                .setContentText("нажми на меня")
                .setVibrate(new long[]{1000, 1000})
                .setSound(Settings.System.DEFAULT_NOTIFICATION_URI);

        Notification notification = builder.build();
        nm.notify(NOTIFICATION_ID, notification);
    }

    /**
     * @param user
     * @param order
     * @since Заносит новый заказ в таблицу povars_work и логгирует действие
     */
    private void AddOrderToQueue(User user, Order order) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("id_user", user.getId());
            jsonObject.put("id_order", order.getId());
        } catch (JSONException e) {
            return;
        }

        RetrofitSushi.getApi().addOrderToQueue(Commands.POVAR_WORK_INSERT_ADD_ORDER_TO_QUEUE, jsonObject.toString(), MainConfig.KEY).enqueue(new Callback<Status>() {

            @Override
            public void onResponse(Call<Status> call, Response<Status> response) {
                if (response.body() != null)
                    mStorageClass.getStatusList().add(response.body());
            }

            @Override
            public void onFailure(Call<Status> call, Throwable t) {
                Toasty.warning(getApplicationContext(), Constants.ERROR_OTHER, Toast.LENGTH_SHORT, true).show();
            }
        });

        if(Integer.valueOf(order.getPovarUpakov()) == 1)
            RetrofitSushi.getApi().addOrderToQueue(Commands.UPAKOVS_WORK_INSERT_ADD_ORDER_TO_QUEUE, jsonObject.toString(), MainConfig.KEY).enqueue(new Callback<Status>() {

                @Override
                public void onResponse(Call<Status> call, Response<Status> response) {
                    if (response.body() != null)
                        mStorageClass.getStatusList().add(response.body());
                }

                @Override
                public void onFailure(Call<Status> call, Throwable t) {
                    Toasty.warning(getApplicationContext(), Constants.ERROR_OTHER, Toast.LENGTH_SHORT, true).show();
                }
            });
    }

}
