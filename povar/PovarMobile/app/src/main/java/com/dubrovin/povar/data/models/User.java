package com.dubrovin.povar.data.models;

/**
 * Created by Dubrovin on 11.12.2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class User {

    public String getNumberPhone() {
        return numberPhone;
    }
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("number_phone")
    @Expose
    private String numberPhone;
    @SerializedName("login")
    @Expose
    private String login;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("passport")
    @Expose
    private String passport;
    @SerializedName("id_dolgnost")
    @Expose
    private String idDolgnost;
    @SerializedName("id_point_work")
    @Expose
    private String idPointWork;
    @SerializedName("online")
    @Expose
    private String online;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setNumberPhone(String numberPhone) {
        this.numberPhone = numberPhone;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }


    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }


    public String getPassport() {
        return passport;
    }

    public void setPassport(String passport) {
        this.passport = passport;
    }


    public String getIdDolgnost() {
        return idDolgnost;
    }

    public void setIdDolgnost(String idDolgnost) {
        this.idDolgnost = idDolgnost;
    }


    public String getIdPointWork() {
        return idPointWork;
    }

    public void setIdPointWork(String idPointWork) {
        this.idPointWork = idPointWork;
    }


    public String getOnline() {
        return online;
    }

    public void setOnline(String online) {
        this.online = online;
    }

}