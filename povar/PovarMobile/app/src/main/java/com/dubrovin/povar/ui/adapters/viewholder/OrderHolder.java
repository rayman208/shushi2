package com.dubrovin.povar.ui.adapters.viewholder;

import android.graphics.Typeface;
import android.view.View;
import android.widget.TextView;

import com.dubrovin.povar.R;
import com.dubrovin.povar.common.constants.TypeView;
import com.dubrovin.povar.common.utils.OutUtils;
import com.dubrovin.povar.data.SingletonFonts;
import com.dubrovin.povar.data.StorageClass;
import com.dubrovin.povar.data.models.Order;
import com.dubrovin.povar.ui.base.BaseViewHolder;

/**
 * Created by Dubrovin on 15.12.2017.
 */

public class OrderHolder extends BaseViewHolder {
    private TextView mIdTextView;
    private TextView mPriceTextView;
    private TextView mCountPersonTextView;
    private TextView mOtherTextView;
    private TextView mDescriptionTextView;
    private TextView mDescription;
    private Order mProduct;
    private TypeView mTypeView;
    private Typeface font;

    public OrderHolder(View itemView, TypeView typeView) {
        super(itemView);

        try {
           font = SingletonFonts.getInstance(itemView.getContext()).getFont();

            mIdTextView = itemView.findViewById(R.id.order_id);
            mIdTextView.setTypeface(font);

            mPriceTextView = itemView.findViewById(R.id.order_price);
            mPriceTextView.setTypeface(font);

            mCountPersonTextView = itemView.findViewById(R.id.order_count_person);
            mCountPersonTextView.setTypeface(font);

            mOtherTextView = itemView.findViewById(R.id.order_other);
            mOtherTextView.setTypeface(font);

            mDescriptionTextView = itemView.findViewById(R.id.order_description);
            mDescriptionTextView.setTypeface(font);

            mDescription = itemView.findViewById(R.id.description);
            mDescription.setTypeface(font);
        }
        catch (Exception e){
            e.printStackTrace();
        }

        mTypeView = typeView;
    }

    @Override
    public <T> void bindRepos(T object) {
        mProduct = (Order) object;
        try {
            mIdTextView.setText(mProduct.getId());
            mCountPersonTextView.setText(mProduct.getCountPersons());
            mPriceTextView.setText(mProduct.getPrice());
            mOtherTextView.setText(OutUtils.otherParametersOrder(mProduct));

            if (mTypeView == TypeView.ORDERS) {
                mDescription.setVisibility(View.GONE);
                mDescriptionTextView.setVisibility(View.GONE);
            } else {
                mDescription.setVisibility(View.VISIBLE);
                mDescriptionTextView.setVisibility(View.VISIBLE);
                mDescriptionTextView.setText(mProduct.getDescription());
            }
        }
        catch (Exception e){e.printStackTrace();}
    }


}
