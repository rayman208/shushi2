package com.dubrovin.povar.ui.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dubrovin.povar.R;
import com.dubrovin.povar.common.constants.TypeView;
import com.dubrovin.povar.data.models.Order;
import com.dubrovin.povar.data.models.Product;
import com.dubrovin.povar.ui.adapters.viewholder.OrderHolder;
import com.dubrovin.povar.ui.base.BaseViewHolder;


import java.util.List;

/**
 * Created by Dubrovin on 15.12.2017.
 */

public class OrderAdapter extends RecyclerView.Adapter<BaseViewHolder> {
    private List<Order> mOrders;
    private TypeView mTypeView;

    public OrderAdapter(List<Order> Orders, TypeView typeView) {
        mOrders = Orders;
        mTypeView = typeView;
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //  LayoutInflater layoutInflater = LayoutInflater.from(getActivity())

        BaseViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View viewItem = null;

        switch (mTypeView) {
            case ORDERS:
                viewItem = inflater.inflate(R.layout.item_order, parent, false);
                viewHolder = new OrderHolder(viewItem,mTypeView);
                break;
            case ORDER:
                viewItem = inflater.inflate(R.layout.item_order, parent, false);
                viewHolder = new OrderHolder(viewItem,mTypeView);
                break;
        }

        return viewHolder;
    }


    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        Order order = mOrders.get(position);
        holder.bindRepos(order);
    }

    @Override
    public int getItemCount() {
        if(mOrders == null)
            return 0;
        else
            return mOrders.size();
    }

    public void add(Order r) {
        mOrders.add(r);
        notifyItemInserted(mOrders.size());
    }

    public void addAll(List<Order> postReposes) {
        mOrders.addAll(postReposes);
        notifyItemInserted(mOrders.size());
    }

    public void remove(Order r) {
        int position = mOrders.indexOf(r);
        if (position > -1) {
            mOrders.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void clear() {
        while (getItemCount() > 0) {
            remove(getItem(0));
        }
    }

    public boolean isEmpty() {
        return getItemCount() == 0;
    }

    public Order getItem(int position) {
        return mOrders.get(position);
    }
}
