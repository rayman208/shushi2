package com.dubrovin.povar.ui.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dubrovin.povar.R;
import com.dubrovin.povar.common.constants.TypeView;
import com.dubrovin.povar.common.utils.GLog;
import com.dubrovin.povar.data.models.Order;
import com.dubrovin.povar.data.models.Product;
import com.dubrovin.povar.ui.adapters.viewholder.ProductHolder;
import com.dubrovin.povar.ui.base.BaseViewHolder;

import java.util.List;

/**
 * Created by Dubrovin on 18.12.2017.
 */

public class ProductAdapter extends RecyclerView.Adapter<BaseViewHolder> {
    private List<Product> mProducts;

    public ProductAdapter(List<Product> Products) {
        mProducts = Products;
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //  LayoutInflater layoutInflater = LayoutInflater.from(getActivity())

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View viewItem = inflater.inflate(R.layout.item_product, parent, false);
        BaseViewHolder viewHolder = new ProductHolder(viewItem);

        GLog.i("onCreateViewHolder: " + TypeView.PRODUCT.toString());

        return viewHolder;
    }


    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        Product product = mProducts.get(position);
        holder.bindRepos(product);
    }

    @Override
    public int getItemCount() {
        if(mProducts == null)
            return 0;
        else
            return mProducts.size();
    }

    public void add(Product r) {
        mProducts.add(r);
        notifyItemInserted(mProducts.size());
    }

    public void addAll(List<Product> postReposes) {
        mProducts.addAll(postReposes);
        notifyItemInserted(mProducts.size());
    }

    public void remove(Order r) {
        int position = mProducts.indexOf(r);
        if (position > -1) {
            mProducts.remove(position);
            notifyItemRemoved(position);
        }
    }


    public boolean isEmpty() {
        return getItemCount() == 0;
    }

    public Product getItem(int position) {
        return mProducts.get(position);
    }
}
