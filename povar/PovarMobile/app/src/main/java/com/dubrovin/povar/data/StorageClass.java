package com.dubrovin.povar.data;

import android.content.Context;
import android.graphics.Typeface;

import com.dubrovin.povar.data.models.Order;
import com.dubrovin.povar.data.models.PointWork;
import com.dubrovin.povar.data.models.Product;
import com.dubrovin.povar.data.models.ProductId;
import com.dubrovin.povar.data.models.Status;
import com.dubrovin.povar.data.models.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dubrovin on 15.12.2017.
 */

public class StorageClass {

    private static volatile StorageClass instance;

    private PointWork ChoosePointWork;
    private User mUser;
    private Order mOrderChoose;
    private String State = "auth";

    private List<Product> mProductList = new ArrayList<>();
    private List<Order> mOrdersList = new ArrayList<>();
    private List<Status> mStatusList = new ArrayList<>();
    private List<ProductId> mProductIds = new ArrayList<>();

    public static StorageClass getInstance() {
        StorageClass localInstance = instance;
        if (localInstance == null) {
            synchronized (StorageClass.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new StorageClass();
                }
            }
        }

        return localInstance;
    }

    //region getters && setters && check_null
    public PointWork getChoosePointWork() {
        return ChoosePointWork;
    }

    public void setChoosePointWork(PointWork choosePointWork) {
        ChoosePointWork = choosePointWork;
    }

    public List<Product> getProductList() {
        return mProductList;
    }

    public void setProductList(List<Product> productList) {
        mProductList = productList;
    }

    public boolean isProductListEmpty(){
        return mProductList != null && mProductList.isEmpty();
    }

    public List<Order> getOrdersList() {
        return mOrdersList;
    }

    public void setOrdersList(List<Order> ordersList) {
        mOrdersList = ordersList;
    }

    public boolean isOrdersListEmpty(){
        return mOrdersList != null && mOrdersList.isEmpty();
    }

    public List<Status> getStatusList() {
        return mStatusList;
    }

    public void setStatusList(List<Status> statusList) {
        mStatusList = statusList;
    }

    public boolean isStatusListEmtpy(){
        return mStatusList != null && mStatusList.isEmpty();
    }

    public User getUser() {
        return mUser;
    }

    public void setUser(User user) {
        mUser = user;
    }

    public Order getOrderChoose() {return mOrderChoose;}

    public void setOrderChoose(Order orderChoose) {mOrderChoose = orderChoose;}

    public String getState() {return State;}

    public void setState(String state) {
        State = state;
    }

    public List<ProductId> getProductIds() {
        return mProductIds;
    }

    public void setProductIds(List<ProductId> productIds) {
        mProductIds = productIds;
    }

    public boolean isProductsIdsListEmtpy(){
        return mProductIds != null && mProductIds.isEmpty();
    }
    //endregion

}
