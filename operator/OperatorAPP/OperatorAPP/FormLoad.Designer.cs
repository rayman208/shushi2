﻿namespace OperatorAPP
{
    partial class FormLoad
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pictureBoxLoadAnim = new System.Windows.Forms.PictureBox();
            this.timerUpdater = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLoadAnim)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBoxLoadAnim
            // 
            this.pictureBoxLoadAnim.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.pictureBoxLoadAnim.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBoxLoadAnim.Location = new System.Drawing.Point(0, 0);
            this.pictureBoxLoadAnim.Name = "pictureBoxLoadAnim";
            this.pictureBoxLoadAnim.Size = new System.Drawing.Size(240, 233);
            this.pictureBoxLoadAnim.TabIndex = 0;
            this.pictureBoxLoadAnim.TabStop = false;
            this.pictureBoxLoadAnim.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBoxLoadAnim_Paint);
            // 
            // timerUpdater
            // 
            this.timerUpdater.Enabled = true;
            this.timerUpdater.Interval = 15;
            this.timerUpdater.Tick += new System.EventHandler(this.timerUpdater_Tick);
            // 
            // FormLoad
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(240, 233);
            this.Controls.Add(this.pictureBoxLoadAnim);
            this.Name = "FormLoad";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Загрузка";
            this.Load += new System.EventHandler(this.FormLoad_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLoadAnim)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBoxLoadAnim;
        private System.Windows.Forms.Timer timerUpdater;
    }
}