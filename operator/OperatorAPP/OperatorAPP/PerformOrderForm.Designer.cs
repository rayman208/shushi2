﻿namespace OperatorAPP
{
    partial class PerformOrderForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            this.comboBoxProductName = new System.Windows.Forms.ComboBox();
            this.textBoxProductPrice = new System.Windows.Forms.TextBox();
            this.numericUpDownCountProduct = new System.Windows.Forms.NumericUpDown();
            this.buttonAddProduct = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.dataGridViewOrderProducts = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.numericUpDownCountPersons = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxPromocode = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxOrderTotalSum = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.textBoxSkidka = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.textBoxPrice_With_Skidka = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.textBoxClientGaveMoney = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.richTextBoxOrderDescription = new System.Windows.Forms.RichTextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.comboBoxCity = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.comboBoxStreet = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.textBoxNumber_Home = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.textBoxNumber_Corp = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.textBoxNumber_Build = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.textBoxNumber_Flat = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.textBoxClientName = new System.Windows.Forms.TextBox();
            this.textBoxClientSurname = new System.Windows.Forms.TextBox();
            this.comboBoxPhoneNumber = new System.Windows.Forms.ComboBox();
            this.label20 = new System.Windows.Forms.Label();
            this.comboBoxRayon = new System.Windows.Forms.ComboBox();
            this.numericUpDownBirthDAY = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownBirthMONTH = new System.Windows.Forms.NumericUpDown();
            this.label23 = new System.Windows.Forms.Label();
            this.textBoxCardNumber = new System.Windows.Forms.TextBox();
            this.comboBoxPoint_Work = new System.Windows.Forms.ComboBox();
            this.label24 = new System.Windows.Forms.Label();
            this.checkBoxIsOperator_Prodavec = new System.Windows.Forms.CheckBox();
            this.checkBoxIsPovar_Upakov = new System.Windows.Forms.CheckBox();
            this.checkBoxIsSamovivos = new System.Windows.Forms.CheckBox();
            this.comboBoxSelectPovar = new System.Windows.Forms.ComboBox();
            this.comboBoxSelectUpakov = new System.Windows.Forms.ComboBox();
            this.comboBoxSelectCurer = new System.Windows.Forms.ComboBox();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.buttonResetPerform = new System.Windows.Forms.Button();
            this.checkBoxIsBeznal = new System.Windows.Forms.CheckBox();
            this.buttonPerform = new System.Windows.Forms.Button();
            this.checkBoxSendTo_FNS = new System.Windows.Forms.CheckBox();
            this.label28 = new System.Windows.Forms.Label();
            this.comboBoxAdv_Point = new System.Windows.Forms.ComboBox();
            this.buttonGoAway = new System.Windows.Forms.Button();
            this.buttonGoToEdit = new System.Windows.Forms.Button();
            this.buttonReloadUsers = new System.Windows.Forms.Button();
            this.labelSdacha = new System.Windows.Forms.Label();
            this.buttonResetUserInfo = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownCountProduct)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewOrderProducts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownCountPersons)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownBirthDAY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownBirthMONTH)).BeginInit();
            this.SuspendLayout();
            // 
            // comboBoxProductName
            // 
            this.comboBoxProductName.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.comboBoxProductName.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBoxProductName.FormattingEnabled = true;
            this.comboBoxProductName.Location = new System.Drawing.Point(6, 26);
            this.comboBoxProductName.Margin = new System.Windows.Forms.Padding(2);
            this.comboBoxProductName.Name = "comboBoxProductName";
            this.comboBoxProductName.Size = new System.Drawing.Size(373, 21);
            this.comboBoxProductName.TabIndex = 0;
            this.comboBoxProductName.SelectedIndexChanged += new System.EventHandler(this.comboBoxProductName_SelectedIndexChanged);
            this.comboBoxProductName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.comboBoxProductName_KeyDown);
            // 
            // textBoxProductPrice
            // 
            this.textBoxProductPrice.Location = new System.Drawing.Point(384, 26);
            this.textBoxProductPrice.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxProductPrice.Name = "textBoxProductPrice";
            this.textBoxProductPrice.ReadOnly = true;
            this.textBoxProductPrice.Size = new System.Drawing.Size(64, 20);
            this.textBoxProductPrice.TabIndex = 2;
            this.textBoxProductPrice.TabStop = false;
            // 
            // numericUpDownCountProduct
            // 
            this.numericUpDownCountProduct.Location = new System.Drawing.Point(450, 26);
            this.numericUpDownCountProduct.Margin = new System.Windows.Forms.Padding(2);
            this.numericUpDownCountProduct.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownCountProduct.Name = "numericUpDownCountProduct";
            this.numericUpDownCountProduct.Size = new System.Drawing.Size(96, 20);
            this.numericUpDownCountProduct.TabIndex = 1;
            this.numericUpDownCountProduct.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // buttonAddProduct
            // 
            this.buttonAddProduct.Location = new System.Drawing.Point(552, 6);
            this.buttonAddProduct.Margin = new System.Windows.Forms.Padding(2);
            this.buttonAddProduct.Name = "buttonAddProduct";
            this.buttonAddProduct.Size = new System.Drawing.Size(192, 39);
            this.buttonAddProduct.TabIndex = 2;
            this.buttonAddProduct.Text = "Добавить продукт";
            this.buttonAddProduct.UseVisualStyleBackColor = true;
            this.buttonAddProduct.Click += new System.EventHandler(this.buttonAddProduct_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 6);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(106, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Название продукта";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(384, 6);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Цена";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(450, 6);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(66, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Количество";
            // 
            // dataGridViewOrderProducts
            // 
            this.dataGridViewOrderProducts.AllowUserToAddRows = false;
            this.dataGridViewOrderProducts.AllowUserToDeleteRows = false;
            this.dataGridViewOrderProducts.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewOrderProducts.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle10;
            this.dataGridViewOrderProducts.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewOrderProducts.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5});
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewOrderProducts.DefaultCellStyle = dataGridViewCellStyle11;
            this.dataGridViewOrderProducts.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dataGridViewOrderProducts.Location = new System.Drawing.Point(6, 52);
            this.dataGridViewOrderProducts.Margin = new System.Windows.Forms.Padding(2);
            this.dataGridViewOrderProducts.MultiSelect = false;
            this.dataGridViewOrderProducts.Name = "dataGridViewOrderProducts";
            this.dataGridViewOrderProducts.ReadOnly = true;
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewOrderProducts.RowHeadersDefaultCellStyle = dataGridViewCellStyle12;
            this.dataGridViewOrderProducts.RowHeadersVisible = false;
            this.dataGridViewOrderProducts.RowTemplate.Height = 24;
            this.dataGridViewOrderProducts.Size = new System.Drawing.Size(738, 122);
            this.dataGridViewOrderProducts.TabIndex = 5;
            this.dataGridViewOrderProducts.TabStop = false;
            this.dataGridViewOrderProducts.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewOrderProducts_CellClick);
            this.dataGridViewOrderProducts.CellMouseDown += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridViewOrderProducts_CellMouseDown);
            // 
            // Column1
            // 
            this.Column1.FillWeight = 1F;
            this.Column1.HeaderText = "Номер";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // Column2
            // 
            this.Column2.FillWeight = 4F;
            this.Column2.HeaderText = "Продукт";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            // 
            // Column3
            // 
            this.Column3.FillWeight = 1F;
            this.Column3.HeaderText = "Цена";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            // 
            // Column4
            // 
            this.Column4.FillWeight = 1F;
            this.Column4.HeaderText = "Количество";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            // 
            // Column5
            // 
            this.Column5.FillWeight = 1F;
            this.Column5.HeaderText = "Стоимость";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            // 
            // numericUpDownCountPersons
            // 
            this.numericUpDownCountPersons.Location = new System.Drawing.Point(6, 202);
            this.numericUpDownCountPersons.Margin = new System.Windows.Forms.Padding(2);
            this.numericUpDownCountPersons.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownCountPersons.Name = "numericUpDownCountPersons";
            this.numericUpDownCountPersons.Size = new System.Drawing.Size(102, 20);
            this.numericUpDownCountPersons.TabIndex = 3;
            this.numericUpDownCountPersons.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 182);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(105, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Количество персон";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(126, 182);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(59, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "Промокод";
            // 
            // textBoxPromocode
            // 
            this.textBoxPromocode.Location = new System.Drawing.Point(126, 202);
            this.textBoxPromocode.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxPromocode.MaxLength = 20;
            this.textBoxPromocode.Name = "textBoxPromocode";
            this.textBoxPromocode.Size = new System.Drawing.Size(76, 20);
            this.textBoxPromocode.TabIndex = 4;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(258, 182);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(101, 13);
            this.label6.TabIndex = 7;
            this.label6.Text = "Стоимость заказа";
            // 
            // textBoxOrderTotalSum
            // 
            this.textBoxOrderTotalSum.Location = new System.Drawing.Point(258, 202);
            this.textBoxOrderTotalSum.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxOrderTotalSum.Name = "textBoxOrderTotalSum";
            this.textBoxOrderTotalSum.ReadOnly = true;
            this.textBoxOrderTotalSum.Size = new System.Drawing.Size(103, 20);
            this.textBoxOrderTotalSum.TabIndex = 9;
            this.textBoxOrderTotalSum.TabStop = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(372, 182);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(44, 13);
            this.label7.TabIndex = 10;
            this.label7.Text = "Скидка";
            // 
            // textBoxSkidka
            // 
            this.textBoxSkidka.Location = new System.Drawing.Point(372, 202);
            this.textBoxSkidka.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxSkidka.Name = "textBoxSkidka";
            this.textBoxSkidka.Size = new System.Drawing.Size(76, 20);
            this.textBoxSkidka.TabIndex = 5;
            this.textBoxSkidka.TextChanged += new System.EventHandler(this.textBoxSkidka_TextChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(456, 182);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(37, 13);
            this.label8.TabIndex = 12;
            this.label8.Text = "Итого";
            // 
            // textBoxPrice_With_Skidka
            // 
            this.textBoxPrice_With_Skidka.Location = new System.Drawing.Point(456, 202);
            this.textBoxPrice_With_Skidka.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxPrice_With_Skidka.Name = "textBoxPrice_With_Skidka";
            this.textBoxPrice_With_Skidka.ReadOnly = true;
            this.textBoxPrice_With_Skidka.Size = new System.Drawing.Size(73, 20);
            this.textBoxPrice_With_Skidka.TabIndex = 13;
            this.textBoxPrice_With_Skidka.TabStop = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(570, 182);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(96, 13);
            this.label9.TabIndex = 14;
            this.label9.Text = "Клиент дал денег";
            // 
            // textBoxClientGaveMoney
            // 
            this.textBoxClientGaveMoney.Location = new System.Drawing.Point(570, 202);
            this.textBoxClientGaveMoney.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxClientGaveMoney.Name = "textBoxClientGaveMoney";
            this.textBoxClientGaveMoney.Size = new System.Drawing.Size(109, 20);
            this.textBoxClientGaveMoney.TabIndex = 6;
            this.textBoxClientGaveMoney.TextChanged += new System.EventHandler(this.textBoxClientGaveMoney_TextChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 228);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(322, 13);
            this.label10.TabIndex = 16;
            this.label10.Text = "Примечание к заказу ( не более 1000 символов) (0000 / 1000)";
            // 
            // richTextBoxOrderDescription
            // 
            this.richTextBoxOrderDescription.Location = new System.Drawing.Point(6, 247);
            this.richTextBoxOrderDescription.Margin = new System.Windows.Forms.Padding(2);
            this.richTextBoxOrderDescription.MaxLength = 1000;
            this.richTextBoxOrderDescription.Name = "richTextBoxOrderDescription";
            this.richTextBoxOrderDescription.Size = new System.Drawing.Size(379, 59);
            this.richTextBoxOrderDescription.TabIndex = 8;
            this.richTextBoxOrderDescription.Text = "";
            this.richTextBoxOrderDescription.TextChanged += new System.EventHandler(this.richTextBoxOrderDescription_TextChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(227, 311);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(37, 13);
            this.label11.TabIndex = 18;
            this.label11.Text = "Город";
            // 
            // comboBoxCity
            // 
            this.comboBoxCity.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.comboBoxCity.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBoxCity.FormattingEnabled = true;
            this.comboBoxCity.Location = new System.Drawing.Point(227, 331);
            this.comboBoxCity.Margin = new System.Windows.Forms.Padding(2);
            this.comboBoxCity.Name = "comboBoxCity";
            this.comboBoxCity.Size = new System.Drawing.Size(103, 21);
            this.comboBoxCity.TabIndex = 12;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(335, 311);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(39, 13);
            this.label12.TabIndex = 20;
            this.label12.Text = "Улица";
            // 
            // comboBoxStreet
            // 
            this.comboBoxStreet.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.comboBoxStreet.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBoxStreet.FormattingEnabled = true;
            this.comboBoxStreet.Location = new System.Drawing.Point(335, 331);
            this.comboBoxStreet.Margin = new System.Windows.Forms.Padding(2);
            this.comboBoxStreet.Name = "comboBoxStreet";
            this.comboBoxStreet.Size = new System.Drawing.Size(133, 21);
            this.comboBoxStreet.TabIndex = 13;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(173, 358);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(30, 13);
            this.label13.TabIndex = 22;
            this.label13.Text = "Дом";
            // 
            // textBoxNumber_Home
            // 
            this.textBoxNumber_Home.Location = new System.Drawing.Point(173, 377);
            this.textBoxNumber_Home.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxNumber_Home.Name = "textBoxNumber_Home";
            this.textBoxNumber_Home.Size = new System.Drawing.Size(55, 20);
            this.textBoxNumber_Home.TabIndex = 18;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(233, 358);
            this.label14.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(43, 13);
            this.label14.TabIndex = 22;
            this.label14.Text = "Корпус";
            // 
            // textBoxNumber_Corp
            // 
            this.textBoxNumber_Corp.Location = new System.Drawing.Point(233, 377);
            this.textBoxNumber_Corp.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxNumber_Corp.Name = "textBoxNumber_Corp";
            this.textBoxNumber_Corp.Size = new System.Drawing.Size(55, 20);
            this.textBoxNumber_Corp.TabIndex = 19;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(293, 358);
            this.label15.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(55, 13);
            this.label15.TabIndex = 22;
            this.label15.Text = "Строение";
            // 
            // textBoxNumber_Build
            // 
            this.textBoxNumber_Build.Location = new System.Drawing.Point(293, 377);
            this.textBoxNumber_Build.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxNumber_Build.Name = "textBoxNumber_Build";
            this.textBoxNumber_Build.Size = new System.Drawing.Size(55, 20);
            this.textBoxNumber_Build.TabIndex = 20;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(353, 358);
            this.label16.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(55, 13);
            this.label16.TabIndex = 22;
            this.label16.Text = "Квартира";
            // 
            // textBoxNumber_Flat
            // 
            this.textBoxNumber_Flat.Location = new System.Drawing.Point(353, 377);
            this.textBoxNumber_Flat.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxNumber_Flat.Name = "textBoxNumber_Flat";
            this.textBoxNumber_Flat.Size = new System.Drawing.Size(61, 20);
            this.textBoxNumber_Flat.TabIndex = 21;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(7, 312);
            this.label17.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(52, 13);
            this.label17.TabIndex = 22;
            this.label17.Text = "Телефон";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(115, 312);
            this.label18.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(29, 13);
            this.label18.TabIndex = 22;
            this.label18.Text = "Имя";
            // 
            // textBoxClientName
            // 
            this.textBoxClientName.Location = new System.Drawing.Point(115, 332);
            this.textBoxClientName.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxClientName.Name = "textBoxClientName";
            this.textBoxClientName.Size = new System.Drawing.Size(103, 20);
            this.textBoxClientName.TabIndex = 10;
            // 
            // textBoxClientSurname
            // 
            this.textBoxClientSurname.Location = new System.Drawing.Point(631, 226);
            this.textBoxClientSurname.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxClientSurname.Name = "textBoxClientSurname";
            this.textBoxClientSurname.Size = new System.Drawing.Size(21, 20);
            this.textBoxClientSurname.TabIndex = 11;
            this.textBoxClientSurname.TabStop = false;
            // 
            // comboBoxPhoneNumber
            // 
            this.comboBoxPhoneNumber.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.comboBoxPhoneNumber.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBoxPhoneNumber.FormattingEnabled = true;
            this.comboBoxPhoneNumber.Location = new System.Drawing.Point(7, 332);
            this.comboBoxPhoneNumber.Margin = new System.Windows.Forms.Padding(2);
            this.comboBoxPhoneNumber.Name = "comboBoxPhoneNumber";
            this.comboBoxPhoneNumber.Size = new System.Drawing.Size(103, 21);
            this.comboBoxPhoneNumber.TabIndex = 9;
            this.comboBoxPhoneNumber.SelectedValueChanged += new System.EventHandler(this.comboBoxPhoneNumber_SelectedValueChanged);
            this.comboBoxPhoneNumber.Leave += new System.EventHandler(this.comboBoxPhoneNumber_Leave);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(479, 311);
            this.label20.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(38, 13);
            this.label20.TabIndex = 18;
            this.label20.Text = "Район";
            // 
            // comboBoxRayon
            // 
            this.comboBoxRayon.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.comboBoxRayon.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBoxRayon.FormattingEnabled = true;
            this.comboBoxRayon.Location = new System.Drawing.Point(479, 331);
            this.comboBoxRayon.Margin = new System.Windows.Forms.Padding(2);
            this.comboBoxRayon.Name = "comboBoxRayon";
            this.comboBoxRayon.Size = new System.Drawing.Size(103, 21);
            this.comboBoxRayon.TabIndex = 14;
            // 
            // numericUpDownBirthDAY
            // 
            this.numericUpDownBirthDAY.Location = new System.Drawing.Point(656, 226);
            this.numericUpDownBirthDAY.Margin = new System.Windows.Forms.Padding(2);
            this.numericUpDownBirthDAY.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            this.numericUpDownBirthDAY.Name = "numericUpDownBirthDAY";
            this.numericUpDownBirthDAY.Size = new System.Drawing.Size(40, 20);
            this.numericUpDownBirthDAY.TabIndex = 15;
            this.numericUpDownBirthDAY.TabStop = false;
            this.numericUpDownBirthDAY.Value = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            // 
            // numericUpDownBirthMONTH
            // 
            this.numericUpDownBirthMONTH.Location = new System.Drawing.Point(700, 226);
            this.numericUpDownBirthMONTH.Margin = new System.Windows.Forms.Padding(2);
            this.numericUpDownBirthMONTH.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            this.numericUpDownBirthMONTH.Name = "numericUpDownBirthMONTH";
            this.numericUpDownBirthMONTH.Size = new System.Drawing.Size(43, 20);
            this.numericUpDownBirthMONTH.TabIndex = 16;
            this.numericUpDownBirthMONTH.TabStop = false;
            this.numericUpDownBirthMONTH.Value = new decimal(new int[] {
            1,
            0,
            0,
            -2147483648});
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(6, 358);
            this.label23.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(75, 13);
            this.label23.TabIndex = 22;
            this.label23.Text = "Номер карты";
            // 
            // textBoxCardNumber
            // 
            this.textBoxCardNumber.Location = new System.Drawing.Point(6, 378);
            this.textBoxCardNumber.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxCardNumber.Name = "textBoxCardNumber";
            this.textBoxCardNumber.Size = new System.Drawing.Size(163, 20);
            this.textBoxCardNumber.TabIndex = 17;
            // 
            // comboBoxPoint_Work
            // 
            this.comboBoxPoint_Work.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxPoint_Work.FormattingEnabled = true;
            this.comboBoxPoint_Work.Location = new System.Drawing.Point(390, 422);
            this.comboBoxPoint_Work.Margin = new System.Windows.Forms.Padding(2);
            this.comboBoxPoint_Work.Name = "comboBoxPoint_Work";
            this.comboBoxPoint_Work.Size = new System.Drawing.Size(355, 21);
            this.comboBoxPoint_Work.TabIndex = 23;
            this.comboBoxPoint_Work.SelectedIndexChanged += new System.EventHandler(this.comboBoxPoint_Work_SelectedIndexChanged);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(390, 403);
            this.label24.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(77, 13);
            this.label24.TabIndex = 22;
            this.label24.Text = "Точка работы";
            // 
            // checkBoxIsOperator_Prodavec
            // 
            this.checkBoxIsOperator_Prodavec.AutoSize = true;
            this.checkBoxIsOperator_Prodavec.Location = new System.Drawing.Point(7, 417);
            this.checkBoxIsOperator_Prodavec.Margin = new System.Windows.Forms.Padding(2);
            this.checkBoxIsOperator_Prodavec.Name = "checkBoxIsOperator_Prodavec";
            this.checkBoxIsOperator_Prodavec.Size = new System.Drawing.Size(128, 17);
            this.checkBoxIsOperator_Prodavec.TabIndex = 24;
            this.checkBoxIsOperator_Prodavec.Text = "Оператор-Продавец";
            this.checkBoxIsOperator_Prodavec.UseVisualStyleBackColor = true;
            this.checkBoxIsOperator_Prodavec.CheckedChanged += new System.EventHandler(this.OnSelectPersonCheckedChanged);
            this.checkBoxIsOperator_Prodavec.KeyDown += new System.Windows.Forms.KeyEventHandler(this.checkBoxIsOperator_Prodavec_KeyDown);
            // 
            // checkBoxIsPovar_Upakov
            // 
            this.checkBoxIsPovar_Upakov.AutoSize = true;
            this.checkBoxIsPovar_Upakov.Location = new System.Drawing.Point(139, 417);
            this.checkBoxIsPovar_Upakov.Margin = new System.Windows.Forms.Padding(2);
            this.checkBoxIsPovar_Upakov.Name = "checkBoxIsPovar_Upakov";
            this.checkBoxIsPovar_Upakov.Size = new System.Drawing.Size(120, 17);
            this.checkBoxIsPovar_Upakov.TabIndex = 25;
            this.checkBoxIsPovar_Upakov.Text = "Повар-Упаковщик";
            this.checkBoxIsPovar_Upakov.UseVisualStyleBackColor = true;
            this.checkBoxIsPovar_Upakov.CheckedChanged += new System.EventHandler(this.OnSelectPersonCheckedChanged);
            this.checkBoxIsPovar_Upakov.KeyDown += new System.Windows.Forms.KeyEventHandler(this.checkBoxIsPovar_Upakov_KeyDown);
            // 
            // checkBoxIsSamovivos
            // 
            this.checkBoxIsSamovivos.AutoSize = true;
            this.checkBoxIsSamovivos.Location = new System.Drawing.Point(271, 417);
            this.checkBoxIsSamovivos.Margin = new System.Windows.Forms.Padding(2);
            this.checkBoxIsSamovivos.Name = "checkBoxIsSamovivos";
            this.checkBoxIsSamovivos.Size = new System.Drawing.Size(85, 17);
            this.checkBoxIsSamovivos.TabIndex = 26;
            this.checkBoxIsSamovivos.Text = "Самовывоз";
            this.checkBoxIsSamovivos.UseVisualStyleBackColor = true;
            this.checkBoxIsSamovivos.CheckedChanged += new System.EventHandler(this.OnSelectPersonCheckedChanged);
            this.checkBoxIsSamovivos.KeyDown += new System.Windows.Forms.KeyEventHandler(this.checkBoxIsSamovivos_KeyDown);
            // 
            // comboBoxSelectPovar
            // 
            this.comboBoxSelectPovar.FormattingEnabled = true;
            this.comboBoxSelectPovar.Location = new System.Drawing.Point(6, 455);
            this.comboBoxSelectPovar.Margin = new System.Windows.Forms.Padding(2);
            this.comboBoxSelectPovar.Name = "comboBoxSelectPovar";
            this.comboBoxSelectPovar.Size = new System.Drawing.Size(127, 21);
            this.comboBoxSelectPovar.TabIndex = 27;
            // 
            // comboBoxSelectUpakov
            // 
            this.comboBoxSelectUpakov.FormattingEnabled = true;
            this.comboBoxSelectUpakov.Location = new System.Drawing.Point(138, 455);
            this.comboBoxSelectUpakov.Margin = new System.Windows.Forms.Padding(2);
            this.comboBoxSelectUpakov.Name = "comboBoxSelectUpakov";
            this.comboBoxSelectUpakov.Size = new System.Drawing.Size(127, 21);
            this.comboBoxSelectUpakov.TabIndex = 28;
            // 
            // comboBoxSelectCurer
            // 
            this.comboBoxSelectCurer.FormattingEnabled = true;
            this.comboBoxSelectCurer.Location = new System.Drawing.Point(270, 455);
            this.comboBoxSelectCurer.Margin = new System.Windows.Forms.Padding(2);
            this.comboBoxSelectCurer.Name = "comboBoxSelectCurer";
            this.comboBoxSelectCurer.Size = new System.Drawing.Size(115, 21);
            this.comboBoxSelectCurer.TabIndex = 29;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(6, 436);
            this.label25.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(39, 13);
            this.label25.TabIndex = 22;
            this.label25.Text = "Повар";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(138, 436);
            this.label26.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(66, 13);
            this.label26.TabIndex = 22;
            this.label26.Text = "Упаковщик";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(270, 436);
            this.label27.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(43, 13);
            this.label27.TabIndex = 22;
            this.label27.Text = "Курьер";
            // 
            // buttonResetPerform
            // 
            this.buttonResetPerform.Location = new System.Drawing.Point(138, 481);
            this.buttonResetPerform.Margin = new System.Windows.Forms.Padding(2);
            this.buttonResetPerform.Name = "buttonResetPerform";
            this.buttonResetPerform.Size = new System.Drawing.Size(126, 39);
            this.buttonResetPerform.TabIndex = 28;
            this.buttonResetPerform.TabStop = false;
            this.buttonResetPerform.Text = "Очистить все поля";
            this.buttonResetPerform.UseVisualStyleBackColor = true;
            this.buttonResetPerform.Click += new System.EventHandler(this.buttonResetPerform_Click);
            // 
            // checkBoxIsBeznal
            // 
            this.checkBoxIsBeznal.AutoSize = true;
            this.checkBoxIsBeznal.Location = new System.Drawing.Point(684, 202);
            this.checkBoxIsBeznal.Margin = new System.Windows.Forms.Padding(2);
            this.checkBoxIsBeznal.Name = "checkBoxIsBeznal";
            this.checkBoxIsBeznal.Size = new System.Drawing.Size(63, 17);
            this.checkBoxIsBeznal.TabIndex = 7;
            this.checkBoxIsBeznal.Text = "Безнал";
            this.checkBoxIsBeznal.UseVisualStyleBackColor = true;
            this.checkBoxIsBeznal.CheckedChanged += new System.EventHandler(this.checkBoxIsBeznal_CheckedChanged);
            // 
            // buttonPerform
            // 
            this.buttonPerform.Location = new System.Drawing.Point(390, 481);
            this.buttonPerform.Margin = new System.Windows.Forms.Padding(2);
            this.buttonPerform.Name = "buttonPerform";
            this.buttonPerform.Size = new System.Drawing.Size(354, 39);
            this.buttonPerform.TabIndex = 31;
            this.buttonPerform.Text = "Отправить заказ и распечатать чек";
            this.buttonPerform.UseVisualStyleBackColor = true;
            this.buttonPerform.Click += new System.EventHandler(this.buttonPerform_Click);
            // 
            // checkBoxSendTo_FNS
            // 
            this.checkBoxSendTo_FNS.AutoSize = true;
            this.checkBoxSendTo_FNS.Location = new System.Drawing.Point(600, 403);
            this.checkBoxSendTo_FNS.Margin = new System.Windows.Forms.Padding(2);
            this.checkBoxSendTo_FNS.Name = "checkBoxSendTo_FNS";
            this.checkBoxSendTo_FNS.Size = new System.Drawing.Size(154, 17);
            this.checkBoxSendTo_FNS.TabIndex = 30;
            this.checkBoxSendTo_FNS.Text = "Передать данные в ФНС";
            this.checkBoxSendTo_FNS.UseVisualStyleBackColor = true;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(425, 358);
            this.label28.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(86, 13);
            this.label28.TabIndex = 22;
            this.label28.Text = "Точка рекламы";
            // 
            // comboBoxAdv_Point
            // 
            this.comboBoxAdv_Point.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.comboBoxAdv_Point.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBoxAdv_Point.FormattingEnabled = true;
            this.comboBoxAdv_Point.Location = new System.Drawing.Point(425, 377);
            this.comboBoxAdv_Point.Margin = new System.Windows.Forms.Padding(2);
            this.comboBoxAdv_Point.Name = "comboBoxAdv_Point";
            this.comboBoxAdv_Point.Size = new System.Drawing.Size(103, 21);
            this.comboBoxAdv_Point.TabIndex = 22;
            // 
            // buttonGoAway
            // 
            this.buttonGoAway.Location = new System.Drawing.Point(389, 448);
            this.buttonGoAway.Margin = new System.Windows.Forms.Padding(2);
            this.buttonGoAway.Name = "buttonGoAway";
            this.buttonGoAway.Size = new System.Drawing.Size(126, 25);
            this.buttonGoAway.TabIndex = 30;
            this.buttonGoAway.TabStop = false;
            this.buttonGoAway.Text = "Отойти";
            this.buttonGoAway.UseVisualStyleBackColor = true;
            this.buttonGoAway.Click += new System.EventHandler(this.buttonGoAway_Click);
            // 
            // buttonGoToEdit
            // 
            this.buttonGoToEdit.Location = new System.Drawing.Point(546, 448);
            this.buttonGoToEdit.Margin = new System.Windows.Forms.Padding(2);
            this.buttonGoToEdit.Name = "buttonGoToEdit";
            this.buttonGoToEdit.Size = new System.Drawing.Size(198, 25);
            this.buttonGoToEdit.TabIndex = 31;
            this.buttonGoToEdit.TabStop = false;
            this.buttonGoToEdit.Text = "Редактирование заказов";
            this.buttonGoToEdit.UseVisualStyleBackColor = true;
            this.buttonGoToEdit.Click += new System.EventHandler(this.buttonGoToEdit_Click);
            // 
            // buttonReloadUsers
            // 
            this.buttonReloadUsers.Location = new System.Drawing.Point(6, 481);
            this.buttonReloadUsers.Margin = new System.Windows.Forms.Padding(2);
            this.buttonReloadUsers.Name = "buttonReloadUsers";
            this.buttonReloadUsers.Size = new System.Drawing.Size(126, 39);
            this.buttonReloadUsers.TabIndex = 32;
            this.buttonReloadUsers.TabStop = false;
            this.buttonReloadUsers.Text = "Перезагрузить исполнителей";
            this.buttonReloadUsers.UseVisualStyleBackColor = true;
            this.buttonReloadUsers.Click += new System.EventHandler(this.buttonReloadUsers_Click);
            // 
            // labelSdacha
            // 
            this.labelSdacha.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelSdacha.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelSdacha.Location = new System.Drawing.Point(390, 247);
            this.labelSdacha.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelSdacha.Name = "labelSdacha";
            this.labelSdacha.Size = new System.Drawing.Size(354, 59);
            this.labelSdacha.TabIndex = 111;
            this.labelSdacha.Text = "Сдача: ";
            this.labelSdacha.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelSdacha.Visible = false;
            // 
            // buttonResetUserInfo
            // 
            this.buttonResetUserInfo.Location = new System.Drawing.Point(270, 481);
            this.buttonResetUserInfo.Margin = new System.Windows.Forms.Padding(2);
            this.buttonResetUserInfo.Name = "buttonResetUserInfo";
            this.buttonResetUserInfo.Size = new System.Drawing.Size(114, 39);
            this.buttonResetUserInfo.TabIndex = 28;
            this.buttonResetUserInfo.TabStop = false;
            this.buttonResetUserInfo.Text = "Очистить данные клиента";
            this.buttonResetUserInfo.UseVisualStyleBackColor = true;
            this.buttonResetUserInfo.Click += new System.EventHandler(this.buttonResetUserInfo_Click);
            // 
            // PerformOrderForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(754, 537);
            this.Controls.Add(this.labelSdacha);
            this.Controls.Add(this.buttonReloadUsers);
            this.Controls.Add(this.buttonGoToEdit);
            this.Controls.Add(this.buttonGoAway);
            this.Controls.Add(this.checkBoxSendTo_FNS);
            this.Controls.Add(this.checkBoxIsBeznal);
            this.Controls.Add(this.buttonPerform);
            this.Controls.Add(this.buttonResetUserInfo);
            this.Controls.Add(this.buttonResetPerform);
            this.Controls.Add(this.checkBoxIsSamovivos);
            this.Controls.Add(this.checkBoxIsPovar_Upakov);
            this.Controls.Add(this.checkBoxIsOperator_Prodavec);
            this.Controls.Add(this.comboBoxPoint_Work);
            this.Controls.Add(this.numericUpDownBirthMONTH);
            this.Controls.Add(this.numericUpDownBirthDAY);
            this.Controls.Add(this.comboBoxSelectCurer);
            this.Controls.Add(this.comboBoxSelectUpakov);
            this.Controls.Add(this.comboBoxSelectPovar);
            this.Controls.Add(this.comboBoxPhoneNumber);
            this.Controls.Add(this.textBoxNumber_Flat);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.textBoxNumber_Build);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.textBoxNumber_Corp);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.textBoxCardNumber);
            this.Controls.Add(this.textBoxClientSurname);
            this.Controls.Add(this.textBoxClientName);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.textBoxNumber_Home);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.comboBoxStreet);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.comboBoxAdv_Point);
            this.Controls.Add(this.comboBoxRayon);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.comboBoxCity);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.richTextBoxOrderDescription);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.textBoxClientGaveMoney);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.textBoxSkidka);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.textBoxPrice_With_Skidka);
            this.Controls.Add(this.textBoxOrderTotalSum);
            this.Controls.Add(this.textBoxPromocode);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.numericUpDownCountPersons);
            this.Controls.Add(this.dataGridViewOrderProducts);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonAddProduct);
            this.Controls.Add(this.numericUpDownCountProduct);
            this.Controls.Add(this.textBoxProductPrice);
            this.Controls.Add(this.comboBoxProductName);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "PerformOrderForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Оформление заказа";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.PerformOrderForm_FormClosing);
            this.Load += new System.EventHandler(this.PerformOrderForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownCountProduct)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewOrderProducts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownCountPersons)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownBirthDAY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownBirthMONTH)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBoxProductName;
        private System.Windows.Forms.TextBox textBoxProductPrice;
        private System.Windows.Forms.NumericUpDown numericUpDownCountProduct;
        private System.Windows.Forms.Button buttonAddProduct;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridView dataGridViewOrderProducts;
        private System.Windows.Forms.NumericUpDown numericUpDownCountPersons;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBoxPromocode;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBoxOrderTotalSum;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBoxSkidka;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBoxPrice_With_Skidka;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBoxClientGaveMoney;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.RichTextBox richTextBoxOrderDescription;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox comboBoxCity;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox comboBoxStreet;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox textBoxNumber_Home;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox textBoxNumber_Corp;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox textBoxNumber_Build;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox textBoxNumber_Flat;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox textBoxClientName;
        private System.Windows.Forms.TextBox textBoxClientSurname;
        private System.Windows.Forms.ComboBox comboBoxPhoneNumber;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.ComboBox comboBoxRayon;
        private System.Windows.Forms.NumericUpDown numericUpDownBirthDAY;
        private System.Windows.Forms.NumericUpDown numericUpDownBirthMONTH;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox textBoxCardNumber;
        private System.Windows.Forms.ComboBox comboBoxPoint_Work;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.CheckBox checkBoxIsOperator_Prodavec;
        private System.Windows.Forms.CheckBox checkBoxIsPovar_Upakov;
        private System.Windows.Forms.CheckBox checkBoxIsSamovivos;
        private System.Windows.Forms.ComboBox comboBoxSelectPovar;
        private System.Windows.Forms.ComboBox comboBoxSelectUpakov;
        private System.Windows.Forms.ComboBox comboBoxSelectCurer;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Button buttonResetPerform;
        private System.Windows.Forms.CheckBox checkBoxIsBeznal;
        private System.Windows.Forms.Button buttonPerform;
        private System.Windows.Forms.CheckBox checkBoxSendTo_FNS;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.ComboBox comboBoxAdv_Point;
        private System.Windows.Forms.Button buttonGoAway;
        private System.Windows.Forms.Button buttonGoToEdit;
        private System.Windows.Forms.Button buttonReloadUsers;
        private System.Windows.Forms.Label labelSdacha;
        private System.Windows.Forms.Button buttonResetUserInfo;
    }
}