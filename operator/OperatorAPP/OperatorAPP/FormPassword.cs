﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OperatorAPP
{
    public partial class FormPassword : Form
    {
        public FormPassword()
        {
            InitializeComponent();
        }

        private void FormPassword_FormClosed(object sender, FormClosedEventArgs e)
        {
            
        }

        private void buttonInput_Click(object sender, EventArgs e)
        {
            string password = DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString();

            if (textBoxPass.Text == password)
            {
                this.DialogResult = DialogResult.OK;
                MessageBox.Show("Пароль верный!");
                this.Close();
            }
            else
            {
                MessageBox.Show("Пароль НЕверный!");
            }

        }

        private void buttonClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }
    }
}
