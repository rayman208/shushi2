﻿using SushiLib;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Threading;
using System.Windows.Forms;

using System.IO;

namespace OperatorAPP
{
    public partial class PerformOrderForm : Form
    {
        
        

        #region переменные и функции для печати чека - Алексей
        int myCheckLenght = 29;
        string PrintText = String.Empty;
     

        public string CreateProductRecordInCheck(Tuple<Product, int> currentProduct, int strLenght)
        {
            string lines = String.Empty;
            string currentLine = String.Empty;

            for (int i = 0; i < currentProduct.Item1.name.Length; i++)
            {
                if (currentLine.Length == strLenght)
                {
                    lines += currentLine + "\n";
                    currentLine = String.Empty;
                }
                currentLine += currentProduct.Item1.name[i];
            }
            string priceLine = currentLine+"\n";
            string tempPriceLine = " x " + currentProduct.Item2 + " шт. = ";
            tempPriceLine += ((currentProduct.Item1.price * currentProduct.Item2) + "р.");
            lines += priceLine+tempPriceLine;

            return lines;
        }

        public string CreateAppendicsCheck(int strLenght)
        {
            try
            {
                string text = File.ReadAllText("check_text.txt");

                string lines = String.Empty;
                string currentLine = String.Empty;

                for (int i = 0; i < text.Length; i++)
                {
                    if (currentLine.Length == strLenght)
                    {
                        lines += currentLine.Trim(' ') + "\n";
                        currentLine = String.Empty;
                    }
                    currentLine += text[i];
                }
                lines+=currentLine.Trim(' ') + "\n\n";
                lines += "СПАСИБО ЗА ПОКУПКУ !\n\n.";
                return lines;
            }
            catch
            {
                return "\nСПАСИБО ЗА ПОКУПКУ !\n\n.";
            }
        }

        public void PrintPageHandler(object sender, PrintPageEventArgs e)
        {
            Font lageFont = new Font("Arial", 8);

            int heightLagreFont = (int)e.Graphics.MeasureString("A", lageFont).Height;

            e.Graphics.DrawImage(Properties.Resources.logo, new Rectangle(0, 0, 180, 100));

            e.Graphics.DrawString(PrintText, lageFont, Brushes.Black, 0, 100);

            int yQrCodeImg = heightLagreFont * PrintText.Count(c => c == '\n') +100;

            e.Graphics.DrawImage(Properties.Resources.qr_code,new Rectangle(40, yQrCodeImg,100,100));

            string footerCheck = new string('=', myCheckLenght) + "\n";
            footerCheck +="Сайт чеков: gate.ofd.ru" + "\n";
            footerCheck += "Сайт ФНС: www.nalog.ru" + "\n";
            footerCheck += "ФН №: 8710000101783257" + "\n";
            footerCheck += "ФД №: 0000002668" + "\n";
            footerCheck += "ФП: 0884171289" + "\n";
            footerCheck += new string('=', myCheckLenght) + "\n";

            e.Graphics.DrawString(footerCheck, lageFont, Brushes.Black, 0, yQrCodeImg+100+heightLagreFont/4);
            
            int yAppendics = yQrCodeImg + 100 + heightLagreFont / 4 + heightLagreFont * footerCheck.Count(c => c == '\n');

            e.Graphics.DrawString(CreateAppendicsCheck(myCheckLenght), lageFont, Brushes.Black, 0, yAppendics);
        }

        #endregion

        public PerformOrderForm()
        {
            Graphics dpiGraphics = Graphics.FromHwnd(IntPtr.Zero);
            this.AutoScaleDimensions = new SizeF(dpiGraphics.DpiX, dpiGraphics.DpiX);
            this.AutoScaleMode = AutoScaleMode.Dpi;
            dpiGraphics.Dispose();
            InitializeComponent();
        }

        private List<Tuple<Product, int>> CurrentOrderProducts = new List<Tuple<Product, int>>();

        private int CurrentProductSelectedIndex = -1;

        //Изменяется ли сейчас состояние CheckBox-ов 
        private bool isNowCheckedChanging = false;

        //Нужно ли выполнять выход пользователя при закрытии формы
        private bool ExitUser = true;

        private int GetSkidkaProcent()
        {
            int skidka = 0;
            int.TryParse(textBoxSkidka.Text, out skidka);
            if (skidka > 100)
            {
                skidka = 100;
            }
            return skidka;
        }

        private int RecountOrderTotalPrice()
        {
            int sum = 0;
            for (int i = 0; i < CurrentOrderProducts.Count; i++)
            {
                sum += CurrentOrderProducts[i].Item1.price * CurrentOrderProducts[i].Item2;
            }
            return sum;
        }

        private int CountSumWithSkidka(int sum, int skidka)
        {
            int result = (sum * (100 - skidka)) / 100;
            return result;
        }

        private void RefillUsersComboBoxes()
        {
            comboBoxSelectPovar.Items.Clear();
            comboBoxSelectCurer.Items.Clear();
            comboBoxSelectUpakov.Items.Clear();

            int Povar_Dolgnost_ID = LocalData.dolgnosti.FirstOrDefault(d => d.name == "Повар")?.id ?? -1;
            int Upakov_Dolgnost_ID = LocalData.dolgnosti.FirstOrDefault(d => d.name == "Упаковщик")?.id ?? -1;
            int Curer_Dolgnost_ID = LocalData.dolgnosti.FirstOrDefault(d => d.name == "Курьер")?.id ?? -1;

            if (Povar_Dolgnost_ID != -1)
            {
                comboBoxSelectPovar.Items.AddRange(LocalData.users.Where(t =>
                    t.id_dolgnost == Povar_Dolgnost_ID &&
                    t.id_point_work == LocalData.CurrentPoint_Work.id &&
                    t.online == 1).ToArray()
                    );

                if (comboBoxSelectPovar.Items.Count > 0)
                {
                    comboBoxSelectPovar.SelectedIndex = 0;
                }
                else
                {
                    comboBoxSelectPovar.Text = string.Empty;
                }
            }

            if (Upakov_Dolgnost_ID != -1)
            {
                comboBoxSelectUpakov.Items.AddRange(LocalData.users.Where(t =>
                    t.id_dolgnost == Upakov_Dolgnost_ID &&
                    t.id_point_work == LocalData.CurrentPoint_Work.id &&
                    t.online == 1).ToArray()
                    );

                if (comboBoxSelectUpakov.Items.Count > 0)
                {
                    comboBoxSelectUpakov.SelectedIndex = 0;
                }
                else
                {
                    comboBoxSelectUpakov.Text = string.Empty;
                }
            }
            if (Curer_Dolgnost_ID != -1)
            {
                comboBoxSelectCurer.Items.AddRange(LocalData.users.Where(t =>
                    t.id_dolgnost == Curer_Dolgnost_ID &&
                    t.online == 1).ToArray()
                    );

                if (comboBoxSelectCurer.Items.Count > 0)
                {
                    comboBoxSelectCurer.SelectedIndex = 0;
                }
                else
                {
                    comboBoxSelectCurer.Text = string.Empty;
                }
            }
        }

        private void ReFillBoxTotalOrderPrice()
        {
            textBoxOrderTotalSum.Text = RecountOrderTotalPrice().ToString() + "р";
        }

        private void ReFillBoxPriceWithSkidka(int SumWithSkidka)
        {
            textBoxPrice_With_Skidka.Text = SumWithSkidka.ToString() + "р";
        }

        private void ClearUserInfo()
        {
            //ЧИСТИМ ПОЛЬЗОВАТЕЛЯ И АДРЕС
            //______________________________________
            comboBoxPhoneNumber.SelectedIndex = -1;
            comboBoxCity.SelectedIndex = -1;
            comboBoxStreet.SelectedIndex = -1;
            comboBoxRayon.SelectedIndex = -1;
            comboBoxAdv_Point.SelectedIndex = -1;

            comboBoxPhoneNumber.Text = string.Empty;
            comboBoxCity.Text = string.Empty;
            comboBoxStreet.Text = string.Empty;
            comboBoxRayon.Text = string.Empty;
            comboBoxAdv_Point.Text = string.Empty;

            textBoxClientName.Text = string.Empty;
            textBoxClientSurname.Text = string.Empty;
            numericUpDownBirthDAY.Value = numericUpDownBirthDAY.Minimum;
            numericUpDownBirthMONTH.Value = numericUpDownBirthMONTH.Minimum;

            textBoxCardNumber.Text = string.Empty;
            textBoxNumber_Home.Text = string.Empty;
            textBoxNumber_Corp.Text = string.Empty;
            textBoxNumber_Build.Text = string.Empty;
            textBoxNumber_Flat.Text = string.Empty;
            //______________________________________
        }

        private void ClearAllFields()
        {
            //ЧИСТИМ ЗАКАЗ
            //______________________________________
            comboBoxProductName.SelectedIndex = -1;

            comboBoxProductName.Text = string.Empty;

            textBoxProductPrice.Text = string.Empty;

            textBoxClientGaveMoney.Text = string.Empty;

            numericUpDownCountProduct.Value = numericUpDownCountProduct.Minimum;

            CurrentOrderProducts.Clear();
            dataGridViewOrderProducts.Rows.Clear();

            textBoxOrderTotalSum.Text = string.Empty;
            textBoxSkidka.Text = string.Empty;
            textBoxPrice_With_Skidka.Text = string.Empty;

            labelSdacha.Text = "Сдача: ";
            labelSdacha.Visible = false;
            //______________________________________

            textBoxPromocode.Clear();
            numericUpDownCountPersons.Value = numericUpDownCountPersons.Minimum;
            richTextBoxOrderDescription.Clear();

            ClearUserInfo();

            //ЧИСТИМ СИСТЕМНУЮ ИНФОРМАЦИЮ
            //______________________________________

            checkBoxIsOperator_Prodavec.Checked = false;
            checkBoxIsPovar_Upakov.Checked = false;
            checkBoxIsSamovivos.Checked = false;
            checkBoxIsBeznal.Checked = false;
            //______________________________________
        }

        private void ShowSdachaLabel()
        {
            //Если текст пустой
            if (textBoxClientGaveMoney.Text.Length == 0)
            {
                labelSdacha.Text = "Сдача: ";
                labelSdacha.Visible = false;
                return;
            }
            //Если не безнал
            if (!checkBoxIsBeznal.Checked)
            {
                //Получаем сумму заказа
                int OrderPrice = RecountOrderTotalPrice();
                int Skidka = GetSkidkaProcent();
                int OrderPriceWithSkidka = CountSumWithSkidka(OrderPrice, Skidka);

                //Получаем количество денег клиента
                int ClientMoney = 0;
                if (checkBoxIsBeznal.Checked)
                {
                    ClientMoney = OrderPriceWithSkidka;
                }
                else
                {
                    int.TryParse(textBoxClientGaveMoney.Text, out ClientMoney);
                }

                //Считаем сдачу
                int Sdacha = (ClientMoney - OrderPriceWithSkidka);
                if (Sdacha >= 0)
                {
                    labelSdacha.Text = "Сдача: " + Sdacha + "р";
                }
                else
                {
                    labelSdacha.Text = "Введённая сумма меньше стоимости заказа";
                }
                labelSdacha.Visible = true;
            }
            else//если безнал
            {
                labelSdacha.Text = "Сдача: ";
                labelSdacha.Visible = false;
            }
        }

        private void PerformOrderForm_Load(object sender, EventArgs e)
        {

            textBoxClientSurname.Top = -100;
            numericUpDownBirthDAY.Top = -100;
            numericUpDownBirthMONTH.Top = -100;

            comboBoxProductName.Items.AddRange(LocalData.products.ToArray());
            comboBoxPhoneNumber.Items.AddRange(LocalData.clients.ToArray());
            comboBoxCity.Items.AddRange(LocalData.cities.ToArray());
            comboBoxStreet.Items.AddRange(LocalData.streets.ToArray());
            comboBoxRayon.Items.AddRange(LocalData.rayons.ToArray());
            comboBoxPoint_Work.Items.AddRange(LocalData.points_work.ToArray());
            comboBoxAdv_Point.Items.AddRange(LocalData.advpoints.ToArray());

            RefillUsersComboBoxes();

            comboBoxPoint_Work.SelectedItem = LocalData.CurrentPoint_Work;

            ExitUser = true;

            this.Text = ExitUser.ToString();
        }

        private void comboBoxProductName_SelectedIndexChanged(object sender, EventArgs e)
        {
            Product selectedProduct = comboBoxProductName.SelectedItem as Product;
            if (selectedProduct != null)
            {
                textBoxProductPrice.Text = selectedProduct.price.ToString() + "р";
            }
        }

        private void comboBoxPhoneNumber_SelectedValueChanged(object sender, EventArgs e)
        {
            Client selectedClient = comboBoxPhoneNumber.SelectedItem as Client;
            if (selectedClient != null)
            {
                textBoxClientName.Text = selectedClient.first_name;
                textBoxClientSurname.Text = selectedClient.last_name;
                numericUpDownBirthDAY.Value = selectedClient.bd_day;
                numericUpDownBirthMONTH.Value = selectedClient.bd_month;
                textBoxCardNumber.Text = selectedClient.number_card;

                if (selectedClient.id_city != -1)
                {
                    City clientCity = LocalData.cities.FirstOrDefault(t => t.id == selectedClient.id_city);
                    if (clientCity != null)
                    {
                        comboBoxCity.SelectedItem = clientCity;
                    }
                }
                else
                {
                    comboBoxCity.SelectedIndex = -1;
                }

                if (selectedClient.id_street != -1)
                {
                    Street clientStreet = LocalData.streets.FirstOrDefault(t => t.id == selectedClient.id_street);
                    if (clientStreet != null)
                    {
                        comboBoxStreet.SelectedItem = clientStreet;
                    }
                }
                else
                {
                    comboBoxStreet.SelectedIndex = -1;
                }
                if (selectedClient.id_rayon != -1)
                {
                    Rayon clientRayon = LocalData.rayons.FirstOrDefault(t => t.id == selectedClient.id_rayon);
                    if (clientRayon != null)
                    {
                        comboBoxRayon.SelectedItem = clientRayon;
                    }
                }
                else
                {
                    comboBoxRayon.SelectedIndex = -1;
                }
                if (selectedClient.number_home != -1)
                {
                    textBoxNumber_Home.Text = selectedClient.number_home.ToString();
                }
                else
                {
                    textBoxNumber_Home.Text = string.Empty;
                }
                if (selectedClient.number_corp != -1)
                {
                    textBoxNumber_Corp.Text = selectedClient.number_corp.ToString();
                }
                else
                {
                    textBoxNumber_Corp.Text = string.Empty;
                }
                if (selectedClient.number_flat != -1)
                {
                    textBoxNumber_Flat.Text = selectedClient.number_flat.ToString();
                }
                else
                {
                    textBoxNumber_Flat.Text = string.Empty;
                }
                if (selectedClient.id_advpoint != -1)
                {
                    AdvPoint adv = LocalData.advpoints.FirstOrDefault(t => t.id == selectedClient.id_advpoint);
                    if (adv != null)
                    {
                        comboBoxAdv_Point.SelectedItem = adv;
                    }
                }
                else
                {
                    comboBoxAdv_Point.SelectedIndex = -1;
                }
            }
            else
            {
                textBoxClientName.Text = string.Empty;
                textBoxClientSurname.Text = string.Empty;
                numericUpDownBirthDAY.Value = numericUpDownBirthDAY.Minimum;
                numericUpDownBirthMONTH.Value = numericUpDownBirthMONTH.Minimum;
                textBoxCardNumber.Text = string.Empty;
                comboBoxCity.SelectedIndex = -1;
                comboBoxStreet.SelectedIndex = -1;
                comboBoxRayon.SelectedIndex = -1;
                textBoxNumber_Home.Text = string.Empty;
                textBoxNumber_Corp.Text = string.Empty;
                textBoxNumber_Build.Text = string.Empty;
                textBoxNumber_Flat.Text = string.Empty;
                comboBoxAdv_Point.SelectedIndex = -1;
            }
        }

        private void comboBoxPhoneNumber_Leave(object sender, EventArgs e)
        {
            if (comboBoxPhoneNumber.SelectedItem != null)
            {
                comboBoxPhoneNumber_SelectedValueChanged(null, null);
            }
        }

        private void buttonAddProduct_Click(object sender, EventArgs e)
        {
            Product selectedProduct = comboBoxProductName.SelectedItem as Product;
            int countProduct = (int)numericUpDownCountProduct.Value;
            if (selectedProduct != null)
            {
                if (CurrentProductSelectedIndex == -1)
                {
                    CurrentOrderProducts.Add(new Tuple<Product, int>(selectedProduct, countProduct));
                    dataGridViewOrderProducts.Rows.Add(selectedProduct.id, selectedProduct.name, selectedProduct.price, countProduct, selectedProduct.price * countProduct);
                }
                else
                {
                    CurrentOrderProducts[CurrentProductSelectedIndex] = new Tuple<Product, int>(selectedProduct, countProduct);

                    DataGridViewRow row = dataGridViewOrderProducts.Rows[CurrentProductSelectedIndex];
                    row.Cells[0].Value = selectedProduct.id;
                    row.Cells[1].Value = selectedProduct.name;
                    row.Cells[2].Value = selectedProduct.price;
                    row.Cells[3].Value = countProduct;
                    row.Cells[4].Value = selectedProduct.price * countProduct;

                    buttonAddProduct.Text = "Добавить продукт";
                    dataGridViewOrderProducts.ClearSelection();
                    CurrentProductSelectedIndex = -1;
                }
                int SumWithSkidka = CountSumWithSkidka(RecountOrderTotalPrice(), GetSkidkaProcent());
                ReFillBoxTotalOrderPrice();
                ReFillBoxPriceWithSkidka(SumWithSkidka);
                numericUpDownCountProduct.Value = numericUpDownCountProduct.Minimum;
            }
        }

        private void textBoxSkidka_TextChanged(object sender, EventArgs e)
        {
            int SumWithSkidka = CountSumWithSkidka(RecountOrderTotalPrice(), GetSkidkaProcent());
            ReFillBoxPriceWithSkidka(SumWithSkidka);
            if (checkBoxIsBeznal.Checked)
            {
                textBoxClientGaveMoney.Text = SumWithSkidka.ToString();
                textBoxClientGaveMoney.Enabled = false;
            }
            ShowSdachaLabel();
        }

        private void comboBoxProductName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Return)
            {
                Product selectedProduct = comboBoxProductName.SelectedItem as Product;
                if (selectedProduct != null)
                {
                    textBoxProductPrice.Text = selectedProduct.price.ToString() + "р";
                }
                if (!comboBoxProductName.DroppedDown)
                {
                    comboBoxProductName.DroppedDown = false;
                    e.SuppressKeyPress = true;
                    e.Handled = false;
                    buttonAddProduct.PerformClick();
                }
            }
            if (e.KeyCode == Keys.Add)
            {
                e.SuppressKeyPress = true;
                e.Handled = false;
                if (numericUpDownCountProduct.Value != numericUpDownCountProduct.Maximum)
                {
                    numericUpDownCountProduct.Value++;
                    comboBoxProductName_SelectedIndexChanged(null, null);
                }
            }
            if (e.KeyCode == Keys.Subtract)
            {
                e.SuppressKeyPress = true;
                e.Handled = false;
                if (numericUpDownCountProduct.Value != numericUpDownCountProduct.Minimum)
                {
                    numericUpDownCountProduct.Value--;
                    comboBoxProductName_SelectedIndexChanged(null, null);
                }
            }
        }

        private void buttonResetPerform_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Отменить это действие будет невозможно!\nПродолжить?", "Действительно сбросить оформление", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                ClearAllFields();
            }
        }

        private void dataGridViewOrderProducts_CellClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void checkBoxIsBeznal_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxIsBeznal.Checked)
            {
                textBoxClientGaveMoney.Text = CountSumWithSkidka(RecountOrderTotalPrice(), GetSkidkaProcent()).ToString();
                textBoxClientGaveMoney.Enabled = false;
            }
            else
            {
                textBoxClientGaveMoney.Text = string.Empty;
                textBoxClientGaveMoney.Enabled = true;
            }
        }

        private void richTextBoxOrderDescription_TextChanged(object sender, EventArgs e)
        {
            int cursorPos = richTextBoxOrderDescription.SelectionStart;
            if (richTextBoxOrderDescription.TextLength >= 1000)
            {
                richTextBoxOrderDescription.Text = richTextBoxOrderDescription.Text.Substring(0, 1000);
                richTextBoxOrderDescription.SelectionStart = cursorPos;
            }
            label10.Text = "Примечание к заказу ( не более 1000 символов) (" + richTextBoxOrderDescription.TextLength.ToString("0000") + " / " + "1000)";
        }

        private void dataGridViewOrderProducts_CellMouseDown(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                if (e.ColumnIndex != -1 && e.RowIndex != -1)
                {
                    dataGridViewOrderProducts[e.ColumnIndex, e.RowIndex].Selected = true;
                }
                ContextMenuStrip cms = new ContextMenuStrip();
                int RowIndex = e.RowIndex;
                cms.Items.Add("Удалить").Click += (t, ev) =>
                {
                    if (RowIndex != -1)
                    {
                        numericUpDownCountProduct.Value = numericUpDownCountProduct.Minimum;
                        buttonAddProduct.Text = "Добавить продукт";
                        CurrentOrderProducts.RemoveAt(RowIndex);
                        dataGridViewOrderProducts.Rows.RemoveAt(RowIndex);
                        int SumWithSkidka = CountSumWithSkidka(RecountOrderTotalPrice(), GetSkidkaProcent());
                        ShowSdachaLabel();
                        ReFillBoxTotalOrderPrice();
                        ReFillBoxPriceWithSkidka(SumWithSkidka);
                        dataGridViewOrderProducts.ClearSelection();

                        CurrentProductSelectedIndex = -1;
                        comboBoxProductName.SelectedIndex = -1;
                    }
                    cms.Dispose();
                };

                cms.Items.Add("Изменить").Click += (t, ev) =>
                {
                    if (RowIndex >= 0 && RowIndex < CurrentOrderProducts.Count)
                    {
                        Tuple<Product, int> productInfo = CurrentOrderProducts[RowIndex];
                        CurrentProductSelectedIndex = RowIndex;
                        comboBoxProductName.SelectedItem = productInfo.Item1;
                        numericUpDownCountProduct.Value = productInfo.Item2;
                        buttonAddProduct.Text = "Сохранить продукт";
                    }
                    cms.Dispose();
                };


                cms.Show(MousePosition.X, MousePosition.Y);
            }
        }

        private void comboBoxPoint_Work_SelectedIndexChanged(object sender, EventArgs e)
        {
            Point_Work SelectedPointWork = comboBoxPoint_Work.SelectedItem as Point_Work;
            if (SelectedPointWork != null)
            {
                LocalData.CurrentPoint_Work = SelectedPointWork;
                RefillUsersComboBoxes();
            }
        }

        private async void buttonPerform_Click(object sender, EventArgs e)
        {
            #region Если продуктов в заказе нет
            if (CurrentOrderProducts.Count == 0)
            {
                //Алексей
                MessageBox.Show("Продукты для заказа не введены!");
                return;
            }
            #endregion

            #region Получаем Суммы для заказа

            int OrderPrice = RecountOrderTotalPrice();
            int Skidka = GetSkidkaProcent();
            int OrderPriceWithSkidka = CountSumWithSkidka(OrderPrice, Skidka);
            #endregion
            # region Получаем количество денег, которые дал клиент

            //Алексей
            //if (textBoxClientGaveMoney.Text =="" && checkBoxIsSamovivos.Checked==true)
            //{
            //    MessageBox.Show("Поле \"клиент дал денег\" не заполнено!");
            //    return;
            //}

            int ClientMoney = 0;
            if (checkBoxIsBeznal.Checked)
            {
                ClientMoney = OrderPriceWithSkidka;
            }
            else
            {
                if (checkBoxIsSamovivos.Checked == true)
                {
                    bool temp = int.TryParse(textBoxClientGaveMoney.Text, out ClientMoney);
                    //Алексей
                    if (temp == false)
                    {
                        MessageBox.Show("Поле \"клиент дал денег\" заполнено неверно!");
                        return;
                    }
                }
                else
                {
                    ClientMoney = -1;
                }
            }
            #endregion

            #region Проверка работников -Алексей
            if (checkBoxIsOperator_Prodavec.Checked == false &&
                checkBoxIsPovar_Upakov.Checked == false && checkBoxIsSamovivos.Checked == false)
            {
                if (comboBoxSelectCurer.Text == "" || comboBoxSelectPovar.Text == "" || comboBoxSelectUpakov.Text == "")
                {
                    MessageBox.Show("Ошибка. Сотрудники не выбраны!");
                    return;
                }
            }
            else if (checkBoxIsOperator_Prodavec.Checked == false &&
                checkBoxIsPovar_Upakov.Checked == true && checkBoxIsSamovivos.Checked == false)
            {
                if (comboBoxSelectCurer.Text == "" || comboBoxSelectPovar.Text == "")
                {
                    MessageBox.Show("Ошибка. Сотрудники не выбраны!");
                    return;
                }
            }
            else if (checkBoxIsOperator_Prodavec.Checked == false &&
                checkBoxIsPovar_Upakov.Checked == true && checkBoxIsSamovivos.Checked == true)
            {
                if ( comboBoxSelectPovar.Text == "")
                {
                    MessageBox.Show("Ошибка. Сотрудники не выбраны!");
                    return;
                }
            }
            else if (checkBoxIsOperator_Prodavec.Checked == false &&
                checkBoxIsPovar_Upakov.Checked == false && checkBoxIsSamovivos.Checked == true)
            {
                if (comboBoxSelectPovar.Text == "" || comboBoxSelectUpakov.Text == "")
                {
                    MessageBox.Show("Ошибка. Сотрудники не выбраны!");
                    return;
                }
            }
            #endregion


            #region Получаем количество персон
            int CountPersons = (int)numericUpDownCountPersons.Value;
            #endregion
            #region Получаем описание заказа
            string Description = richTextBoxOrderDescription.Text;
            #endregion
            #region Получаем промокод
            string Promocode = textBoxPromocode.Text;
            #endregion
            #region Получаем параметр No_Cash
            string IsBeznal = checkBoxIsBeznal.Checked ? "1" : "0";
            #endregion

            #region City
            City currentCity = comboBoxCity.SelectedItem as City;
            int CityID = 0;
            if (currentCity == null)
            {
                if (comboBoxCity.Text == "")
                {
                    CityID = -1;
                }
                else
                {
                    string CityName = comboBoxCity.Text;
                    CityID = await API.cities_insert_new_city(LocalData.CurrentUser.id.ToString(), CityName);
                    if (API.IsLastRequestInternetError)
                    {
                        MessageBox.Show("Во время создания города произошла ошибка на сервере.\nСоздание заказа отменено!");
                        return;
                    }
                    LocalData.cities.Add(new City() { id = CityID, name = CityName });
                }
            }
            else
            {
                CityID = currentCity.id;
            }
            #endregion
            #region Street
            Street currentStreet = comboBoxStreet.SelectedItem as Street;
            int StreetID = 0;
            if (currentStreet == null)
            {
                if (comboBoxStreet.Text == "")
                {
                    StreetID = -1;
                }
                else
                {
                    string StreetName = comboBoxStreet.Text;
                    StreetID = await API.streets_insert_new_street(LocalData.CurrentUser.id.ToString(), StreetName);
                    if (API.IsLastRequestInternetError)
                    {
                        MessageBox.Show("Во время создания улицы произошла ошибка на сервере.\nСоздание заказа отменено!");
                        return;
                    }
                    LocalData.streets.Add(new Street() { id = StreetID, name = StreetName });
                }
            }
            else
            {
                StreetID = currentStreet.id;
            }
            #endregion
            #region Rayon
            Rayon currentRayon = comboBoxRayon.SelectedItem as Rayon;
            int RayonID = 0;
            if (currentRayon == null)
            {
                if (comboBoxRayon.Text == "")
                {
                    RayonID = -1;
                }
                else
                {
                    string RayonName = comboBoxRayon.Text;
                    RayonID = await API.rayons_insert_new_rayon(LocalData.CurrentUser.id.ToString(), RayonName);
                    if (API.IsLastRequestInternetError)
                    {
                        MessageBox.Show("Во время создания района произошла ошибка на сервере.\nСоздание заказа отменено!");
                        return;
                    }
                    LocalData.rayons.Add(new Rayon() { id = RayonID, name = RayonName });
                }
            }
            else
            {
                RayonID = currentRayon.id;
            }
            #endregion
            #region Adv_Point
            AdvPoint currentAdvPoint = comboBoxAdv_Point.SelectedItem as AdvPoint;
            int AdvPointID = 0;
            if (currentAdvPoint == null)
            {
                if (comboBoxAdv_Point.Text.Length == 0)
                {
                    AdvPointID = -1;
                }
                else
                {
                    string AdvPointName = comboBoxAdv_Point.Text;
                    AdvPointID = await API.advpoints_insert_new_advpoint(LocalData.CurrentUser.id.ToString(), AdvPointName);
                    if (API.IsLastRequestInternetError)
                    {
                        MessageBox.Show("Во время создания точки рекламы произошла ошибка на сервере.\nСоздание заказа отменено!");
                        return;
                    }
                    LocalData.advpoints.Add(new AdvPoint() { id = AdvPointID, name = AdvPointName });
                }
            }
            else
            {
                AdvPointID = currentRayon.id;
            }
            #endregion

            #region Работа с клиентом

            Client currentClient = comboBoxPhoneNumber.SelectedItem as Client;
            int ClientID = 0;

            if (comboBoxPhoneNumber.Text == string.Empty)
            {
                ClientID = -1;
                MessageBox.Show("Не заполнен телефон клиента");
                return;
            }
            else
            {
                //Если выбранный клиент не существует 
                if (currentClient == null)
                {
                    if (comboBoxPhoneNumber.Text.Length != 0)//Если заполнен номер телефона
                    {
                        int number_home = 0;
                        if (!int.TryParse(textBoxNumber_Home.Text, out number_home))
                        {
                            number_home = -1;
                        }
                        int number_corp = 0;
                        if (!int.TryParse(textBoxNumber_Corp.Text, out number_corp))
                        {
                            number_corp = -1;
                        }
                        int number_build = 0;
                        if (!int.TryParse(textBoxNumber_Build.Text, out number_build))
                        {
                            number_build = -1;
                        }
                        int number_flat = 0;
                        if (!int.TryParse(textBoxNumber_Flat.Text, out number_flat))
                        {
                            number_flat = -1;
                        }
                        //Создать пользователя
                        ClientID = await API.clients_insert_new_client(
                            comboBoxPhoneNumber.Text,
                            textBoxClientName.Text,
                            textBoxClientSurname.Text,
                            CityID.ToString(),
                            RayonID.ToString(),
                            StreetID.ToString(),
                            number_home.ToString(),
                            number_corp.ToString(),
                            number_build.ToString(),
                            number_flat.ToString(),
                            numericUpDownBirthDAY.Value.ToString(),
                            numericUpDownBirthMONTH.Value.ToString(),
                            textBoxCardNumber.Text,
                            AdvPointID.ToString(),
                            LocalData.CurrentUser.id.ToString()
                            );
                        if (API.IsLastRequestInternetError)
                        {
                            MessageBox.Show("Во время создания клиента произошла ошибка на сервере.\nСоздание заказа отменено!");
                            return;
                        }
                        currentClient = new Client()
                        {
                            id = ClientID,
                            number_phone = comboBoxPhoneNumber.Text,
                            first_name = textBoxClientName.Text,
                            last_name = textBoxClientSurname.Text,
                            id_city = CityID,
                            id_rayon = RayonID,
                            id_street = StreetID,
                            number_home = number_home,
                            number_corp = number_corp,
                            number_build = number_build,
                            number_flat = number_flat,
                            bd_day = (int)numericUpDownBirthDAY.Value,
                            bd_month = (int)numericUpDownBirthMONTH.Value,
                            number_card = textBoxCardNumber.Text,
                            id_advpoint = AdvPointID
                        };
                        LocalData.clients.Add(currentClient);
                        comboBoxPhoneNumber.Items.Add(currentClient);
                    }
                }
                else
                {
                    int number_home = 0;
                    if (!int.TryParse(textBoxNumber_Home.Text, out number_home))
                    {
                        number_home = -1;
                    }
                    int number_corp = 0;
                    if (!int.TryParse(textBoxNumber_Corp.Text, out number_corp))
                    {
                        number_corp = -1;
                    }
                    int number_build = 0;
                    if (!int.TryParse(textBoxNumber_Build.Text, out number_build))
                    {
                        number_build = -1;
                    }
                    int number_flat = 0;
                    if (!int.TryParse(textBoxNumber_Flat.Text, out number_flat))
                    {
                        number_flat = -1;
                    }
                    //Если хотя бы 1 параметр изменился     
                    if (currentClient.bd_day != (int)numericUpDownBirthDAY.Value ||
                        currentClient.bd_month != (int)numericUpDownBirthMONTH.Value ||
                        currentClient.first_name != textBoxClientName.Text ||
                        currentClient.last_name != textBoxClientSurname.Text ||
                        currentClient.id_city != CityID ||
                        currentClient.id_rayon != RayonID ||
                        currentClient.id_street != StreetID ||
                        currentClient.id_advpoint != AdvPointID ||
                        currentClient.number_build != number_build ||
                        currentClient.number_card.ToString() != textBoxCardNumber.Text ||
                        currentClient.number_corp != number_corp ||
                        currentClient.number_home != number_home ||
                        currentClient.number_flat != number_flat)
                    {
                        bool UpdateResult = await API.clients_update_by_id(currentClient.id.ToString(), comboBoxPhoneNumber.Text, textBoxClientName.Text, textBoxClientSurname.Text, CityID.ToString(), RayonID.ToString(), StreetID.ToString(), textBoxNumber_Home.Text, textBoxNumber_Corp.Text, textBoxNumber_Build.Text, textBoxNumber_Flat.Text, numericUpDownBirthDAY.Value.ToString(), numericUpDownBirthMONTH.Value.ToString(), textBoxCardNumber.Text, AdvPointID.ToString(), LocalData.CurrentUser.id.ToString());
                        currentClient.number_phone = comboBoxPhoneNumber.Text;
                        currentClient.first_name = textBoxClientName.Text;
                        currentClient.last_name = textBoxClientSurname.Text;
                        currentClient.id_city = CityID;
                        currentClient.id_rayon = RayonID;
                        currentClient.id_street = StreetID;
                        currentClient.number_home = number_home;
                        currentClient.number_corp =  number_corp;
                        currentClient.number_build = number_build;
                        currentClient.number_flat = number_flat;
                        currentClient.bd_day = (int)numericUpDownBirthDAY.Value;
                        currentClient.bd_month = (int)numericUpDownBirthMONTH.Value;
                        currentClient.number_card = textBoxCardNumber.Text;
                        currentClient.id_advpoint = AdvPointID;

                        if (!UpdateResult || API.IsLastRequestInternetError)
                        {
                            ClientID = 0;
                            MessageBox.Show("Во время обновления клиента произошла ошибка на сервере.\nСоздание заказа отменено!");
                            return;
                        }
                    }
                    ClientID = currentClient.id;
                }
            }

            #endregion

            #region Определение ID исполнителей
            int PovarID = -1;
            int KurerID = -1;
            int UpakovID = -1;

            #region Работа с checkBox-ами, определение повара, упаковщика, курьера
            if (checkBoxIsOperator_Prodavec.Checked && checkBoxIsPovar_Upakov.Checked && checkBoxIsSamovivos.Checked)
            {
                PovarID = -1;
                KurerID = -1;
                UpakovID = -1;
            }

            // 1 1 0

            else if (checkBoxIsOperator_Prodavec.Checked && checkBoxIsPovar_Upakov.Checked && !checkBoxIsSamovivos.Checked)
            {
                PovarID = -1;
                KurerID = -1;
                UpakovID = -1;
            }

            // 1 0 1

            else if (checkBoxIsOperator_Prodavec.Checked && !checkBoxIsPovar_Upakov.Checked && checkBoxIsSamovivos.Checked)
            {
                PovarID = -1;
                KurerID = -1;
                UpakovID = -1;
            }

            // 1 0 0

            else if (checkBoxIsOperator_Prodavec.Checked && !checkBoxIsPovar_Upakov.Checked && !checkBoxIsSamovivos.Checked)
            {
                PovarID = -1;
                KurerID = -1;
                UpakovID = -1;
            }

            // 0 1 1

            else if (!checkBoxIsOperator_Prodavec.Checked && checkBoxIsPovar_Upakov.Checked && checkBoxIsSamovivos.Checked)
            {
                PovarID = (comboBoxSelectPovar.SelectedItem as User)?.id ?? -1;
                KurerID = -1;
                UpakovID = -1;
            }

            // 0 1 0

            else if (!checkBoxIsOperator_Prodavec.Checked && checkBoxIsPovar_Upakov.Checked && !checkBoxIsSamovivos.Checked)
            {
                PovarID = (comboBoxSelectPovar.SelectedItem as User)?.id ?? -1;
                KurerID = (comboBoxSelectCurer.SelectedItem as User)?.id ?? -1;
                UpakovID = -1;
            }

            // 0 0 1

            else if (!checkBoxIsOperator_Prodavec.Checked && !checkBoxIsPovar_Upakov.Checked && checkBoxIsSamovivos.Checked)
            {
                PovarID = (comboBoxSelectPovar.SelectedItem as User)?.id ?? -1;
                KurerID = -1;
                UpakovID = (comboBoxSelectUpakov.SelectedItem as User)?.id ?? -1;
            }

            // 0 0 0

            else if (!checkBoxIsOperator_Prodavec.Checked && !checkBoxIsPovar_Upakov.Checked && !checkBoxIsSamovivos.Checked)
            {
                PovarID = (comboBoxSelectPovar.SelectedItem as User)?.id ?? -1;
                KurerID = (comboBoxSelectCurer.SelectedItem as User)?.id ?? -1;
                UpakovID = (comboBoxSelectUpakov.SelectedItem as User)?.id ?? -1;
            }
            #endregion

            #endregion

            #region Определение Оператор-Продавец, Повар-Упаковщик, Самовывоз
            string isOperatorProdavec = checkBoxIsOperator_Prodavec.Checked ? "1" : "0";
            string isPovarUpakov = checkBoxIsPovar_Upakov.Checked ? "1" : "0";
            string isSamovivos = checkBoxIsSamovivos.Checked ? "1" : "0";
            #endregion

            #region Определяем начальное состояние заказа
            int id_order_state = checkBoxIsOperator_Prodavec.Checked ? 4 : 1;
            #endregion

                #region Определяем нужно ли отправлять данные в налоговую службу
            string send_fiscal_data = checkBoxSendTo_FNS.Checked ? "1" : "0";
            #endregion


            #region Формирование чека
            //int checkWidth = 50;
            //int checkLeftPartWidth = 25;
            //string Check = string.Empty;
            //Check += "Оператор:".PadRight(checkLeftPartWidth, ' ') + (LocalData.CurrentUser.last_name + " " + LocalData.CurrentUser.first_name).PadLeft(checkWidth - checkLeftPartWidth, ' ') + "\n";
            //Check += "Точка работы:".PadRight(checkLeftPartWidth, ' ') + (LocalData.CurrentPoint_Work.name).PadLeft(checkWidth - checkLeftPartWidth, ' ') + "\n";
            //Check += "Клиент дал денег:".PadRight(checkLeftPartWidth, ' ') + (ClientMoney.ToString()).PadLeft(checkWidth - checkLeftPartWidth, ' ') + "\n";
            //Check += "Количество персон:".PadRight(checkLeftPartWidth, ' ') + (CountPersons.ToString()).PadLeft(checkWidth - checkLeftPartWidth, ' ') + "\n";
            //Check += "Описание (сокр):".PadRight(checkLeftPartWidth, ' ') + ((Description.Length > 20 ? Description.Substring(0, 20) : Description)).PadLeft(checkWidth - checkLeftPartWidth, ' ') + "\n";
            //Check += "Промокод:".PadRight(checkLeftPartWidth, ' ') + (Promocode).PadLeft(checkWidth - checkLeftPartWidth, ' ') + "\n";
            //Check += "Безнал:".PadRight(checkLeftPartWidth, ' ') + (IsBeznal).PadLeft(checkWidth - checkLeftPartWidth, ' ') + "\n";
            //Check += "Город:".PadRight(checkLeftPartWidth, ' ') + (LocalData.cities.FirstOrDefault(t => t.id == CityID)?.name ?? "unknown").PadLeft(checkWidth - checkLeftPartWidth, ' ') + "\n";
            //Check += "Улица:".PadRight(checkLeftPartWidth, ' ') + (LocalData.streets.FirstOrDefault(t => t.id == StreetID)?.name ?? "unknown").PadLeft(checkWidth - checkLeftPartWidth, ' ') + "\n";
            //Check += "Район:".PadRight(checkLeftPartWidth, ' ') + (LocalData.rayons.FirstOrDefault(t => t.id == RayonID)?.name ?? "unknown").PadLeft(checkWidth - checkLeftPartWidth, ' ') + "\n";
            //Check += "Точка рекламы:".PadRight(checkLeftPartWidth, ' ') + (LocalData.advpoints.FirstOrDefault(t => t.id == AdvPointID)?.name ?? "unknown").PadLeft(checkWidth - checkLeftPartWidth, ' ') + "\n";
            //Check += "Клиент:".PadRight(checkLeftPartWidth, ' ') + ("id=" + ClientID + ": " + (currentClient.last_name + " " + currentClient.first_name)).PadLeft(checkWidth - checkLeftPartWidth, ' ') + "\n";
            //Check += "Повар:".PadRight(checkLeftPartWidth, ' ') + ("id=" + PovarID + (povar != null ? ": " : "") + (((povar != null ? povar.last_name + " " : "") + (povar != null ? povar.first_name : "")))).PadLeft(checkWidth - checkLeftPartWidth, ' ') + "\n";
            //Check += "Курьер:".PadRight(checkLeftPartWidth, ' ') + ("id=" + KurerID + (kurer != null ? ": " : "") + (((kurer != null ? kurer.last_name + " " : "") + (kurer != null ? kurer.first_name : "")))).PadLeft(checkWidth - checkLeftPartWidth, ' ') + "\n";
            //Check += "Упаковщик:".PadRight(checkLeftPartWidth, ' ') + ("id=" + UpakovID + (upakov != null ? ": " : "") + (((upakov != null ? upakov.last_name + " " : "") + (upakov != null ? upakov.first_name : "")))).PadLeft(checkWidth - checkLeftPartWidth, ' ') + "\n";
            //Check += "Оператор-Продавец:".PadRight(checkLeftPartWidth, ' ') + (isOperatorProdavec).PadLeft(checkWidth - checkLeftPartWidth, ' ') + "\n";
            //Check += "Повар-Упаковщик:".PadRight(checkLeftPartWidth, ' ') + (isPovarUpakov).PadLeft(checkWidth - checkLeftPartWidth, ' ') + "\n";
            //Check += "Самовывоз:".PadRight(checkLeftPartWidth, ' ') + (isSamovivos).PadLeft(checkWidth - checkLeftPartWidth, ' ') + "\n";
            //Check += "Состояние заказа:".PadRight(checkLeftPartWidth, ' ') + (LocalData.order_states.FirstOrDefault(t => t.id == id_order_state)?.name ?? "unknown").PadLeft(checkWidth - checkLeftPartWidth, ' ') + "\n";
            //Check += "Передать данные в ФНС:".PadRight(checkLeftPartWidth, ' ') + (send_fiscal_data).PadLeft(checkWidth - checkLeftPartWidth, ' ') + "\n";
            //Check += "ID заказа:".PadRight(checkLeftPartWidth, ' ') + (OrderID.ToString()).PadLeft(checkWidth - checkLeftPartWidth, ' ') + "\n";
            //Check += "Список продуктов".PadRight(checkWidth, '-') + "\n";
            //Check += string.Join("\n", CurrentOrderProducts.Select(
            //        order_product =>
            //        //Вывод id и названия продукта с дополнением справа ' ' до checkWidth символов
            //        ("id=" + order_product.Item1.id.ToString() + ": " + order_product.Item1.name).PadRight(checkWidth, ' ') + "\n" +
            //        //Вывод цены, количества и суммы с дополнением слева ' ' до checkWidth символов
            //        (((order_product.Item1.price).ToString() + " * " + (order_product.Item2).ToString() + "шт = " + (order_product.Item1.price * order_product.Item2).ToString()) + "р").PadLeft(checkWidth, ' ') + "\n" +
            //        new string('-', checkWidth)
            //        )) + "\n";
            //Check += new string('_', checkWidth) + "\n";
            //Check += string.Format("{0,-20}{1,5}р", "Цена:", OrderPrice).PadLeft(checkWidth, ' ') + "\n";
            //Check += string.Format("{0,-21}{1,5}", "Скидка:", Skidka + "%").PadLeft(checkWidth, ' ') + "\n";
            //Check += string.Format("{0,-20}{1,5}р", "Цена со скидкой:", OrderPriceWithSkidka).PadLeft(checkWidth, ' ');
            ////MessageBox.Show(Check);


            #endregion

            #region Формирование чека  - Алексей
            Random rnd = new Random(DateTime.Now.Millisecond);

            string myCheckFirstDate = String.Empty;
            string myCheckPointWork = String.Empty;
            string myCheckNextDate = String.Empty;
            string myCheckProducts = String.Empty;
            string myCheckPrice = String.Empty;

            myCheckFirstDate += "Эспресс-бар \"Наши суши\"" + "\n";
            myCheckFirstDate += "ООО \"Деловой Город\"" + "\n";//1 storka
            myCheckFirstDate += "ИНН 3255051367" + "\n";//2 stroka;
            myCheckFirstDate += "ОГРН 1083254013842" + "\n";//3 stroka

            myCheckPointWork = "г.Брянск,ул.Красноармейская,71" + "\n";//4 stroka

            myCheckNextDate = new string('=', myCheckLenght) + "\n";//5 stroka

            myCheckNextDate += ("Смена №" + rnd.Next(10, 100)) + ("Чек №" + rnd.Next(10,10000)).PadLeft(myCheckLenght) + "\n";//6 stroka
            myCheckNextDate += "Оператор: " + (LocalData.CurrentUser.last_name + " " + LocalData.CurrentUser.first_name) + "\n";//7 stroka;
            myCheckNextDate += "Приход: " + DateTime.Now.ToString() + "\n";//8-9 stroka;

            myCheckProducts = new string('=', myCheckLenght) + "\n";//9 stroka

            for (int i = 0; i < CurrentOrderProducts.Count; i++)
            {
                myCheckProducts += CreateProductRecordInCheck(CurrentOrderProducts[i], myCheckLenght) + "\n";
            }
            myCheckPrice = new string('=', myCheckLenght) + "\n";

            myCheckPrice += ("Цена: " + OrderPrice + "р.") + "\n";
            myCheckPrice += ("Скидка: " + Skidka + "%") + "\n";
            myCheckPrice += ("ИТОГО: " + OrderPriceWithSkidka + "р.") + "\n";
            myCheckPrice += "Тип оплаты: ";
            myCheckPrice += IsBeznal == "1" ? "Безналичная" : "Наличная";
            myCheckPrice += "\n";

            myCheckPrice += new string('=', myCheckLenght) + "\n";
            //myCheckPrice +="Сайт: gate.ofd.ru" + "\n";

            PrintText = myCheckFirstDate + myCheckPointWork + myCheckNextDate + myCheckProducts + myCheckPrice;
            #endregion

            #region печать чека - Алексей
            PrintDocument printDoc = new PrintDocument();
            printDoc.PrintPage += PrintPageHandler;
            printDoc.Print();
            //PrintDialog printDialog = new PrintDialog();
            //if (printDialog.ShowDialog() == DialogResult.OK)
            //{
            //    printDialog.PrinterSettings = printDialog.PrinterSettings;
            //    printDoc.PrintPage += PrintPageHandler;
            //    printDoc.Print();
            //}
            #endregion


            #region Создаём заказ

            int OrderID = 0;
            OrderID = await API.orders_insert_new_order(LocalData.CurrentUser.id.ToString(), PovarID.ToString(), UpakovID.ToString(), KurerID.ToString(), ClientID.ToString(), LocalData.CurrentPoint_Work.id.ToString(), OrderPrice.ToString(), Skidka.ToString(), OrderPriceWithSkidka.ToString(), ClientMoney.ToString(), CountPersons.ToString(), Description.ToString(), Promocode, IsBeznal.ToString(), isOperatorProdavec, isPovarUpakov, isSamovivos, id_order_state.ToString(), send_fiscal_data, LocalData.CurrentUser.id.ToString());

            if (API.IsLastRequestInternetError)
            {
                MessageBox.Show("Во время создания заказа произошла ошибка на сервере.\nСоздание заказа отменено!");
                return;
            }

            #endregion

            #region Получение повара, курьера и упаковщика из списка по ID
            User povar = PovarID != -1 ? LocalData.users.FirstOrDefault(t => t.id == PovarID) : null;
            User kurer = KurerID != -1 ? LocalData.users.FirstOrDefault(t => t.id == KurerID) : null;
            User upakov = UpakovID != -1 ? LocalData.users.FirstOrDefault(t => t.id == UpakovID) : null;
            #endregion

            #region Отправка продуктов заказа
            bool Order_ProductsResult = await API.order_products_insert_new_order_products(CurrentOrderProducts, OrderID);
            if (API.IsLastRequestInternetError)
            {
                MessageBox.Show("Во время добавления продуктов к заказу произошла ошибка на сервере.\nСоздание заказа отменено!");
                return;
            }
            #endregion


         


            #region Завершение заказа и очистка полей
            MessageBox.Show("Заказ успешно создан");
            ClearAllFields();
            #endregion
        }

        private void OnSelectPersonCheckedChanged(object sender, EventArgs e)
        {
            if (!isNowCheckedChanging)
            {
                isNowCheckedChanging = true;

                // 1 1 1

                if (checkBoxIsOperator_Prodavec.Checked && checkBoxIsPovar_Upakov.Checked && checkBoxIsSamovivos.Checked)
                {
                    comboBoxSelectCurer.Enabled = false;
                    comboBoxSelectPovar.Enabled = false;
                    comboBoxSelectUpakov.Enabled = false;
                }

                // 1 1 0

                if (checkBoxIsOperator_Prodavec.Checked && checkBoxIsPovar_Upakov.Checked && !checkBoxIsSamovivos.Checked)
                {
                    comboBoxSelectCurer.Enabled = false;
                    comboBoxSelectPovar.Enabled = false;
                    comboBoxSelectUpakov.Enabled = false;
                }

                // 1 0 1

                if (checkBoxIsOperator_Prodavec.Checked && !checkBoxIsPovar_Upakov.Checked && checkBoxIsSamovivos.Checked)
                {
                    comboBoxSelectCurer.Enabled = false;
                    comboBoxSelectPovar.Enabled = false;
                    comboBoxSelectUpakov.Enabled = false;
                }

                // 1 0 0

                if (checkBoxIsOperator_Prodavec.Checked && !checkBoxIsPovar_Upakov.Checked && !checkBoxIsSamovivos.Checked)
                {
                    comboBoxSelectCurer.Enabled = false;
                    comboBoxSelectPovar.Enabled = false;
                    comboBoxSelectUpakov.Enabled = false;
                }

                // 0 1 1

                if (!checkBoxIsOperator_Prodavec.Checked && checkBoxIsPovar_Upakov.Checked && checkBoxIsSamovivos.Checked)
                {
                    comboBoxSelectCurer.Enabled = false;
                    comboBoxSelectPovar.Enabled = true;
                    comboBoxSelectUpakov.Enabled = false;
                }

                // 0 1 0

                if (!checkBoxIsOperator_Prodavec.Checked && checkBoxIsPovar_Upakov.Checked && !checkBoxIsSamovivos.Checked)
                {
                    comboBoxSelectCurer.Enabled = true;
                    comboBoxSelectPovar.Enabled = true;
                    comboBoxSelectUpakov.Enabled = false;
                }

                // 0 0 1

                if (!checkBoxIsOperator_Prodavec.Checked && !checkBoxIsPovar_Upakov.Checked && checkBoxIsSamovivos.Checked)
                {
                    comboBoxSelectCurer.Enabled = false;
                    comboBoxSelectPovar.Enabled = true;
                    comboBoxSelectUpakov.Enabled = true;
                }

                // 0 0 0

                if (!checkBoxIsOperator_Prodavec.Checked && !checkBoxIsPovar_Upakov.Checked && !checkBoxIsSamovivos.Checked)
                {
                    comboBoxSelectCurer.Enabled = true;
                    comboBoxSelectPovar.Enabled = true;
                    comboBoxSelectUpakov.Enabled = true;
                }

                isNowCheckedChanging = false;
            }
        }

        private async void buttonGoAway_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Действительно отойти?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
            {
                return;
            }
            ExitUser = false;
            //bool finish_work_result = await API.users_update_finish_work(LocalData.CurrentUser.id);
            //if (API.IsLastRequestInternetError)
            //{
            //    MessageBox.Show("Во время выхода произошла ошибка на сервере.");
            //}
            Thread thr = new Thread(new ThreadStart(() =>
            {
                Application.Run(new FormPause());
            }));
            thr.SetApartmentState(ApartmentState.STA);
            thr.Start();
            this.Close();
        }

        private async void buttonGoToEdit_Click(object sender, EventArgs e)
        {
            if (CurrentOrderProducts.Count > 0)
            {
                if (MessageBox.Show("Действительно перейти к редактированию?\nТекущий заказ будет сброшен.", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
                {
                    return;
                }
            }
            ExitUser = false;

            //bool finish_work_result = await API.users_update_finish_work(LocalData.CurrentUser.id);
            //if (API.IsLastRequestInternetError)
            //{
            //    MessageBox.Show("Во время выхода произошла ошибка на сервере.");
            //}

            FormPassword formPass = new FormPassword();
            DialogResult result = formPass.ShowDialog();
            if (result  == DialogResult.OK)
            {
                Thread thr = new Thread(new ThreadStart(() =>
                {
                    Application.Run(new EditOrderForm());
                }));
                thr.SetApartmentState(ApartmentState.STA);
                thr.Start();

                this.Close();
            }
        }

        private async void PerformOrderForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (ExitUser)
            {
                bool finish_work_result = await API.users_update_finish_work(LocalData.CurrentUser.id);
                if (API.IsLastRequestInternetError)
                {
                    MessageBox.Show("Во время выхода произошла ошибка на сервере.");
                }
            }
        }

        private void buttonReloadUsers_Click(object sender, EventArgs e)
        {
            LocalData.ReloadUsers();
            RefillUsersComboBoxes();
        }

        private void textBoxClientGaveMoney_TextChanged(object sender, EventArgs e)
        {
            ShowSdachaLabel();
        }

        private void buttonResetUserInfo_Click(object sender, EventArgs e)
        {
            ClearUserInfo();
            MessageBox.Show("Данные клиента очищены");
        }

        private void checkBoxIsOperator_Prodavec_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode==Keys.Enter)
            {
                if (checkBoxIsOperator_Prodavec.Checked == true)
                {
                    checkBoxIsOperator_Prodavec.Checked = false;
                }
                else
                {
                    checkBoxIsOperator_Prodavec.Checked = true;
                }
            }
        }

        private void checkBoxIsPovar_Upakov_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (checkBoxIsPovar_Upakov.Checked == true)
                {
                    checkBoxIsPovar_Upakov.Checked = false;
                }
                else
                {
                    checkBoxIsPovar_Upakov.Checked = true;
                }
            }
        }

        private void checkBoxIsSamovivos_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (checkBoxIsSamovivos.Checked == true)
                {
                    checkBoxIsSamovivos.Checked = false;
                }
                else
                {
                    checkBoxIsSamovivos.Checked = true;
                }
            }
        }
    }
}