﻿using SushiLib;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OperatorAPP
{
    public partial class FormPause : Form
    {
        public FormPause()
        {
            Graphics dpiGraphics = Graphics.FromHwnd(IntPtr.Zero);
            this.AutoScaleDimensions = new SizeF(dpiGraphics.DpiX, dpiGraphics.DpiX);
            this.AutoScaleMode = AutoScaleMode.Dpi;
            dpiGraphics.Dispose();
            InitializeComponent();
        }

        private void FormPause_Load(object sender, EventArgs e)
        {
            this.maskedTextBoxLogin.Text = LocalData.CurrentUser.login;
        }

        private async void buttonEnter_Click(object sender, EventArgs e)
        {
            string Login = maskedTextBoxLogin.Text;
            string Password = maskedTextBoxPassword.Text;

            if (Login.Length == 0)
            {
                MessageBox.Show("Не заполнен логин!");
                return;
            }
            if (Password.Length == 0)
            {
                MessageBox.Show("Не заполнен пароль!");
                return;
            }

            if (LocalData.CurrentUser.login != Login)
            {
                MessageBox.Show("Вы пытаетесь зайти под другим пользователем!", "", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                return;
            }
            else if (LocalData.CurrentUser.password != Password)
            {
                MessageBox.Show("Неверный пароль!");
                return;
            }

            User user = await API.users_select_by_login_password(Login, Password);

            if (user == null)
            {
                if (API.IsLastRequestInternetError)
                {
                    MessageBox.Show("Ошибка соединения");
                }
                else
                {
                    MessageBox.Show("Ошибка входа");
                }
                return;
            }
            else
            {
                if (user.id_dolgnost != (LocalData.dolgnosti.FirstOrDefault(t => t.name == "Оператор")?.id ?? 0))
                {
                    MessageBox.Show("Выбранный пользователь не является оператором");
                    return;
                }

                //await API.users_update_start_work(user.id, LocalData.CurrentPoint_Work.id);
                MessageBox.Show("Вход выполнен успешно.");

                LocalData.CurrentUser = user;
            }

            Thread thr = new Thread(new ThreadStart(() =>
            {
                Application.Run(new PerformOrderForm());
            }));
            thr.SetApartmentState(ApartmentState.STA);
            thr.Start();
            this.Close();
        }

        private void maskedTextBoxPassword_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Return)
            {
                buttonEnter.PerformClick();
            }
        }

        private void FormPause_Shown(object sender, EventArgs e)
        {
            this.maskedTextBoxPassword.Focus();
        }
    }
}
