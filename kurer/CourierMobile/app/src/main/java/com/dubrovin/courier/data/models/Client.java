package com.dubrovin.courier.data.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by super on 14.01.2018.
 */

public class Client {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("number_phone")
    @Expose
    private String numberPhone;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("id_city")
    @Expose
    private String idCity;
    @SerializedName("id_rayon")
    @Expose
    private String idRayon;
    @SerializedName("id_street")
    @Expose
    private String idStreet;
    @SerializedName("number_home")
    @Expose
    private String numberHome;
    @SerializedName("number_corp")
    @Expose
    private String numberCorp;
    @SerializedName("number_build")
    @Expose
    private String numberBuild;
    @SerializedName("number_flat")
    @Expose
    private String numberFlat;
    @SerializedName("bd_day")
    @Expose
    private String bdDay;
    @SerializedName("bd_month")
    @Expose
    private String bdMonth;
    @SerializedName("number_card")
    @Expose
    private String numberCard;
    @SerializedName("id_advpoint")
    @Expose
    private String idAdvpoint;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNumberPhone() {
        return numberPhone;
    }

    public void setNumberPhone(String numberPhone) {
        this.numberPhone = numberPhone;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getIdCity() {
        return idCity;
    }

    public void setIdCity(String idCity) {
        this.idCity = idCity;
    }

    public String getIdRayon() {
        return idRayon;
    }

    public void setIdRayon(String idRayon) {
        this.idRayon = idRayon;
    }

    public String getIdStreet() {
        return idStreet;
    }

    public void setIdStreet(String idStreet) {
        this.idStreet = idStreet;
    }

    public String getNumberHome() {
        return numberHome;
    }

    public void setNumberHome(String numberHome) {
        this.numberHome = numberHome;
    }

    public String getNumberCorp() {
        return numberCorp;
    }

    public void setNumberCorp(String numberCorp) {
        this.numberCorp = numberCorp;
    }

    public String getNumberBuild() {
        return numberBuild;
    }

    public void setNumberBuild(String numberBuild) {
        this.numberBuild = numberBuild;
    }

    public String getNumberFlat() {
        return numberFlat;
    }

    public void setNumberFlat(String numberFlat) {
        this.numberFlat = numberFlat;
    }

    public String getBdDay() {
        return bdDay;
    }

    public void setBdDay(String bdDay) {
        this.bdDay = bdDay;
    }

    public String getBdMonth() {
        return bdMonth;
    }

    public void setBdMonth(String bdMonth) {
        this.bdMonth = bdMonth;
    }

    public String getNumberCard() {
        return numberCard;
    }

    public void setNumberCard(String numberCard) {
        this.numberCard = numberCard;
    }

    public String getIdAdvpoint() {
        return idAdvpoint;
    }

    public void setIdAdvpoint(String idAdvpoint) {
        this.idAdvpoint = idAdvpoint;
    }
}
