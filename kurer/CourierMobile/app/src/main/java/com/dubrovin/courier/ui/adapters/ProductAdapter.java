package com.dubrovin.courier.ui.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dubrovin.courier.R;
import com.dubrovin.courier.R;
import com.dubrovin.courier.data.models.Product;
import com.dubrovin.courier.ui.holders.ProductViewHolder;

import java.util.List;

/**
 * Created by super on 19.02.2018.
 */

public class ProductAdapter extends RecyclerView.Adapter<ProductViewHolder> {

    private List<Product> mProducts;


    public ProductAdapter(List<Product> products) {
        mProducts = products;
    }

    @Override
    public ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_product, parent, false);

        return new ProductViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ProductViewHolder holder, int position) {
        Product product = mProducts.get(position);
        holder.setName(product.getName());
        holder.setCount(product.getCount());
        holder.setInfo(product.getIngredients());
        holder.setPrice(product.getPrice());
    }

    @Override
    public int getItemCount() {
        return mProducts.size();
    }

    public void update()
    {
        notifyDataSetChanged();
    }

}
