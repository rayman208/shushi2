package com.dubrovin.courier;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;


import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;

import android.provider.Settings;
import android.support.annotation.Nullable;


import com.dubrovin.courier.common.constants.Commands;
import com.dubrovin.courier.common.constants.Config;


import com.dubrovin.courier.common.utils.GLog;
import com.dubrovin.courier.data.App;
import com.dubrovin.courier.data.eventbus.models.EventMessage;
import com.dubrovin.courier.data.models.AdvPoint;
import com.dubrovin.courier.data.models.City;
import com.dubrovin.courier.data.models.Order;

import com.dubrovin.courier.data.models.Rayon;
import com.dubrovin.courier.data.models.Status;
import com.dubrovin.courier.data.models.Street;
import com.dubrovin.courier.data.network.RetrofitSushi;
import com.dubrovin.courier.ui.activities.CurrentOrdersActivity;


import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.concurrent.TimeUnit;


import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Dubrovin on 15.12.2017.
 */

public class SushiService extends Service{

    private Thread mThread;
    private volatile boolean mFinish = false;
    private NotificationManager nm;
    final private int NOTIFICATION_ID = 124;

    @Override
    public void onCreate() {
        GLog.i("onCreate SushiService");
        super.onCreate();
        nm = (NotificationManager)getApplicationContext().getSystemService(NOTIFICATION_SERVICE);
        someTask();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void someTask() {
        mThread =  new Thread(new Runnable() {
            @Override
            public void run() {
                while (true)
                {
                    try
                    {
                        TimeUnit.SECONDS.sleep(10);
                    } catch (InterruptedException e){GLog.i("error");}


                    GLog.i("server is working");

                    try {
                        getAdvPoints();
                        getCities();
                        getStreets();
                        getRayons();
                        getListOrders();
                    }
                    catch (NullPointerException e)
                    {
                        e.printStackTrace();
                    }

                    if(mFinish) {
                        mThread.interrupt();
                         return;
                    }
                }
            }
        });

        mThread.start();
    }


    public void postInCurrentActivity()
    {
        GLog.i("eventBus_postInCurrentActivity");
        EventBus.getDefault().post(new EventMessage("newOrders"));
    }

    private void getCities() {
        final App app = (App)getApplicationContext();
        RetrofitSushi.getApi().getCity(Commands.commandCitiesSelectAll, Config.KEY).enqueue(new Callback<List<City>>() {
            @Override
            public void onResponse(Call<List<City>> call, final Response<List<City>> response) {
                try {
                    if(response.isSuccessful())
                    {
                        app.getCityList().addAll(response.body());
                    }
                }
                catch (NullPointerException e)
                {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<List<City>> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private void getRayons() {
        final App app = (App)getApplicationContext();
        RetrofitSushi.getApi().getRayon(Commands.commandRayonsSelectAll, Config.KEY).enqueue(new Callback<List<Rayon>>() {
            @Override
            public void onResponse(Call<List<Rayon>> call, final Response<List<Rayon>> response) {
                try {
                    if(response.isSuccessful())
                    {
                        app.getRayonList().addAll(response.body());
                    }
                }
                catch (NullPointerException e)
                {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<List<Rayon>> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private void getStreets() {
        final App app = (App)getApplicationContext();
        RetrofitSushi.getApi().getStreet(Commands.commandStreetsSelectAll, Config.KEY).enqueue(new Callback<List<Street>>() {
            @Override
            public void onResponse(Call<List<Street>> call, final Response<List<Street>> response) {
                try {
                    if(response.isSuccessful())
                    {
                        app.getStreetList().addAll(response.body());
                    }
                }
                catch (NullPointerException e)
                {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<List<Street>> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private void getAdvPoints() {
        final App app = (App)getApplicationContext();
        RetrofitSushi.getApi().getAdvPoint(Commands.commandAdvPointsSelectAll, Config.KEY).enqueue(new Callback<List<AdvPoint>>() {
            @Override
            public void onResponse(Call<List<AdvPoint>> call, final Response<List<AdvPoint>> response) {
                try {
                    if (response.isSuccessful()) {
                        app.getAdvPointList().addAll(response.body());
                    }
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<List<AdvPoint>> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    @Override
    public void onDestroy() {
        GLog.i("onDestroy SushiService");
        stopService(new Intent(getApplicationContext(), SushiService.class));
        mFinish = true;
            mThread.interrupt();
        super.onDestroy();
    }

    private void getListOrders() throws NullPointerException
    {

        final App app = (App) getApplicationContext();

        RetrofitSushi.getApi().getAllOrders(Commands.commandSelectCourierOrders,app.getChooseUser().getId(), Config.KEY).enqueue(new Callback<List<Order>>() {
            @Override
            public void onResponse(Call<List<Order>> call, Response<List<Order>> response) {

                if(response.isSuccessful())
                {
                    for (Order order : response.body()) {
                        app.AddOrderToQueue(app.getChooseUser().getId(),order.getId(),null,order.getDatetimeStart(),order.getDatetimeFinish(),null,null);
                    }

                    if(!response.body().isEmpty()){

                        Notification.Builder builder = new Notification.Builder(getApplicationContext());

                        app.getOrdersList().clear();
                        app.getOrdersList().addAll(response.body());
                        postInCurrentActivity();

                        Intent intent = new Intent(getApplicationContext(),CurrentOrdersActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(),0,intent,PendingIntent.FLAG_CANCEL_CURRENT);

                        GLog.i("SushiService Notification Build");

                        builder
                                .setContentIntent(pendingIntent)
                                .setSmallIcon(R.mipmap.ic_launcher)
                                .setTicker("КУРЬЕР, ВАЖНОЕ СООБЩЕНИЕ")
                                .setWhen(System.currentTimeMillis())
                                .setAutoCancel(true)
                                .setContentTitle("Курьер, Новые заказы!!!")
                                .setContentText("нажми на меня")
                                .setVibrate(new long[] { 1000, 1000})
                                .setSound(Settings.System.DEFAULT_NOTIFICATION_URI);

                        Notification notification= builder.build();

                        nm.notify(NOTIFICATION_ID,notification);
                    }


                }
            }
            @Override
            public void onFailure(Call<List<Order>> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }






}

