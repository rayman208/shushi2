package com.dubrovin.courier.common.constants;

/**
 * Created by Dubrovin on 15.12.2017.
 */

public class Commands {

    // enter in system
    public static final String commandLoginInSystem = "users.select.by_login_password";

    //return points of work
    public static final String commandGetPointWork = "points_work.select.all";

    //start work
    public static final String commandUsersUpdateStartWork = "users.update.start_work";

    //return orders
    public static final String commandSelectCourierOrders = "orders.select.kurer_orders";

    //add new order to queue
    public static final String commandCourierWorkInsertAddOrderToQueue = "kurers_work.insert.add_order_to_queue";

    //finish work
    public static final String commandUsersUpdateFinishWork  = "users.update.finish_work";

    //start new order
    public static final String commandCourierWorkUpdateOrderStart = "kurers_work.update.order_start";

    //start work
    public static final String commandOrdersUpdateOnWorkStart= "orders.update.on_work_start";

    //finish working order
    public static final String commandCourierWorkUpdateOrderFinish = "kurers_work.update.order_finish";

    //finish order
    public static final String commandOrdersUpdateFinishOrder = "orders.update.finish_order";

    //return list of products
    public static final String commandCitiesSelectAll = "cities.select.all";

    //return list of products
    public static final String commandRayonsSelectAll = "rayons.select.all";

    //return list of products
    public static final String commandAdvPointsSelectAll = "advpoints.select.all";

    //return list of products
    public static final String commandStreetsSelectAll = "streets.select.all";

    public static final String commandWriteDistance = "kurers_work.update.write_distance";


    public static final String commandGetClient = "clients.select.by_id";

    //return id products for order
    public static final String commandOrderProductsSelectByIdOrder = "order_products.select.by_id_order";

    //return list of products
    public static final String commandProductsSelectAll = "products.select.all";
}
