<?php 
require('config.php');

if(!$_SESSION["id"]) die('access denied');
$oid = xss($_GET['id']);
if(empty($oid)) die('null');
if(isset($_GET['dist']))
{
	$dist = xss($_GET['dist']);
	if(empty($dist)) die('Вы не указали дистанцию.');
	$finish_kurer_table = array(
		'command' => 'kurers_work.update.order_finish',
		'key' => API_KEY,
		'parameters' => $oid,
		'other' => $_SESSION["id"]
	);

	$ansK = json_decode(sendPost($finish_kurer_table), true);


	$finish_order = array(
		'command' => 'orders.update.finish_order',
		'parameters' => $oid,
		'key' => API_KEY,
		'other' => $_SESSION["id"]
	);

	$ansO = json_decode(sendPost($finish_order), true);

	$wr_dist_params = array(
		'id_order' => $oid,
		'distance' => $dist
	);

	$write_dist = array(
		'command' => 'kurers_work.update.write_distance',
		'key' => API_KEY,
		'parameters' => json_encode($wr_dist_params),
		
		'other' => $_SESSION["id"]

	);

	$ansD = json_decode(sendPost($write_dist), true);

	header('Location: /kurer/index.php');
}
else
{
	top('Завершение заказа');
	echo '<center><span id="" class="work_time">Завершение заказа</span></center>';
		echo '<center><input type="text" id="distance_num" name="login" placeholder="Введите пройденную дистанцию" class="login_input">';
		echo '<button class="login_button" onclick="finish_order('.$oid.')">Закончить выполнение</button></center>';
	footer();
}

?>