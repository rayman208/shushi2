<?php
require('config.php');

top("Главная");

if(xss(@$_GET['act']) == 'logout' AND isset($_SESSION["id"]))
{
	$update_data = array(

		'command' => 'users.update.finish_work',
		'parameters' => $_SESSION["id"], 
		'key' => API_KEY,
		'other' => ''

	);
	sendPost($update_data);
	session_destroy();
	header('Location: /kurer/index.php');
}


if(isset($_SESSION["id"]))
{
	echo '<center><a href="?act=logout" class="destroy_session">Закончить сессию</a></center>';
	?>
<script>

	window.setInterval(function(){
		location.reload();
	}, 10000);
	
	

</script>
<?php
	echo '<div class="page">';
	echo '<span class="big_title">Текущие заказы</span>';

	$select_orders_array = array(
	'command' => 'orders.select.kurer_orders',
	'key' => API_KEY,
	'parameters' => $_SESSION["id"],
	'other' => ''
	);

	$json_row = json_decode(sendPost($select_orders_array), true);

	if($json_row == null) echo '<br><span class="empty">Заказов нет</span>';
    
    $id_work_order = -1;
	for ($i=0; $i < count($json_row); $i++) 
	{ 
	    if($i==0) {$id_work_order=$json_row[$i]['id'];}
	    
			$parameters_array = array(
				'id_user' => $_SESSION["id"],
				'id_order' => $json_row[$i]["id"],
			);
			$json_array = array(
				'command' => 'kurers_work.insert.add_order_to_queue',
				'key' => API_KEY,
				'parameters' => json_encode($parameters_array)
			);

			$answer = json_decode(sendPost($json_array), true);
		

		echo '<div class="one_order_block" id="'.$json_row[$i]['id'].'">';
			echo "<br><br><span class='zakaz_info'>№ заказа: ".$json_row[$i]["id"].'</span><br>';
			echo "<span class='zakaz_info'>Дата заказа: ". date('Y-m-d H:i:s',$json_row[$i]["datetime_start"]).'</span><br>';
			echo "<span class='zakaz_info'>Цена: ".$json_row[$i]["price"].'</span><br>';
			echo "<span class='zakaz_info'>Кол-во персон: ".$json_row[$i]["count_persons"].'</span><br>';
// 			if($i == 0) echo "<span class='zakaz_info'><b style='color: #3e527a; display: inline-block;'>Выполняется!</b></span><br>";
// 			else echo "<span class='zakaz_info'><b style='color: #9599a1; display: inline-block;'>В очереди</b></span><br>";
// 		    if($i==0) echo '<a class="more" href="zakaz.php?id='.$json_row[$i]["id"].'">Начать выполнять!</a>';
		echo '</div>';
	}
	if($id_work_order != -1)
	{
	    //sleep(2);
	    
		$order_start = array(
		'command' => 'kurers_work.update.order_start',
		'key' => API_KEY,
		'parameters' => $id_work_order,
		'other' => $_SESSION["id"]
		);
		$answer = json_decode(sendPost($order_start), true);

		$order_start_onwork = array(
			'command' => 'orders.update.on_work_start',
			'key' => API_KEY,
			'parameters' => $id_work_order,
			'other' => $_SESSION["id"]
		);
    	$answer = json_decode(sendPost($order_start_onwork), true);
    	
        header('Location: /kurer/zakaz.php?id='.$id_work_order);
	}


	echo '</div>';
}
else
{
	echo "<center><p class='not_logged_in'>Вы не вошли. Пожалуйста, сделайте это</p></center><br>";

	if(!empty($_POST['login_sub']))
	{
		$login = xss($_POST['login']);
		$password = xss($_POST['password']);
		@$work_point = xss($_POST['work_point']);

		$json_array = array('login' => $login, 'password' => $password);
		$json_data = json_encode($json_array);

		$array = array(
			'command' => 'users.select.by_login_password',
			'key' => API_KEY,
			'parameters' => $json_data
		);

		$json_answer = sendPost($array);
		$json_row = json_decode($json_answer, true);
		if($json_row["id_dolgnost"] != 5) echo "<center><p class='login_error'>Ошибка при вводе данных</p></center>";
		else if($json_row == null) echo "<center><p class='login_error'>Ошибка при вводе данных</p></center>";
		else
		{
			if($json_row["login"] == $login AND $json_row['password'] == $password)
			{
				$update_data = array(

					'id' => $json_row["id"],
					'id_point_work' => $work_point,
				);
				start_work(json_encode($update_data));

				$_SESSION["id"] = $json_row["id"];
				$_SESSION["id_point_work"] = $json_row["id_point_work"];
				$_SESSION["name"] = $json_row["first_name"];
				$_SESSION["surname"] = $json_row["last_name"];
				$_SESSION["phone"] = $json_row["number_phone"];
				$_SESSION["rank"] = $json_row["id_dolgnost"];
				$_SESSION["pass"] = $json_row["passport"];
				header('Location: /kurer/index.php');

			}
			else
			{
				echo "<center><p class='login_error'>Ошибка при вводе данных</p></center>";
			}
		}
	}
	echo '<form method="post">';
		echo '<center><input type="text" name="login" placeholder="Логин" class="login_input">';
		echo '<input type="password" name="password" placeholder="Пароль" class="login_input">';
		echo '<select name="work_point" class="login_select">';
			$array_json_points = array('command' => 'points_work.select.all', 'key' => API_KEY, 'other' => '');
			$json_answer = json_decode( sendPost($array_json_points) , true);

			for ($i=0; $i < count($json_answer); $i++) { 
				echo '<option value="'.$json_answer[$i]["id"].'">'.$json_answer[$i]["name"].'</option>';
			}


		echo '</select>';
			

		
		echo '<input type="submit" name="login_sub" class="login_button"></center>';



	echo '</form>';
}


footer();
?>