﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SushiLib
{
    [Serializable]
    public class Product
    {
        public int id;
        public string name;
        public string ingredients;
        public int price;
        public override string ToString()
        {
            return name;
        }
        public Product()
        {

        }
    }
}
