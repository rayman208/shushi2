﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace SushiLib
{
    [Serializable]
    public class User
    {
        public int id;
        public string number_phone;
        public string login;
        public string password;
        public string first_name;
        public string last_name;
        public string passport;
        public int id_dolgnost;
        public int id_point_work;
        public int online;

        public override string ToString()
        {
            return id + ": " + last_name + " " + first_name.Substring(0, 1) + ".";
        }

        public User()
        {

        }
    }
}
