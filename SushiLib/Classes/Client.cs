﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SushiLib
{
    [Serializable]
    public class Client
    {
        public int id;
        public string first_name;
        public string last_name;
        public string number_phone;
        public int id_city;
        public int id_rayon;
        public int id_street;
        public int number_home;
        public int number_corp;
        public int number_build;
        public int number_flat;
        public int bd_day;
        public int bd_month;
        public string number_card;
        public int id_advpoint;
        public override string ToString()
        {
            return number_phone;
        }
        public Client()
        {

        }
    }
}
