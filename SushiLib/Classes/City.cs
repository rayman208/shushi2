﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SushiLib
{
    [Serializable]
    public class City
    {
        public int id;
        public string name;
        public override string ToString()
        {
            return name;
        }
        public City()
        {

        }
    }
}
