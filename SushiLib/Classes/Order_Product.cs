﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SushiLib
{
    [Serializable]
    public class Order_Product
    {
        public int id;
        public int id_order;
        public int id_product;
        public int count;
        public Order_Product()
        {

        }
    }
}
