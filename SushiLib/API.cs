﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace SushiLib
{
    public class StatusResult
    {
        public string status;
        public StatusResult()
        {
            status = string.Empty;
        }
    }
    public class API
    {
        private static JavaScriptSerializer serializer = new JavaScriptSerializer();

        public const string EmptyJSON = "{}";
        public const string ServerUrl = "http://nashisyshi.pluton-host.ru/api.php";
        public const string APIKey = "skdlfjhjw7834267bsbshudft673RBFDNB72R356VCSBX";

        public static bool IsLastRequestInternetError = false;

        public static async Task<string> ExecuteCommand(NameValueCollection parameters)
        {
            try
            {
                WebClient wc = new WebClient();
                parameters["key"] = APIKey;
                byte[] buffer = wc.UploadValues(ServerUrl, parameters);
                string Response = Encoding.UTF8.GetString(buffer);
                wc.Dispose();
                IsLastRequestInternetError = false;
                return Response;
            }
            catch
            {
                IsLastRequestInternetError = true;
                return "";
            }
        }

        public static async Task<bool> users_update_start_work(int id, int id_point_work)
        {
            NameValueCollection parameters = new NameValueCollection();
            parameters["command"] = "users.update.start_work";

            parameters["parameters"] = serializer.Serialize(new { id = id, id_point_work = id_point_work });

            string Response = await ExecuteCommand(parameters);
            if (Response == EmptyJSON)
            {
                return false;
            }

            StatusResult res = serializer.Deserialize<StatusResult>(Response);
            if (res.status == "ok")
            {
                return true;
            }
            else // res.status=="error"
            {
                return false;
            }
        }

        public static async Task<bool> users_update_finish_work(int id)
        {
            NameValueCollection parameters = new NameValueCollection();
            parameters["command"] = "users.update.finish_work";

            parameters["parameters"] = id.ToString();

            string Response = await ExecuteCommand(parameters);
            if (Response == EmptyJSON)
            {
                return false;
            }

            StatusResult res = serializer.Deserialize<StatusResult>(Response);
            if (res.status == "ok")
            {
                return true;
            }
            else // res.status=="error"
            {
                return false;
            }
        }

        public static async Task<User> users_select_by_login_password(string login, string password)
        {
            NameValueCollection parameters = new NameValueCollection();
            parameters["command"] = "users.select.by_login_password";

            parameters["parameters"] = serializer.Serialize(new { login = login, password = password });

            string Response = await ExecuteCommand(parameters);
            if (Response == EmptyJSON)
            {
                return null;
            }
            User user = serializer.Deserialize<User>(Response);
            return user;
        }

        public static async Task<int> clients_insert_new_client(string number_phone, string first_name, string last_name, string id_city, string id_rayon, string id_street, string number_home, string number_corp, string number_build, string number_flat, string bd_day, string bd_month, string number_card, string id_advpoint, string id_sotrudnik)
        {
            NameValueCollection parameters = new NameValueCollection();
            parameters["command"] = "clients.insert.new_client";

            parameters["parameters"] = serializer.Serialize(new { number_phone = number_phone, first_name = first_name, last_name = last_name, id_city = id_city, id_rayon = id_rayon, id_street = id_street, number_home = number_home, number_corp = number_corp, number_build = number_build, number_flat = number_flat, bd_day = bd_day, bd_month = bd_month, number_card = number_card, id_advpoint = id_advpoint });

            parameters["other"] = id_sotrudnik;

            string Response = await ExecuteCommand(parameters);
            if (Response == EmptyJSON)
            {
                return 0;
            }
            StatusResult res = serializer.Deserialize<StatusResult>(Response);
            if (res == null)
            {
                return 0;
            }
            else
            {
                int statusint = 0;
                int.TryParse(res.status, out statusint);
                return statusint;
            }
        }

        public static async Task<int> orders_insert_new_order(string id_operator, string id_povar, string id_upakov, string id_kurer, string id_client, string id_point_work, string price, string skidka, string price_with_skidka, string client_money, string count_persons, string description, string promocode, string no_cash, string operator_prodavec, string povar_upakov, string samovivoz, string id_order_state, string send_fiscal_data, string id_sotrudnik)
        {
            NameValueCollection parameters = new NameValueCollection();
            parameters["command"] = "orders.insert.new_order";

            parameters["parameters"] = serializer.Serialize(new
            {
                id_operator = id_operator,
                id_povar = id_povar,
                id_upakov = id_upakov,
                id_kurer = id_kurer,
                id_client = id_client,
                id_point_work = id_point_work,
                datetime_start = "-1",
                datetime_finish = "-1",
                duration = "-1",
                price = price,
                skidka = skidka,
                price_with_skidka = price_with_skidka,
                client_money = client_money,
                count_persons = count_persons,
                description = description,
                promocode = promocode,
                no_cash = no_cash,
                operator_prodavec = operator_prodavec,
                povar_upakov = povar_upakov,
                samovivoz = samovivoz,
                id_order_state = id_order_state,
                on_work = "0",
                send_fiscal_data = send_fiscal_data
            });

            parameters["other"] = id_sotrudnik;

            string Response = await ExecuteCommand(parameters);
            if (Response == EmptyJSON)
            {
                return 0;
            }
            StatusResult res = serializer.Deserialize<StatusResult>(Response);
            if (res == null)
            {
                return 0;
            }
            else
            {
                int statusint = 0;
                int.TryParse(res.status, out statusint);
                return statusint;
            }
        }

        public static async Task<bool> orders_update_choose_new_povar(int id, int id_povar, int id_sotrudnik)
        {
            NameValueCollection parameters = new NameValueCollection();
            parameters["command"] = "orders.update.choose_new_povar";

            parameters["parameters"] = serializer.Serialize(new { id = id, id_povar = id_povar });

            parameters["other"] = id_sotrudnik.ToString();

            string Response = await ExecuteCommand(parameters);
            if (Response == EmptyJSON)
            {
                return false;
            }

            StatusResult res = serializer.Deserialize<StatusResult>(Response);
            if (res.status == "ok")
            {
                return true;
            }
            else // res.status=="error"
            {
                return false;
            }
        }
        public static async Task<bool> orders_update_choose_new_upakov(int id, int id_upakov, int id_sotrudnik)
        {
            NameValueCollection parameters = new NameValueCollection();
            parameters["command"] = "orders.update.choose_new_upakov";

            parameters["parameters"] = serializer.Serialize(new { id = id, id_upakov = id_upakov });

            parameters["other"] = id_sotrudnik.ToString();

            string Response = await ExecuteCommand(parameters);
            if (Response == EmptyJSON)
            {
                return false;
            }

            StatusResult res = serializer.Deserialize<StatusResult>(Response);
            if (res.status == "ok")
            {
                return true;
            }
            else // res.status=="error"
            {
                return false;
            }
        }
        public static async Task<bool> orders_update_choose_new_kurer(int id, int id_kurer, int id_sotrudnik)
        {
            NameValueCollection parameters = new NameValueCollection();
            parameters["command"] = "orders.update.choose_new_kurer";

            parameters["parameters"] = serializer.Serialize(new { id = id, id_kurer = id_kurer });

            parameters["other"] = id_sotrudnik.ToString();

            string Response = await ExecuteCommand(parameters);
            if (Response == EmptyJSON)
            {
                return false;
            }

            StatusResult res = serializer.Deserialize<StatusResult>(Response);
            if (res.status == "ok")
            {
                return true;
            }
            else // res.status=="error"
            {
                return false;
            }
        }
        public static async Task<bool> orders_update_cancel_order(int id, int id_sotrudnik)
        {
            NameValueCollection parameters = new NameValueCollection();
            parameters["command"] = "orders.update.cancel_order";

            parameters["parameters"] = id.ToString();

            parameters["other"] = id_sotrudnik.ToString();

            string Response = await ExecuteCommand(parameters);
            if (Response == EmptyJSON)
            {
                return false;
            }

            StatusResult res = serializer.Deserialize<StatusResult>(Response);
            if (res.status == "ok")
            {
                return true;
            }
            else // res.status=="error"
            {
                return false;
            }
        }
        public static async Task<bool> orders_update_finish_order(int id, int id_sotrudnik)
        {
            NameValueCollection parameters = new NameValueCollection();
            parameters["command"] = "orders.update.finish_order";

            parameters["parameters"] = id.ToString();

            parameters["other"] = id_sotrudnik.ToString();

            string Response = await ExecuteCommand(parameters);
            if (Response == EmptyJSON)
            {
                return false;
            }

            StatusResult res = serializer.Deserialize<StatusResult>(Response);
            if (res.status == "ok")
            {
                return true;
            }
            else // res.status=="error"
            {
                return false;
            }
        }
        public static async Task<bool> orders_update_description(int id, string description, int id_sotrudnik)
        {
            NameValueCollection parameters = new NameValueCollection();
            parameters["command"] = "orders.update.description";

            parameters["parameters"] = serializer.Serialize(new { id = id, description = description });

            parameters["other"] = id_sotrudnik.ToString();

            string Response = await ExecuteCommand(parameters);
            if (Response == EmptyJSON)
            {
                return false;
            }

            StatusResult res = serializer.Deserialize<StatusResult>(Response);
            if (res.status == "ok")
            {
                return true;
            }
            else // res.status=="error"
            {
                return false;
            }
        }

        public static async Task<bool> clients_update_by_id(string id, string number_phone, string first_name, string last_name, string id_city, string id_rayon, string id_street, string number_home, string number_corp, string number_build, string number_flat, string bd_day, string bd_month, string number_card, string id_advpoint, string id_sotrudnik)
        {
            NameValueCollection parameters = new NameValueCollection();
            parameters["command"] = "clients.update.by_id";

            parameters["parameters"] = serializer.Serialize(new { id = id, number_phone = number_phone, first_name = first_name, last_name = last_name, id_city = id_city, id_rayon = id_rayon, id_street = id_street, number_home = number_home, number_corp = number_corp, number_build = number_build, number_flat = number_flat, bd_day = bd_day, bd_month = bd_month, number_card = number_card, id_advpoint = id_advpoint });

            parameters["other"] = id_sotrudnik;

            string Response = await ExecuteCommand(parameters);
            if (Response == EmptyJSON)
            {
                return false;
            }
            StatusResult res = serializer.Deserialize<StatusResult>(Response);
            if (res == null)
            {
                return false;
            }
            else
            {
                if (res.status == "ok")
                {
                    return true;
                }
                else // res.status=="error"
                {
                    return false;
                }
            }
        }

        public static async Task<bool> order_products_insert_new_order_products(List<Tuple<Product, int>> products, int id_order)
        {
            NameValueCollection parameters = new NameValueCollection();
            parameters["command"] = "order_products.insert.new_order_products";

            string productsstr = serializer.Serialize(products.Select(t => new { id_product = t.Item1.id, count = t.Item2 }));
            parameters["parameters"] = productsstr;

            parameters["other"] = id_order.ToString();

            string Response = await ExecuteCommand(parameters);
            if (Response == EmptyJSON)
            {
                return false;
            }
            StatusResult res = serializer.Deserialize<StatusResult>(Response);
            if (res == null)
            {
                return false;
            }
            else
            {
                if (res.status == "ok")
                {
                    return true;
                }
                else // res.status=="error"
                {
                    return false;
                }
            }
        }

        public static async Task<List<Order>> orders_select_last24h_orders()
        {
            NameValueCollection parameters = new NameValueCollection();
            parameters["command"] = "orders.select.last24h_orders";

            string Response = await ExecuteCommand(parameters);
            if (Response == EmptyJSON)
            {
                return new List<Order>();
            }

            List<Order> orders = serializer.Deserialize<List<Order>>(Response);
            return orders ?? new List<Order>();
        }

        public static async Task<List<Order_Product>> order_products_select_by_id_order(int id_order)
        {
            NameValueCollection parameters = new NameValueCollection();
            parameters["command"] = "order_products.select.by_id_order";
            parameters["parameters"] = id_order.ToString();

            string Response = await ExecuteCommand(parameters);
            if (Response == EmptyJSON)
            {
                return new List<Order_Product>();
            }

            List<Order_Product> order_products = serializer.Deserialize<List<Order_Product>>(Response);
            return order_products ?? new List<Order_Product>();
        }

        public static async Task<int> cities_insert_new_city(string id_sotrudnik, string name)
        {
            NameValueCollection parameters = new NameValueCollection();
            parameters["command"] = "cities.insert.new_city";
            parameters["parameters"] = serializer.Serialize(new { name = name });
            parameters["other"] = id_sotrudnik;
            string Response = await ExecuteCommand(parameters);
            if (Response == EmptyJSON)
            {
                return 0;
            }
            StatusResult res = serializer.Deserialize<StatusResult>(Response);
            if (res == null)
            {
                return 0;
            }
            else
            {
                int statusint = 0;
                int.TryParse(res.status, out statusint);
                return statusint;
            }
        }
        public static async Task<int> rayons_insert_new_rayon(string id_sotrudnik, string name)
        {
            NameValueCollection parameters = new NameValueCollection();
            parameters["command"] = "rayons.insert.new_rayon";
            parameters["parameters"] = serializer.Serialize(new { name = name });
            parameters["other"] = id_sotrudnik;
            string Response = await ExecuteCommand(parameters);
            if (Response == EmptyJSON)
            {
                return 0;
            }
            StatusResult res = serializer.Deserialize<StatusResult>(Response);
            if (res == null)
            {
                return 0;
            }
            else
            {
                int statusint = 0;
                int.TryParse(res.status, out statusint);
                return statusint;
            }
        }
        public static async Task<int> streets_insert_new_street(string id_sotrudnik, string name)
        {
            NameValueCollection parameters = new NameValueCollection();
            parameters["command"] = "streets.insert.new_street";
            parameters["parameters"] = serializer.Serialize(new { name = name });
            parameters["other"] = id_sotrudnik;
            string Response = await ExecuteCommand(parameters);
            if (Response == EmptyJSON)
            {
                return 0;
            }
            StatusResult res = serializer.Deserialize<StatusResult>(Response);
            if (res == null)
            {
                return 0;
            }
            else
            {
                int statusint = 0;
                int.TryParse(res.status, out statusint);
                return statusint;
            }
        }
        public static async Task<int> advpoints_insert_new_advpoint(string id_sotrudnik, string name)
        {
            NameValueCollection parameters = new NameValueCollection();
            parameters["command"] = "advpoints.insert.new_advpoint";
            parameters["parameters"] = serializer.Serialize(new { name = name });
            parameters["other"] = id_sotrudnik;
            string Response = await ExecuteCommand(parameters);
            if (Response == EmptyJSON)
            {
                return 0;
            }
            StatusResult res = serializer.Deserialize<StatusResult>(Response);
            if (res == null)
            {
                return 0;
            }
            else
            {
                int statusint = 0;
                int.TryParse(res.status, out statusint);
                return statusint;
            }
        }

        #region Dictionaries Select_ALL
        public static async Task<List<User>> users_select_all()
        {
            NameValueCollection parameters = new NameValueCollection();
            parameters["command"] = "users.select.all";

            string Response = await ExecuteCommand(parameters);
            if (Response == EmptyJSON)
            {
                return new List<User>();
            }

            List<User> users = serializer.Deserialize<List<User>>(Response);
            return users ?? new List<User>();
        }

        public static async Task<List<AdvPoint>> advpoints_select_all()
        {
            NameValueCollection parameters = new NameValueCollection();
            parameters["command"] = "advpoints.select.all";

            string Response = await ExecuteCommand(parameters);
            if (Response == EmptyJSON)
            {
                return new List<AdvPoint>();
            }

            List<AdvPoint> advPoints = serializer.Deserialize<List<AdvPoint>>(Response);
            return advPoints ?? new List<AdvPoint>();
        }

        public static async Task<List<City>> cities_select_all()
        {
            NameValueCollection parameters = new NameValueCollection();
            parameters["command"] = "cities.select.all";

            string Response = await ExecuteCommand(parameters);
            if (Response == EmptyJSON)
            {
                return new List<City>();
            }

            List<City> cities = serializer.Deserialize<List<City>>(Response);
            return cities ?? new List<City>();
        }

        public static async Task<List<Client>> clients_select_all()
        {
            NameValueCollection parameters = new NameValueCollection();
            parameters["command"] = "clients.select.all";

            string Response = await ExecuteCommand(parameters);
            if (Response == EmptyJSON)
            {
                return new List<Client>();
            }

            List<Client> clients = serializer.Deserialize<List<Client>>(Response);
            return clients ?? new List<Client>();
        }

        public static async Task<List<Dolgnost>> dolgnosti_select_all()
        {
            NameValueCollection parameters = new NameValueCollection();
            parameters["command"] = "dolgnosti.select.all";

            string Response = await ExecuteCommand(parameters);
            if (Response == EmptyJSON)
            {
                return new List<Dolgnost>();
            }

            List<Dolgnost> dolgnosti = serializer.Deserialize<List<Dolgnost>>(Response);
            return dolgnosti ?? new List<Dolgnost>();
        }

        public static async Task<List<Order_State>> order_states_select_all()
        {
            NameValueCollection parameters = new NameValueCollection();
            parameters["command"] = "order_states.select.all";

            string Response = await ExecuteCommand(parameters);
            if (Response == EmptyJSON)
            {
                return new List<Order_State>();
            }

            List<Order_State> order_states = serializer.Deserialize<List<Order_State>>(Response);
            return order_states ?? new List<Order_State>();
        }

        public static async Task<List<Point_Work>> points_work_select_all()
        {
            NameValueCollection parameters = new NameValueCollection();
            parameters["command"] = "points_work.select.all";

            string Response = await ExecuteCommand(parameters);
            if (Response == EmptyJSON)
            {
                return new List<Point_Work>();
            }

            List<Point_Work> points = serializer.Deserialize<List<Point_Work>>(Response);
            return points ?? new List<Point_Work>();
        }

        public static async Task<List<Product>> products_select_all()
        {
            NameValueCollection parameters = new NameValueCollection();
            parameters["command"] = "products.select.all";

            string Response = await ExecuteCommand(parameters);
            if (Response == EmptyJSON)
            {
                return new List<Product>();
            }

            List<Product> products = serializer.Deserialize<List<Product>>(Response);
            return products ?? new List<Product>();
        }

        public static async Task<List<Rayon>> rayons_select_all()
        {
            NameValueCollection parameters = new NameValueCollection();
            parameters["command"] = "rayons.select.all";

            string Response = await ExecuteCommand(parameters);
            if (Response == EmptyJSON)
            {
                return new List<Rayon>();
            }

            List<Rayon> rayons = serializer.Deserialize<List<Rayon>>(Response);
            return rayons ?? new List<Rayon>();
        }

        public static async Task<List<Street>> streets_select_all()
        {
            NameValueCollection parameters = new NameValueCollection();
            parameters["command"] = "streets.select.all";

            string Response = await ExecuteCommand(parameters);
            if (Response == EmptyJSON)
            {
                return new List<Street>();
            }

            List<Street> streets = serializer.Deserialize<List<Street>>(Response);
            return streets ?? new List<Street>();
        }
        #endregion
    }
}
