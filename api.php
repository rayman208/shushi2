<?php
$command = $_POST["command"];
$parameters = $_POST["parameters"];
$other = $_POST["other"];
$key = $_POST["key"];

if($key!="skdlfjhjw7834267bsbshudft673RBFDNB72R356VCSBX") {die("WRONG_API_KEY");}

//$mysqli = new mysqli("localhost", "root", "", "develop");
$mysqli = new mysqli('localhost', 'nashisys_itpark', 'itpark', 'nashisys_develop');


function InsertLog($mysqli, $id_user, $id_log_action, $id_order)
{
    $date = time();
    $mysqli->query("INSERT INTO `logs`(`datetime`, `id_user`, `id_log_action`, `id_order`) VALUES ('{$date}','{$id_user}','{$id_log_action}','{$id_order}');");
}

switch ($command)
{
    //===ORDERS START===//

    case "orders.insert.new_order":
        $parameters = str_replace("\\\"","\"",$parameters);
        
        $parameters = json_decode($parameters,true);

        $id_operator=$parameters["id_operator"];

        $id_povar=$parameters["id_povar"];
        $id_upakov=$parameters["id_upakov"];
        $id_kurer=$parameters["id_kurer"];

        $id_client=$parameters["id_client"];
        $id_point_work=$parameters["id_point_work"];

        $datetime_start=time();

        if($parameters["operator_prodavec"]==1)
        {
            $datetime_finish=time();
            $duration=0;
        }
        else
        {
            $datetime_finish = -1;
            $duration = -1;
        }
        $price=$parameters["price"];
        $skidka=$parameters["skidka"];
        $price_with_skidka=$parameters["price_with_skidka"];
        $client_money=$parameters["client_money"];
        $count_persons=$parameters["count_persons"];

        $description=$parameters["description"];
        $promocode=$parameters["promocode"];
        $no_cash=$parameters["no_cash"];

        $operator_prodavec=$parameters["operator_prodavec"];
        $povar_upakov=$parameters["povar_upakov"];
        $samovivoz=$parameters["samovivoz"];

        $id_order_state=$parameters["id_order_state"];
        $on_work=$parameters["on_work"];

        $send_fiscal_data=$parameters["send_fiscal_data"];

        $result = $mysqli->query("INSERT INTO `orders`(`id_operator`, `id_povar`, `id_upakov`, `id_kurer`, `id_client`, `id_point_work`, `datetime_start`, `datetime_finish`, `duration`, `price`, `skidka`, `price_with_skidka`, `client_money`, `count_persons`, `description`, `promocode`, `no_cash`, `operator_prodavec`, `povar_upakov`, `samovivoz`, `id_order_state`, `on_work`, `send_fiscal_data`) VALUES ('{$id_operator}','{$id_povar}','{$id_upakov}','{$id_kurer}','{$id_client}','{$id_point_work}','{$datetime_start}','{$datetime_finish}','{$duration}','{$price}','{$skidka}','{$price_with_skidka}','{$client_money}','{$count_persons}','{$description}','{$promocode}','{$no_cash}','{$operator_prodavec}','{$povar_upakov}','{$samovivoz}','{$id_order_state}','{$on_work}', '{$send_fiscal_data}');");
        if($result==true)
        {
            $id_user = $other;
            $id_order = $mysqli->insert_id;
            if($operator_prodavec==1)
            {
                InsertLog($mysqli,$id_user,1,$id_order);
                InsertLog($mysqli,$id_user,9,$id_order);
            }
            else
            {
                InsertLog($mysqli,$id_user,1,$id_order);
                InsertLog($mysqli,$id_user,2,$id_order);
            }
            echo json_encode(array("status" => $id_order), JSON_FORCE_OBJECT);
        }
        else
        {
            echo json_encode(array("status" => 0), JSON_FORCE_OBJECT);
        }



        break;

    case "orders.select.by_id":
        $id=$parameters;

        $result = $mysqli->query("SELECT * FROM `orders` WHERE `id`='{$id}'");
        $row = $result->fetch_assoc();

        if(mysqli_num_rows($result)>0)
        {
            echo json_encode($row, JSON_FORCE_OBJECT);
        }
        else
        {
            echo json_encode([], JSON_FORCE_OBJECT);
        }
        break;

    case "orders.select.povar_orders":
        $id_povar=$parameters;

        $result = $mysqli->query("SELECT * FROM `orders` WHERE `id_povar`='{$id_povar}' AND `id_order_state`=1");

        $data = array();
        while ($row = $result->fetch_assoc())
        {
            $data[] = $row;
        }
        echo json_encode($data);

        break;

    case "orders.select.upakov_orders":
        $id_upakov=$parameters;

        $result = $mysqli->query("SELECT * FROM `orders` WHERE `id_upakov`='{$id_upakov}' AND `id_order_state`=2");

        $data = array();
        while ($row = $result->fetch_assoc())
        {
            $data[] = $row;
        }
        echo json_encode($data);

        break;

    case "orders.select.kurer_orders":
        $id_kurer=$parameters;

        $result = $mysqli->query("SELECT * FROM `orders` WHERE `id_kurer`='{$id_kurer}' AND `id_order_state`=3");

        $data = array();
        while ($row = $result->fetch_assoc())
        {
            $data[] = $row;
        }
        echo json_encode($data);

        break;

    case "orders.select.last24h_orders":
        $datetime_finish = time();
        $secIn24h = 86400;
        $datetime_start = $datetime_finish-$secIn24h;

        $result = $mysqli->query("SELECT * FROM `orders` WHERE `datetime_start`>='{$datetime_start}' AND `datetime_start`<='{$datetime_finish}'");

        $data = array();
        while ($row = $result->fetch_assoc())
        {
            $data[] = $row;
        }
        echo json_encode($data);

        break;

    case "orders.select.dts_to_dtf_orders":
        $parameters = str_replace("\\\"","\"",$parameters);
        $parameters = json_decode($parameters,true);

        $datetime_start = $parameters["datetime_start"];
        $datetime_finish = $parameters["datetime_finish"];

        $result = $mysqli->query("SELECT * FROM `orders` WHERE `datetime_start`>='{$datetime_start}' AND `datetime_finish`<='{$datetime_finish}'");

        $data = array();
        while ($row = $result->fetch_assoc())
        {
            $data[] = $row;
        }
        echo json_encode($data);

        break;

    case "orders.select.on_work":
        $id=$parameters;

        $result = $mysqli->query("SELECT `on_work` FROM `orders` WHERE `id`='{$id}'");

        $row = $result->fetch_assoc();
        $on_work = $row["on_work"];

        if($on_work==1)
        {
            echo json_encode(array("status" => "true"), JSON_FORCE_OBJECT);
        }
        else
        {
            echo json_encode(array("status" => "false"), JSON_FORCE_OBJECT);
        }
        break;

    case "orders.update.on_work_start":
        $id=$parameters;

        $result = $mysqli->query("UPDATE `orders` SET `on_work`=1 WHERE `id`='{$id}'");

        if($result==true)
        {
            $id_user = $other;
            $id_order = $id;
            //InsertLog($mysqli,$id_user,12,$id_order);

            echo json_encode(array("status" => "ok"), JSON_FORCE_OBJECT);
        }
        else
        {
            echo json_encode(array("status" => "error"), JSON_FORCE_OBJECT);
        }
        break;

    case "orders.update.move_order_to_upakov":
        $id=$parameters;

        $mysqli->query("UPDATE `orders` SET `on_work`=0 WHERE `id`='{$id}'");
        $result = $mysqli->query("UPDATE `orders` SET `id_order_state`=2 WHERE `id`='{$id}'");

        if($result==true)
        {
            $id_user = $other;
            $id_order = $id;
            InsertLog($mysqli,$id_user,3,$id_order);

            echo json_encode(array("status" => "ok"), JSON_FORCE_OBJECT);
        }
        else
        {
            echo json_encode(array("status" => "error"), JSON_FORCE_OBJECT);
        }

        break;

    case "orders.update.move_order_to_kurer":
        $id=$parameters;

        $mysqli->query("UPDATE `orders` SET `on_work`=0 WHERE `id`='{$id}'");
        $result = $mysqli->query("UPDATE `orders` SET `id_order_state`=3 WHERE `id`='{$id}'");

        if($result==true)
        {
            $id_user = $other;
            $id_order = $id;
            InsertLog($mysqli,$id_user,4,$id_order);

            echo json_encode(array("status" => "ok"), JSON_FORCE_OBJECT);
        }
        else
        {
            echo json_encode(array("status" => "error"), JSON_FORCE_OBJECT);
        }

        break;

    case "orders.update.finish_order":
        $id=$parameters;

        $mysqli->query("UPDATE `orders` SET `on_work`=0 WHERE `id`='{$id}'");
        $result = $mysqli->query("SELECT  `datetime_start` FROM `orders` WHERE `id`='{$id}'");
        $row = $result->fetch_assoc();

        $datetime_start = $row["datetime_start"];
        $datetime_finish = time();
        $duration = $datetime_finish-$datetime_start;

        $result = $mysqli->query("UPDATE `orders` SET `id_order_state`=4, `datetime_finish`='{$datetime_finish}', `duration`='{$duration}' WHERE `id`='{$id}'");

        if($result==true)
        {
            $id_user = $other;
            $id_order = $id;
            InsertLog($mysqli,$id_user,9,$id_order);

            echo json_encode(array("status" => "ok"), JSON_FORCE_OBJECT);
        }
        else
        {
            echo json_encode(array("status" => "error"), JSON_FORCE_OBJECT);
        }

        break;

    case "orders.update.cancel_order":
        $id=$parameters;

        $mysqli->query("UPDATE `orders` SET `on_work`=0 WHERE `id`='{$id}'");
        $result = $mysqli->query("UPDATE `orders` SET `id_order_state`=5 WHERE `id`='{$id}'");

        if($result==true)
        {
            $id_user = $other;
            $id_order = $id;
            InsertLog($mysqli,$id_user,11,$id_order);

            echo json_encode(array("status" => "ok"), JSON_FORCE_OBJECT);
        }
        else
        {
            echo json_encode(array("status" => "error"), JSON_FORCE_OBJECT);
        }
        break;

    case "orders.update.choose_new_povar":
        $parameters = str_replace("\\\"","\"",$parameters);
        $parameters = json_decode($parameters,true);

        $id = $parameters["id"];
        $id_povar = $parameters["id_povar"];

        $result = $mysqli->query("UPDATE `orders` SET `id_povar`='{$id_povar}' WHERE `id`='{$id}'");

        if($result==true)
        {
            $id_user = $other;
            $id_order = $id;
            InsertLog($mysqli,$id_user,10,$id_order);

            echo json_encode(array("status" => "ok"), JSON_FORCE_OBJECT);
        }
        else
        {
            echo json_encode(array("status" => "error"), JSON_FORCE_OBJECT);
        }
        break;

    case "orders.update.choose_new_upakov":
        $parameters = str_replace("\\\"","\"",$parameters);
        $parameters = json_decode($parameters,true);

        $id = $parameters["id"];
        $id_upakov = $parameters["id_upakov"];

        $result = $mysqli->query("UPDATE `orders` SET `id_upakov`='{$id_upakov}' WHERE `id`='{$id}'");

        if($result==true)
        {
            $id_user = $other;
            $id_order = $id;
            InsertLog($mysqli,$id_user,10,$id_order);

            echo json_encode(array("status" => "ok"), JSON_FORCE_OBJECT);
        }
        else
        {
            echo json_encode(array("status" => "error"), JSON_FORCE_OBJECT);
        }
        break;

    case "orders.update.choose_new_kurer":
        $parameters = str_replace("\\\"","\"",$parameters);
        $parameters = json_decode($parameters,true);

        $id = $parameters["id"];
        $id_kurer = $parameters["id_kurer"];

        $result = $mysqli->query("UPDATE `orders` SET `id_kurer`='{$id_kurer}' WHERE `id`='{$id}'");

        if($result==true)
        {
            $id_user = $other;
            $id_order = $id;
            InsertLog($mysqli,$id_user,10,$id_order);

            echo json_encode(array("status" => "ok"), JSON_FORCE_OBJECT);
        }
        else
        {
            echo json_encode(array("status" => "error"), JSON_FORCE_OBJECT);
        }
        break;
        
    case "orders.update.description":
        $parameters = str_replace("\\\"","\"",$parameters);
        
        $parameters = json_decode($parameters,true);

        $id = $parameters["id"];
        $description = $parameters["description"];

        $result = $mysqli->query("UPDATE `orders` SET `description`='{$description}' WHERE `id`='{$id}'");

        if($result==true)
        {
            $id_user = $other;
            $id_order = $id;
            InsertLog($mysqli,$id_user,10,$id_order);

            echo json_encode(array("status" => "ok"), JSON_FORCE_OBJECT);
        }
        else
        {
            echo json_encode(array("status" => "error"), JSON_FORCE_OBJECT);
        }
        break;
    //===ORDERS FINISH===//

    //-------------------//
    //-------------------//
    //-------------------//

    //===CLIENTS START===//
    case "clients.insert.new_client":
        $parameters = str_replace("\\\"","\"",$parameters);
        $parameters = json_decode($parameters,true);

        $number_phone=$parameters["number_phone"];

        $first_name=$parameters["first_name"];
        $last_name=$parameters["last_name"];

        $id_city=$parameters["id_city"];
        $id_rayon=$parameters["id_rayon"];
        $id_street=$parameters["id_street"];

        $number_home=$parameters["number_home"];
        $number_corp=$parameters["number_corp"];
        $number_build=$parameters["number_build"];
        $number_flat=$parameters["number_flat"];

        $bd_day=$parameters["bd_day"];
        $bd_month=$parameters["bd_month"];

        $number_card=$parameters["number_card"];
        $id_advpoint=$parameters["id_advpoint"];

        $result = $mysqli->query("INSERT INTO `clients`(`number_phone`, `first_name`, `last_name`, `id_city`, `id_rayon`, `id_street`, `number_home`, `number_corp`, `number_build`, `number_flat`, `bd_day`, `bd_month`, `number_card`, `id_advpoint`) VALUES ('{$number_phone}','{$first_name}','{$last_name}','{$id_city}','{$id_rayon}','{$id_street}','{$number_home}','{$number_corp}','{$number_build}','{$number_flat}','{$bd_day}','{$bd_month}','{$number_card}','{$id_advpoint}')");

        if($result==true)
        {
            $id_user = $other;
            $id_order = -1;
            $id_client = $mysqli->insert_id;
            InsertLog($mysqli,$id_user,19,$id_order);

            echo json_encode(array("status" => $id_client), JSON_FORCE_OBJECT);
        }
        else
        {
            echo json_encode(array("status" => 0), JSON_FORCE_OBJECT);
        }
        break;

    case "clients.select.by_id":
        $id=$parameters;

        $result = $mysqli->query("SELECT * FROM `clients` WHERE `id`='{$id}'");
        $row = $result->fetch_assoc();

        if(mysqli_num_rows($result)>0)
        {
            echo json_encode($row, JSON_FORCE_OBJECT);
        }
        else
        {
            echo json_encode([], JSON_FORCE_OBJECT);
        }
        break;

    case "clients.select.all":
        $result = $mysqli->query("SELECT * FROM `clients`");

        $data = array();
        while ($row = $result->fetch_assoc())
        {
            $data[] = $row;
        }
        echo json_encode($data);

        break;

    case "clients.update.by_id":
        $parameters = str_replace("\\\"","\"",$parameters);
        $parameters = json_decode($parameters,true);

        $id = $parameters["id"];
        $number_phone=$parameters["number_phone"];

        $first_name=$parameters["first_name"];
        $last_name=$parameters["last_name"];

        $id_city=$parameters["id_city"];
        $id_rayon=$parameters["id_rayon"];
        $id_street=$parameters["id_street"];

        $number_home=$parameters["number_home"];
        $number_corp=$parameters["number_corp"];
        $number_build=$parameters["number_build"];
        $number_flat=$parameters["number_flat"];

        $bd_day=$parameters["bd_day"];
        $bd_month=$parameters["bd_month"];

        $number_card=$parameters["number_card"];
        $id_advpoint=$parameters["id_advpoint"];

        $result = $mysqli->query("UPDATE `clients` SET `number_phone`='{$number_phone}',`first_name`='{$first_name}',`last_name`='{$last_name}',`id_city`='{$id_city}',`id_rayon`='{$id_rayon}',`id_street`='{$id_street}',`number_home`='{$number_home}',`number_corp`='{$number_corp}',`number_build`='{$number_build}',`number_flat`='{$number_flat}',`bd_day`='{$bd_day}',`bd_month`='{$bd_month}',`number_card`='{$number_card}',`id_advpoint`='{$id_advpoint}' WHERE `id`='{$id}'");

        if($result==true)
        {
            $id_user = $other;
            $id_order = -1;
            InsertLog($mysqli,$id_user,20,$id_order);

            echo json_encode(array("status" => "ok"), JSON_FORCE_OBJECT);
        }
        else
        {
            echo json_encode(array("status" => "error"), JSON_FORCE_OBJECT);
        }
        break;

    case "clients.delete.by_id":
        $id=$parameters;

        $result = $mysqli->query("DELETE FROM `clients` WHERE `id` = '{$id}'");

        if($result==true)
        {
            $id_user = $other;
            $id_order = -1;
            InsertLog($mysqli,$id_user,21,$id_order);

            echo json_encode(array("status" => "ok"), JSON_FORCE_OBJECT);
        }
        else
        {
            echo json_encode(array("status" => "error"), JSON_FORCE_OBJECT);
        }
        break;
    //===CLIENTS FINISH===//

    //-------------------//
    //-------------------//
    //-------------------//

    //===USERS START===//
    case "users.insert.new_user":
        $parameters = str_replace("\\\"","\"",$parameters);
        $parameters = json_decode($parameters,true);

        $number_phone=$parameters["number_phone"];

        $login=$parameters["login"];
        $password=$parameters["password"];

        $first_name=$parameters["first_name"];
        $last_name=$parameters["last_name"];

        $passport=$parameters["passport"];

        $id_dolgnost=$parameters["id_dolgnost"];
        $id_point_work=$parameters["id_point_work"];

        $online=$parameters["online"];

        $result = $mysqli->query("INSERT INTO `users`(`number_phone`, `login`, `password`, `first_name`, `last_name`, `passport`, `id_dolgnost`, `id_point_work`, `online`) VALUES ('{$number_phone}','{$login}','{$password}','{$first_name}','{$last_name}','{$passport}','{$id_dolgnost}','{$id_point_work}','{$online}')");

        if($result==true)
        {
            $id_user = $other;
            $id_order = -1;
            $insertId= $mysqli->insert_id;
            InsertLog($mysqli,$id_user,12,$id_order);

            
            echo json_encode(array("status" => $insertId), JSON_FORCE_OBJECT);
        }
        else
        {
            echo json_encode(array("status" => "error"), JSON_FORCE_OBJECT);
        }
        break;

    case "users.select.all":
        $result = $mysqli->query("SELECT * FROM `users`");

        $data = array();
        while ($row = $result->fetch_assoc())
        {
            $data[] = $row;
        }
        echo json_encode($data);

        break;
    case "users.select.by_login_password":
        $parameters = str_replace("\\\"","\"",$parameters);
        $parameters = json_decode($parameters,true);

        $login=$parameters["login"];
        $password=$parameters["password"];

        $result = $mysqli->query("SELECT * FROM `users` WHERE `login`='{$login}' AND `password`='{$password}'");
        $row = $result->fetch_assoc();

        if(mysqli_num_rows($result)>0)
        {
            echo json_encode($row, JSON_FORCE_OBJECT);
        }
        else
        {
            echo json_encode([], JSON_FORCE_OBJECT);
        }
        break;

    case "users.update.by_id":
        $parameters = str_replace("\\\"","\"",$parameters);
        $parameters = json_decode($parameters,true);

        $id=$parameters["id"];

        $number_phone=$parameters["number_phone"];

        $login=$parameters["login"];
        $password=$parameters["password"];

        $first_name=$parameters["first_name"];
        $last_name=$parameters["last_name"];

        $passport=$parameters["passport"];

        $id_dolgnost=$parameters["id_dolgnost"];
        $id_point_work=$parameters["id_point_work"];

        $online=$parameters["online"];

        $result = $mysqli->query("UPDATE `users` SET `number_phone`='{$number_phone}',`login`='{$login}',`password`='{$password}',`first_name`='{$first_name}',`last_name`='{$last_name}',`passport`='{$passport}',`id_dolgnost`='{$id_dolgnost}',`id_point_work`='{$id_point_work}',`online`='{$online}' WHERE `id`='{$id}'");

        if($result==true)
        {
            $id_user = $other;
            $id_order = -1;
            InsertLog($mysqli,$id_user,13,$id_order);

            echo json_encode(array("status" => "ok"), JSON_FORCE_OBJECT);
        }
        else
        {
            echo json_encode(array("status" => "error"), JSON_FORCE_OBJECT);
        }
        break;

        case "users.update.start_work":
        $parameters = str_replace("\\\"","\"",$parameters);
        $parameters = json_decode($parameters,true);

        $id=$parameters["id"];
        $id_point_work=$parameters["id_point_work"];
        $online=1;

        $result = $mysqli->query("UPDATE `users` SET `id_point_work`='{$id_point_work}',`online`='{$online}' WHERE `id`='{$id}'");

        if($result==true)
        {
            $id_user = $id;
            $id_order = -1;
            InsertLog($mysqli,$id_user,15,$id_order);

            echo json_encode(array("status" => "ok"), JSON_FORCE_OBJECT);
        }
        else
        {
            echo json_encode(array("status" => "error"), JSON_FORCE_OBJECT);
        }
        break;

    case "users.update.finish_work":

        $id=$parameters;
        $online=0;

        $result = $mysqli->query("UPDATE `users` SET `online`='{$online}' WHERE `id`='{$id}'");

        if($result==true)
        {
            $id_user = $id;
            $id_order = -1;
            InsertLog($mysqli,$id_user,18,$id_order);

            echo json_encode(array("status" => "ok"), JSON_FORCE_OBJECT);
        }
        else
        {
            echo json_encode(array("status" => "error"), JSON_FORCE_OBJECT);
        }
        break;

    case "users.delete.by_id":
        $id=$parameters;

        $result = $mysqli->query("SELECT FROM `orders` WHERE `id_povar` = '{$id}' OR `id_upakov`='{$id}' OR `id_kurer`='{$id}'");

        if(mysqli_num_rows($result)>0)
        {
            echo json_encode(array("status" => "error"), JSON_FORCE_OBJECT);
        }
        else
        {
            $result = $mysqli->query("DELETE FROM `users` WHERE `id` = '{$id}'");

            if ($result == true)
            {
                $id_user = $other;
                $id_order = -1;
                InsertLog($mysqli, $id_user, 14, $id_order);

                echo json_encode(array("status" => "ok"), JSON_FORCE_OBJECT);
            }
            else
            {
                echo json_encode(array("status" => "error"), JSON_FORCE_OBJECT);
            }
        }
        break;
    //===USERS FINISH===//

    //-------------------//
    //-------------------//
    //-------------------//

    //===PRODUCTS START===//
    case "products.insert.new_product":
        $parameters = str_replace("\\\"","\"",$parameters);
        $parameters = json_decode($parameters,true);

        $name=$parameters["name"];
        $ingredients=$parameters["ingredients"];
        $price=$parameters["price"];

        $result = $mysqli->query("INSERT INTO `products`(`name`, `ingredients`, `price`) VALUES ('{$name}','{$ingredients}','{$price}')");

        if($result==true)
        {
            $id_user = $other;
            $id_order = 0;
           //  InsertLog($mysqli,$id_user,0,$id_order);

           
            $insertId= $mysqli->insert_id;
            echo json_encode(array("status" => $insertId), JSON_FORCE_OBJECT);
        }
        else
        {
            echo json_encode(array("status" => "error"), JSON_FORCE_OBJECT);
        }
        break;

    case "products.select.all":
        $result = $mysqli->query("SELECT * FROM `products`");

        $data = array();
        while ($row = $result->fetch_assoc())
        {
            $data[] = $row;
        }
        echo json_encode($data);

        break;

    case "products.update.by_id":
        $parameters = str_replace("\\\"","\"",$parameters);
        $parameters = json_decode($parameters,true);

        $id=$parameters["id"];
        $name=$parameters["name"];
        $ingredients=$parameters["ingredients"];
        $price=$parameters["price"];

        $result = $mysqli->query("UPDATE `products` SET `name`='{$name}',`ingredients`='{$ingredients}',`price`='{$price}' WHERE `id`='{$id}'");

        if($result==true)
        {
            $id_user = $other;
            $id_order = 0;
           // InsertLog($mysqli,$id_user,0,$id_order);

            echo json_encode(array("status" => "ok"), JSON_FORCE_OBJECT);
        }
        else
        {
            echo json_encode(array("status" => "error"), JSON_FORCE_OBJECT);
        }
        break;
    case "products.delete.by_id":
        $id=$parameters;

        $result = $mysqli->query("DELETE FROM `products` WHERE `id` = '{$id}'");

        if($result==true)
        {
            $id_user = $other;
            $id_order = 0;
            //InsertLog($mysqli,$id_user,0,$id_order);

            echo json_encode(array("status" => "ok"), JSON_FORCE_OBJECT);
        }
        else
        {
            echo json_encode(array("status" => "error"), JSON_FORCE_OBJECT);
        }
        break;
    //===PRODUCTS FINISH===//


    //-------------------//
    //-------------------//
    //-------------------//

    //===ORDER_PRODUCTS START===//
    case "order_products.insert.new_order_products":
        $parameters = str_replace("\\\"","\"",$parameters);
        $parameters = json_decode($parameters,true);

        $id_order = $other;

        $count =count($parameters);
        $result=false;
        for($i=0;$i<$count;$i++)
        {
            $id_product = $parameters[$i]['id_product'];
            $count_product = $parameters[$i]['count'];

            $result = $mysqli->query("INSERT INTO `order_products`(`id_order`, `id_product`, `count`) VALUES ('{$id_order}','{$id_product}','{$count_product}')");

            if($result==false){break;}
        }

        if($result==true)
        {
            echo json_encode(array("status" => "ok"), JSON_FORCE_OBJECT);
        }
        else
        {
            echo json_encode(array("status" => "error"), JSON_FORCE_OBJECT);
        }
        break;

    case "order_products.select.all":
        $result = $mysqli->query("SELECT * FROM `order_products`");

        $data = array();
        while ($row = $result->fetch_assoc())
        {
            $data[] = $row;
        }
        echo json_encode($data);

        break;

    case "order_products.select.by_id_order":
        $id_order=$parameters;

        $result = $mysqli->query("SELECT * FROM `order_products` WHERE `id_order`='{$id_order}'");

        $data = array();
        while ($row = $result->fetch_assoc())
        {
            $data[] = $row;
        }
        echo json_encode($data);
        break;
    //===ORDER_PRODUCTS FINISH===//

    //-------------------//
    //-------------------//
    //-------------------//

    //===CITIES START===//
    case "cities.insert.new_city":
        $parameters = str_replace("\\\"","\"",$parameters);
        $parameters = json_decode($parameters,true);

        $name=$parameters["name"];

        $result = $mysqli->query("INSERT INTO `cities`(`name`) VALUES ('{$name}')");

        if($result==true)
        {
            $id_user = $other;
            $id_order = 0;
            //InsertLog($mysqli,$id_user,0,$id_order);

            $id_city=$mysqli->insert_id;
            echo json_encode(array("status" => $id_city), JSON_FORCE_OBJECT);
        }
        else
        {
            echo json_encode(array("status" => 0), JSON_FORCE_OBJECT);
        }
        break;

    case "cities.select.all":
        $result = $mysqli->query("SELECT * FROM `cities`");

        $data = array();
        while ($row = $result->fetch_assoc())
        {
            $data[] = $row;
        }
        echo json_encode($data);

        break;

    case "cities.update.by_id":
        $parameters = str_replace("\\\"","\"",$parameters);
        $parameters = json_decode($parameters,true);

        $id=$parameters["id"];
        $name=$parameters["name"];

        $result = $mysqli->query("UPDATE `cities` SET `name`='{$name}' WHERE `id`='{$id}'");

        if($result==true)
        {
            $id_user = $other;
            $id_order = 0;
        //    InsertLog($mysqli,$id_user,0,$id_order);

            echo json_encode(array("status" => "ok"), JSON_FORCE_OBJECT);
        }
        else
        {
            echo json_encode(array("status" => "error"), JSON_FORCE_OBJECT);
        }
        break;

    case "cities.delete.by_id":
        $id=$parameters;

        $result = $mysqli->query("DELETE FROM `cities` WHERE `id` = '{$id}'");

        if($result==true)
        {
            $id_user = $other;
            $id_order = 0;
           // InsertLog($mysqli,$id_user,0,$id_order);

            echo json_encode(array("status" => "ok"), JSON_FORCE_OBJECT);
        }
        else
        {
            echo json_encode(array("status" => "error"), JSON_FORCE_OBJECT);
        }
        break;
    //===CITIES FINISH===//

    //-------------------//
    //-------------------//
    //-------------------//

    //===RAYONS START===//
    case "rayons.insert.new_rayon":
        $parameters = str_replace("\\\"","\"",$parameters);
        $parameters = json_decode($parameters,true);

        $name=$parameters["name"];

        $result = $mysqli->query("INSERT INTO `rayons`(`name`) VALUES ('{$name}')");

        if($result==true)
        {
            $id_user = $other;
            $id_order = 0;
            //InsertLog($mysqli,$id_user,0,$id_order);

            $id_rayon=$mysqli->insert_id;
            echo json_encode(array("status" => $id_rayon), JSON_FORCE_OBJECT);
        }
        else
        {
            echo json_encode(array("status" => 0), JSON_FORCE_OBJECT);
        }
        break;

    case "rayons.select.all":
        $result = $mysqli->query("SELECT * FROM `rayons`");

        $data = array();
        while ($row = $result->fetch_assoc())
        {
            $data[] = $row;
        }
        echo json_encode($data);

        break;

    case "rayons.update.by_id":
        $parameters = str_replace("\\\"","\"",$parameters);
        $parameters = json_decode($parameters,true);

        $id=$parameters["id"];
        $name=$parameters["name"];

        $result = $mysqli->query("UPDATE `rayons` SET `name`='{$name}' WHERE `id`='{$id}'");

        if($result==true)
        {
            $id_user = $other;
            $id_order = 0;
            //InsertLog($mysqli,$id_user,0,$id_order);

            echo json_encode(array("status" => "ok"), JSON_FORCE_OBJECT);
        }
        else
        {
            echo json_encode(array("status" => "error"), JSON_FORCE_OBJECT);
        }
        break;

    case "rayons.delete.by_id":
        $id=$parameters;

        $result = $mysqli->query("DELETE FROM `rayons` WHERE `id` = '{$id}'");

        if($result==true)
        {
            $id_user = $other;
            $id_order = 0;
            //InsertLog($mysqli,$id_user,0,$id_order);

            echo json_encode(array("status" => "ok"), JSON_FORCE_OBJECT);
        }
        else
        {
            echo json_encode(array("status" => "error"), JSON_FORCE_OBJECT);
        }
        break;
    //===RAYONS FINISH===//

    //-------------------//
    //-------------------//
    //-------------------//

    //===STREETS START===//
    case "streets.insert.new_street":
        $parameters = str_replace("\\\"","\"",$parameters);
        $parameters = json_decode($parameters,true);

        $name=$parameters["name"];

        $result = $mysqli->query("INSERT INTO `streets`(`name`) VALUES ('{$name}')");

        if($result==true)
        {
            $id_user = $other;
            $id_order = 0;
            //InsertLog($mysqli,$id_user,0,$id_order);

            $id_street=$mysqli->insert_id;
            echo json_encode(array("status" => $id_street), JSON_FORCE_OBJECT);
        }
        else
        {
            echo json_encode(array("status" => 0), JSON_FORCE_OBJECT);
        }
        break;

    case "streets.select.all":
        $result = $mysqli->query("SELECT * FROM `streets`");

        $data = array();
        while ($row = $result->fetch_assoc())
        {
            $data[] = $row;
        }
        echo json_encode($data);

        break;

    case "streets.update.by_id":
        $parameters = str_replace("\\\"","\"",$parameters);
        $parameters = json_decode($parameters,true);

        $id=$parameters["id"];
        $name=$parameters["name"];

        $result = $mysqli->query("UPDATE `streets` SET `name`='{$name}' WHERE `id`='{$id}'");

        if($result==true)
        {
            $id_user = $other;
            $id_order = 0;
            //InsertLog($mysqli,$id_user,0,$id_order);

            echo json_encode(array("status" => "ok"), JSON_FORCE_OBJECT);
        }
        else
        {
            echo json_encode(array("status" => "error"), JSON_FORCE_OBJECT);
        }
        break;

    case "streets.delete.by_id":
        $id=$parameters;

        $result = $mysqli->query("DELETE FROM `streets` WHERE `id` = '{$id}'");

        if($result==true)
        {
            $id_user = $other;
            $id_order = 0;
            //InsertLog($mysqli,$id_user,0,$id_order);

            echo json_encode(array("status" => "ok"), JSON_FORCE_OBJECT);
        }
        else
        {
            echo json_encode(array("status" => "error"), JSON_FORCE_OBJECT);
        }
        break;
    //===STREETS FINISH===//

    //-------------------//
    //-------------------//
    //-------------------//

    //===ADVPOINTS START===//
    case "advpoints.insert.new_advpoint":
        $parameters = str_replace("\\\"","\"",$parameters);
        $parameters = json_decode($parameters,true);

        $name=$parameters["name"];

        $result = $mysqli->query("INSERT INTO `advpoints`(`name`) VALUES ('{$name}')");

        if($result==true)
        {
            $id_user = $other;
            $id_order = 0;
            //InsertLog($mysqli,$id_user,0,$id_order);

            $id_advpoint=$mysqli->insert_id;
            echo json_encode(array("status" => $id_advpoint), JSON_FORCE_OBJECT);
        }
        else
        {
            echo json_encode(array("status" => 0), JSON_FORCE_OBJECT);
        }
        break;

    case "advpoints.select.all":
        $result = $mysqli->query("SELECT * FROM `advpoints`");

        $data = array();
        while ($row = $result->fetch_assoc())
        {
            $data[] = $row;
        }
        echo json_encode($data);

        break;

    case "advpoints.update.by_id":
        $parameters = str_replace("\\\"","\"",$parameters);
        $parameters = json_decode($parameters,true);

        $id=$parameters["id"];
        $name=$parameters["name"];

        $result = $mysqli->query("UPDATE `advpoints` SET `name`='{$name}' WHERE `id`='{$id}'");

        if($result==true)
        {
            $id_user = $other;
            $id_order = 0;
//            InsertLog($mysqli,$id_user,0,$id_order);

            echo json_encode(array("status" => "ok"), JSON_FORCE_OBJECT);
        }
        else
        {
            echo json_encode(array("status" => "error"), JSON_FORCE_OBJECT);
        }
        break;

    case "advpoints.delete.by_id":
        $id=$parameters;

        $result = $mysqli->query("DELETE FROM `advpoints` WHERE `id` = '{$id}'");

        if($result==true)
        {
            $id_user = $other;
            $id_order = 0;
  //          InsertLog($mysqli,$id_user,0,$id_order);

            echo json_encode(array("status" => "ok"), JSON_FORCE_OBJECT);
        }
        else
        {
            echo json_encode(array("status" => "error"), JSON_FORCE_OBJECT);
        }
        break;
    //===ADVPOINTS FINISH===//

    //-------------------//
    //-------------------//
    //-------------------//

    //===POINTS_WORK START===//
    case "points_work.insert.new_point_work":
        $parameters = str_replace("\\\"","\"",$parameters);
        $parameters = json_decode($parameters,true);

        $name=$parameters["name"];

        $result = $mysqli->query("INSERT INTO `points_work`(`name`) VALUES ('{$name}')");

        if($result==true)
        {
            $id_user = $other;
            $id_order = 0;
//            InsertLog($mysqli,$id_user,0,$id_order);

            $insertId = $mysqli->insert_id;
            echo json_encode(array("status" => $insertId), JSON_FORCE_OBJECT);
        }
        else
        {
            echo json_encode(array("status" => "error"), JSON_FORCE_OBJECT);
        }
        break;

    case "points_work.select.all":
        $result = $mysqli->query("SELECT * FROM `points_work`");

        $data = array();
        while ($row = $result->fetch_assoc())
        {
            $data[] = $row;
        }
        echo json_encode($data);

        break;

    case "points_work.update.by_id":
        $parameters = str_replace("\\\"","\"",$parameters);
        $parameters = json_decode($parameters,true);

        $id=$parameters["id"];
        $name=$parameters["name"];

        $result = $mysqli->query("UPDATE `points_work` SET `name`='{$name}' WHERE `id`='{$id}'");

        if($result==true)
        {
            $id_user = $other;
            $id_order = 0;
  //          InsertLog($mysqli,$id_user,0,$id_order);

            echo json_encode(array("status" => "ok"), JSON_FORCE_OBJECT);
        }
        else
        {
            echo json_encode(array("status" => "error"), JSON_FORCE_OBJECT);
        }
        break;
    case "points_work.delete.by_id":
        $id=$parameters;

        $result = $mysqli->query("DELETE FROM `points_work` WHERE `id` = '{$id}'");

        if($result==true)
        {
            $id_user = $other;
            $id_order = 0;
    //        InsertLog($mysqli,$id_user,0,$id_order);

            echo json_encode(array("status" => "ok"), JSON_FORCE_OBJECT);
        }
        else
        {
            echo json_encode(array("status" => "error"), JSON_FORCE_OBJECT);
        }
        break;
    //===POINTS_WORK FINISH===//

    //-------------------//
    //-------------------//
    //-------------------//

    //===LOGS START===//
    case "logs.select.dts_to_dtf_logs":
        $parameters = str_replace("\\\"","\"",$parameters);
        $parameters = json_decode($parameters,true);

        $datetime_start = $parameters["datetime_start"];
        $datetime_finish = $parameters["datetime_finish"];

        $result = $mysqli->query("SELECT * FROM `logs` WHERE `datetime`>='{$datetime_start}' AND `datetime`<='{$datetime_finish}'");

        $data = array();
        while ($row = $result->fetch_assoc())
        {
            $data[] = $row;
        }
        echo json_encode($data);

        break;
    //===LOGS FINISH===//

    //-------------------//
    //-------------------//
    //-------------------//

    //===LOG_ACTIONS START===//
    case "log_actions.select.all":
        $result = $mysqli->query("SELECT * FROM `log_actions`");

        $data = array();
        while ($row = $result->fetch_assoc())
        {
            $data[] = $row;
        }
        echo json_encode($data);

        break;
    //===LOG_ACTIONS FINISH===//

    //-------------------//
    //-------------------//
    //-------------------//

    //===DOLGNOSTI START===//
    case "dolgnosti.select.all":
        $result = $mysqli->query("SELECT * FROM `dolgnosti`");

        $data = array();
        while ($row = $result->fetch_assoc())
        {
            $data[] = $row;
        }
        echo json_encode($data);

        break;
    //===DOLGNOSTI FINISH===//

    //-------------------//
    //-------------------//
    //-------------------//

    //===ORDER_STATES START===//
    case "order_states.select.all":
        $result = $mysqli->query("SELECT * FROM `order_states`");

        $data = array();
        while ($row = $result->fetch_assoc())
        {
            $data[] = $row;
        }
        echo json_encode($data);

        break;
    //===ORDER_STATES FINISH===//

    //-------------------//
    //-------------------//
    //-------------------//

    //===POVARS_WORK START===//
    case "povars_work.insert.add_order_to_queue":
        $parameters = str_replace("\\\"","\"",$parameters);
        $parameters = json_decode($parameters,true);

        $id_user = $parameters["id_user"];
        $id_order = $parameters["id_order"];

        $result = $mysqli->query("SELECT * FROM `povars_work` WHERE `id_order`='{$id_order}'");
        if(mysqli_num_rows($result)>0)
        {
            echo json_encode(array("status" => "already_exists"), JSON_FORCE_OBJECT);
        }
        else
        {
            $datetime_add = time();

            $datetime_start = -1;
            $datetime_finish = -1;
            $duration_add_start = -1;
            $duration_start_finish = -1;

            $result = $mysqli->query("INSERT INTO `povars_work`(`id_user`, `id_order`, `datetime_add`, `datetime_start`, `datetime_finish`, `duration_add_start`, `duration_start_finish`) VALUES ('{$id_user}','{$id_order}','{$datetime_add}','{$datetime_start}','{$datetime_finish}','{$duration_add_start}','{$duration_start_finish}')");

            if ($result == true)
            {
                InsertLog($mysqli, $id_user, 5, $id_order);

                echo json_encode(array("status" => "ok"), JSON_FORCE_OBJECT);
            }
            else
            {
                echo json_encode(array("status" => "error"), JSON_FORCE_OBJECT);
            }

        }
        break;

    case "povars_work.select.all":
        $result = $mysqli->query("SELECT * FROM `povars_work`");

        $data = array();
        while ($row = $result->fetch_assoc())
        {
            $data[] = $row;
        }
        echo json_encode($data);

        break;

    case "povars_work.update.order_start":
        $id_order = $parameters;

        $result = $mysqli->query("SELECT `datetime_add` FROM `povars_work` WHERE `id_order`='{$id_order}'");
        $row = $result->fetch_assoc();
        $datetime_add = $row["datetime_add"];
        $datetime_start = time();

        $duration_add_start = $datetime_start-$datetime_add;

        $mysqli->query("UPDATE `povars_work` SET `datetime_start`='{$datetime_start}',`duration_add_start`='{$duration_add_start}' WHERE `id_order`='{$id_order}'");

        if($result==true)
        {
            $id_user = $other;
            InsertLog($mysqli,$id_user,6,$id_order);

            echo json_encode(array("status" => "ok"), JSON_FORCE_OBJECT);
        }
        else
        {
            echo json_encode(array("status" => "error"), JSON_FORCE_OBJECT);
        }
        break;

    case "povars_work.update.order_finish":
        $id_order = $parameters;

        $result = $mysqli->query("SELECT `datetime_start` FROM `povars_work` WHERE `id_order`='{$id_order}'");
        $row = $result->fetch_assoc();
        $datetime_start = $row["datetime_start"];
        $datetime_finish = time();

        $duration_start_finish = $datetime_finish-$datetime_start;

        $mysqli->query("UPDATE `povars_work` SET `datetime_finish`='{$datetime_finish}',`duration_start_finish`='{$duration_start_finish}' WHERE `id_order`='{$id_order}'");

        if($result==true)
        {
            $id_user = $other;
            InsertLog($mysqli,$id_user,7,$id_order);

            echo json_encode(array("status" => "ok"), JSON_FORCE_OBJECT);
        }
        else
        {
            echo json_encode(array("status" => "error"), JSON_FORCE_OBJECT);
        }
        break;
    //===POVARS_WORK FINISH===//

    //-------------------//
    //-------------------//
    //-------------------//

    //===UPAKOVS_WORK START===//
    case "upakovs_work.insert.add_order_to_queue":
        $parameters = str_replace("\\\"","\"",$parameters);
        $parameters = json_decode($parameters,true);

        $id_user = $parameters["id_user"];
        $id_order = $parameters["id_order"];

        $result = $mysqli->query("SELECT * FROM `upakovs_work` WHERE `id_order`='{$id_order}'");
        if(mysqli_num_rows($result)>0)
        {
            echo json_encode(array("status" => "already_exists"), JSON_FORCE_OBJECT);
        }
        else
        {
            $datetime_add = time();

            $datetime_start = -1;
            $datetime_finish = -1;
            $duration_add_start = -1;
            $duration_start_finish = -1;

            $result = $mysqli->query("INSERT INTO `upakovs_work`(`id_user`, `id_order`, `datetime_add`, `datetime_start`, `datetime_finish`, `duration_add_start`, `duration_start_finish`) VALUES ('{$id_user}','{$id_order}','{$datetime_add}','{$datetime_start}','{$datetime_finish}','{$duration_add_start}','{$duration_start_finish}')");

            if ($result == true)
            {
                InsertLog($mysqli, $id_user, 5, $id_order);

                echo json_encode(array("status" => "ok"), JSON_FORCE_OBJECT);
            }
            else
            {
                echo json_encode(array("status" => "error"), JSON_FORCE_OBJECT);
            }
        }
        break;

    case "upakovs_work.select.all":
        $result = $mysqli->query("SELECT * FROM `upakovs_work`");

        $data = array();
        while ($row = $result->fetch_assoc())
        {
            $data[] = $row;
        }
        echo json_encode($data);

        break;

    case "upakovs_work.update.order_start":
        $id_order = $parameters;

        $result = $mysqli->query("SELECT `datetime_add` FROM `upakovs_work` WHERE `id_order`='{$id_order}'");
        $row = $result->fetch_assoc();
        $datetime_add = $row["datetime_add"];
        $datetime_start = time();

        $duration_add_start = $datetime_start-$datetime_add;

        $mysqli->query("UPDATE `upakovs_work` SET `datetime_start`='{$datetime_start}',`duration_add_start`='{$duration_add_start}' WHERE `id_order`='{$id_order}'");

        if($result==true)
        {
            $id_user = $other;
            InsertLog($mysqli,$id_user,6,$id_order);

            echo json_encode(array("status" => "ok"), JSON_FORCE_OBJECT);
        }
        else
        {
            echo json_encode(array("status" => "error"), JSON_FORCE_OBJECT);
        }
        break;

    case "upakovs_work.update.order_finish":
        $id_order = $parameters;

        $result = $mysqli->query("SELECT `datetime_start` FROM `upakovs_work` WHERE `id_order`='{$id_order}'");
        $row = $result->fetch_assoc();
        $datetime_start = $row["datetime_start"];
        $datetime_finish = time();

        $duration_start_finish = $datetime_finish-$datetime_start;

        $mysqli->query("UPDATE `upakovs_work` SET `datetime_finish`='{$datetime_finish}',`duration_start_finish`='{$duration_start_finish}' WHERE `id_order`='{$id_order}'");

        if($result==true)
        {
            $id_user = $other;
            InsertLog($mysqli,$id_user,7,$id_order);

            echo json_encode(array("status" => "ok"), JSON_FORCE_OBJECT);
        }
        else
        {
            echo json_encode(array("status" => "error"), JSON_FORCE_OBJECT);
        }
        break;
    //===UPAKOVS_WORK FINISH===//

    //-------------------//
    //-------------------//
    //-------------------//

    //===KURERS_WORK START===//
    case "kurers_work.insert.add_order_to_queue":
        $parameters = str_replace("\\\"","\"",$parameters);
        $parameters = json_decode($parameters,true);

        $id_user = $parameters["id_user"];
        $id_order = $parameters["id_order"];

        $result = $mysqli->query("SELECT * FROM `kurers_work` WHERE `id_order`='{$id_order}'");
        if(mysqli_num_rows($result)>0)
        {
            echo json_encode(array("status" => "already_exists"), JSON_FORCE_OBJECT);
        }
        else
        {

            $datetime_add = time();

            $datetime_start = -1;
            $datetime_finish = -1;
            $duration_add_start = -1;
            $duration_start_finish = -1;
            $distance = -1;

            $result = $mysqli->query("INSERT INTO `kurers_work`(`id_user`, `id_order`, `datetime_add`, `datetime_start`, `datetime_finish`, `duration_add_start`, `duration_start_finish`, `distance`) VALUES ('{$id_user}','{$id_order}','{$datetime_add}','{$datetime_start}','{$datetime_finish}','{$duration_add_start}','{$duration_start_finish}','{$distance}')");

            if ($result == true)
            {
                InsertLog($mysqli, $id_user, 5, $id_order);

                echo json_encode(array("status" => "ok"), JSON_FORCE_OBJECT);
            }
            else
            {
                echo json_encode(array("status" => "error"), JSON_FORCE_OBJECT);
            }

        }
        break;

    case "kurers_work.select.all":
        $result = $mysqli->query("SELECT * FROM `kurers_work`");

        $data = array();
        while ($row = $result->fetch_assoc())
        {
            $data[] = $row;
        }
        echo json_encode($data);

        break;

    case "kurers_work.update.order_start":
        $id_order = $parameters;

        $result = $mysqli->query("SELECT `datetime_add` FROM `kurers_work` WHERE `id_order`='{$id_order}'");
        $row = $result->fetch_assoc();
        $datetime_add = $row["datetime_add"];
        $datetime_start = time();

        $duration_add_start = $datetime_start-$datetime_add;

        $mysqli->query("UPDATE `kurers_work` SET `datetime_start`='{$datetime_start}',`duration_add_start`='{$duration_add_start}' WHERE `id_order`='{$id_order}'");

        if($result==true)
        {
            $id_user = $other;
            InsertLog($mysqli,$id_user,6,$id_order);

            echo json_encode(array("status" => "ok"), JSON_FORCE_OBJECT);
        }
        else
        {
            echo json_encode(array("status" => "error"), JSON_FORCE_OBJECT);
        }
        break;

    case "kurers_work.update.order_finish":
        $id_order = $parameters;

        $result = $mysqli->query("SELECT `datetime_start` FROM `kurers_work` WHERE `id_order`='{$id_order}'");
        $row = $result->fetch_assoc();
        $datetime_start = $row["datetime_start"];
        $datetime_finish = time();

        $duration_start_finish = $datetime_finish-$datetime_start;

        $mysqli->query("UPDATE `kurers_work` SET `datetime_finish`='{$datetime_finish}',`duration_start_finish`='{$duration_start_finish}' WHERE `id_order`='{$id_order}'");

        if($result==true)
        {
            $id_user = $other;
            InsertLog($mysqli,$id_user,7,$id_order);

            echo json_encode(array("status" => "ok"), JSON_FORCE_OBJECT);
        }
        else
        {
            echo json_encode(array("status" => "error"), JSON_FORCE_OBJECT);
        }
        break;

    case "kurers_work.update.write_distance":
        $parameters = str_replace("\\\"","\"",$parameters);
        $parameters = json_decode($parameters,true);

        $id_order = $parameters["id_order"];
        $distance = $parameters["distance"];

        $result= $mysqli->query("UPDATE `kurers_work` SET `distance`='{$distance}' WHERE `id_order`='{$id_order}'");

        if($result==true)
        {
            $id_user = $other;
            //InsertLog($mysqli,$id_user,0,$id_order);

            echo json_encode(array("status" => "ok"), JSON_FORCE_OBJECT);
        }
        else
        {
            echo json_encode(array("status" => "error"), JSON_FORCE_OBJECT);
        }
        break;
    //===KURERS_WORK FINISH===//
}
?>