package com.dubrovin.packer.ui.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.dubrovin.packer.R;

/**
 * Created by super on 19.02.2018.
 */

public class ProductViewHolder extends RecyclerView.ViewHolder {


    private TextView mTextViewName;
    private TextView mTextViewCount;
    private TextView mTextViewInfo;
    private TextView mTextViewPrice;


    public ProductViewHolder(View itemView) {
        super(itemView);
        mTextViewName = itemView.findViewById(R.id.name_product2);
        mTextViewPrice = itemView.findViewById(R.id.price_product2);
        mTextViewCount = itemView.findViewById(R.id.count_product2);
        mTextViewInfo = itemView.findViewById(R.id.info_product2);
    }


    public void setName(String name)
    {
        mTextViewName.setText(name);
    }

    public void setPrice(String price)
    {
        mTextViewPrice.setText(price);
    }

    public void setCount(String count)
    {
        mTextViewCount.setText(count);
    }

    public void setInfo(String info)
    {
        mTextViewInfo.setText(info);
    }



}
