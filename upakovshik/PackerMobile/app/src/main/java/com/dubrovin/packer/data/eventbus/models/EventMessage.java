package com.dubrovin.packer.data.eventbus.models;

/**
 * Created by super on 19.02.2018.
 */

public class EventMessage {

    private final String info;

    public EventMessage(String info) {
        this.info = info;
    }

    public String getInfo() {
        return info;
    }
}
