package com.dubrovin.packer.ui.activities;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.os.Vibrator;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dubrovin.packer.R;
import com.dubrovin.packer.common.constants.Commands;
import com.dubrovin.packer.common.constants.Config;
import com.dubrovin.packer.common.constants.ErrorsInfo;
import com.dubrovin.packer.common.utils.GLog;
import com.dubrovin.packer.data.App;
import com.dubrovin.packer.data.models.Product;
import com.dubrovin.packer.data.models.ProductId;
import com.dubrovin.packer.data.models.Status;
import com.dubrovin.packer.data.network.RetrofitSushi;
import com.dubrovin.packer.ui.adapters.ProductAdapter;
import com.wang.avi.AVLoadingIndicatorView;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Dubrovin on 15.12.2017.
 */

public class WorkingOrderActivity extends AppCompatActivity{

    private TextView mTextViewProducts;

    private ProductAdapter mProductAdapter;

    private List<Product> mProductList = new ArrayList<>();


    private TextView idOrder;
    private TextView priceOrder;
    private TextView countOrder;
    private TextView otherOrder;
    private TextView moreOther;


    private NotificationManager nm;


    private AVLoadingIndicatorView mAVLoadingIndicatorView;


    private RelativeLayout mRelativeLayout;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_working);

        GLog.i("onCreate WorkingOrderActivity");

        StartOrder();

        getIDsProducts();


        App app = (App)getApplicationContext();

        //region vibration and notification
        Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        // Vibrate for 500 milliseconds
        if (v != null) {
            v.vibrate(2000);
        }


        try
        {
            nm = (NotificationManager)getApplicationContext().getSystemService(NOTIFICATION_SERVICE);
            nm.cancelAll();
        }
        catch (Exception e){e.printStackTrace();}

        new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                nm = (NotificationManager)getApplicationContext().getSystemService(NOTIFICATION_SERVICE);
                nm.cancelAll();
            }
        }).start();
        //endregion


        mRelativeLayout = findViewById(R.id.relative_layout_working_activity_top_layout);

        Button buttonExit = findViewById(R.id.button_working_activity_exit);

        Chronometer chronometer = findViewById(R.id.chronometer_working_activity_time);

        mTextViewProducts = findViewById(R.id.text_view_working_activity_count_products);

       // mAVLoadingIndicatorView = findViewById(R.id.loader_working_activity_loading);

        RecyclerView recyclerViewProducts = findViewById(R.id.recycler_view_working_activity_products_list);


        idOrder = findViewById(R.id.text_view_working_activity_id_order2);

        otherOrder = findViewById(R.id.text_view_working_activity_other2);
        otherOrder.setMovementMethod(new ScrollingMovementMethod());

        moreOther = findViewById(R.id.text_view_working_activity_more2);
        priceOrder = findViewById(R.id.text_view_working_activity_price2);
        countOrder = findViewById(R.id.text_view_working_activity_count_person2);

        idOrder.setText(app.getChooseOrder().getId());
        priceOrder.setText(app.getChooseOrder().getPrice());
        otherOrder.setText(app.getChooseOrder().getDescription());
        countOrder.setText(app.getChooseOrder().getCountPersons());
        if(app.getChooseOrder().getSamovivoz().equals("1"))
            moreOther.setText("самовывоз");
        else
            moreOther.setText("");


        RecyclerView.LayoutManager manager = new LinearLayoutManager(this);
        recyclerViewProducts.setLayoutManager(manager);
        mProductAdapter = new ProductAdapter(mProductList);
        recyclerViewProducts.setAdapter(mProductAdapter);


        GLog.i(app.getChooseOrder().getId());


        // Chronometer
        chronometer.setBase(SystemClock.elapsedRealtime());
        chronometer.start();


        buttonExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FinishOrder();
            }
        });

    }


    private void FinishOrder()
    {

       final  App app = (App) getApplicationContext();

       loaderShow();
        RetrofitSushi.getApi().addLogUpdateOrderFinish(Commands.commandPackWorkUpdateOrderFinish,app.getChooseOrder().getId(), Config.KEY,app.getChooseUser().getId()).enqueue(new Callback<Status>() {
            @Override
            public void onResponse(Call<Status> call, Response<Status> response) {
                  if(response.isSuccessful() && response.body().getStatus().equals("ok"))
                  {
                      if(Integer.valueOf(app.getChooseOrder().getSamovivoz())==0)
                          RetrofitSushi.getApi().addLogUpdateMoveOrder(Commands.commandOrdersUpdateMoveOrderToKurer,app.getChooseOrder().getId(), Config.KEY,app.getChooseUser().getId()).enqueue(new Callback<Status>() {
                              @Override
                              public void onResponse(Call<Status> call, Response<Status> response) {
                                  if(response.isSuccessful() && response.body().getStatus().equals("ok"))
                                  {
                                      app.getOrdersList().clear();
                                      startActivity(new Intent(WorkingOrderActivity.this,CurrentOrdersActivity.class));
                                      Toasty.info(getApplicationContext(), "Работа над заказом закончена", Toast.LENGTH_SHORT,true).show();
                                  }
                                  else {
                                      loaderClose();
                                      finishActivity();
                                      Toasty.success(getApplicationContext(), ErrorsInfo.errorNotKnowing, Toast.LENGTH_SHORT, true).show();
                                  }


                              }
                              @Override
                              public void onFailure(Call<Status> call, Throwable t) {
                                  loaderClose();
                                  finishActivity();
                                  Toasty.success(getApplicationContext(), ErrorsInfo.errorNotKnowing, Toast.LENGTH_SHORT, true).show();
                              }
                          });
                      else
                          RetrofitSushi.getApi().addLogUpdateFinishOrder(Commands.commandOrdersUpdateFinishOrder,app.getChooseOrder().getId(), Config.KEY,app.getChooseUser().getId()).enqueue(new Callback<Status>() {
                              @Override
                              public void onResponse(Call<Status> call, Response<Status> response) {
                                  if(response.isSuccessful() && response.body().getStatus().equals("ok"))
                                  {
                                      app.getOrdersList().clear();
                                      startActivity(new Intent(WorkingOrderActivity.this,CurrentOrdersActivity.class));
                                      Toasty.info(getApplicationContext(), "Работа над заказом закончена", Toast.LENGTH_SHORT,true).show();
                                  }
                                  else {
                                      loaderClose();
                                      finishActivity();
                                      Toasty.success(getApplicationContext(), ErrorsInfo.errorNotKnowing, Toast.LENGTH_SHORT, true).show();
                                  }
                              }
                              @Override
                              public void onFailure(Call<Status> call, Throwable t) {
                                  t.printStackTrace();loaderClose();
                                  finishActivity();
                                  Toasty.success(getApplicationContext(), ErrorsInfo.errorNotKnowing, Toast.LENGTH_SHORT, true).show();
                              }
                          });
                  }
                  else
                  {
                      loaderClose();
                      finishActivity();
                      Toasty.success(getApplicationContext(), ErrorsInfo.errorNotKnowing, Toast.LENGTH_SHORT, true).show();
                  }
            }
            @Override
            public void onFailure(Call<Status> call, Throwable t) {
                t.printStackTrace();
                loaderClose();
                finishActivity();
                Toasty.success(getApplicationContext(), ErrorsInfo.errorNotKnowing, Toast.LENGTH_SHORT, true).show();
            }
        });



    }

    private void finishActivity()
    {
        Intent intent = new Intent(WorkingOrderActivity.this, CurrentOrdersActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }


    private void StartOrder()
    {
        final App app = (App) getApplicationContext();
        loaderShow();
        try {
            RetrofitSushi.getApi().addLogUpdateOrderStart(Commands.commandPackWorkUpdateOrderStart, app.getChooseOrder().getId(), Config.KEY, app.getChooseUser().getId()).enqueue(new Callback<Status>() {
                @Override
                public void onResponse(Call<Status> call, Response<Status> response) {
                    if(response.isSuccessful() && response.body().getStatus().equals("ok"))
                    {
                        StartWorkOrder();
                    }
                    else
                    {
                        loaderClose();
                        finishActivity();
                        Toasty.success(getApplicationContext(), ErrorsInfo.errorNotKnowing, Toast.LENGTH_SHORT, true).show();
                    }

                }

                @Override
                public void onFailure(Call<Status> call, Throwable t) {
                  t.printStackTrace();
                  Toasty.error(getApplicationContext(), ErrorsInfo.errorNotKnowing, Toast.LENGTH_SHORT, true).show();
                  finishActivity();
                  loaderClose();
                }
            });
        }
        catch (Exception e)
        {
            finishActivity();
            loaderClose();
            Toasty.warning(getApplicationContext(), ErrorsInfo.errorNotKnowing, Toast.LENGTH_SHORT, true).show();
        }
    }

    private void StartWorkOrder()
    {
        App app = (App)getApplicationContext();

        RetrofitSushi.getApi().addLogUpdateOrderStartWork(Commands.commandOrdersUpdateOnWorkStart,app.getChooseOrder().getId(), Config.KEY,app.getChooseUser().getId()).enqueue(new Callback<Status>() {
            @Override
            public void onResponse(Call<Status> call, Response<Status> response) {
               if(response.isSuccessful() && response.body().getStatus().equals("ok"))
               {
                   loaderClose();
                   Toasty.success(getApplicationContext(), "заказ начал выполняться", Toast.LENGTH_SHORT, true).show();
               }
               else
               {
                   loaderClose();
                   finishActivity();
                   Toasty.success(getApplicationContext(), ErrorsInfo.errorNotKnowing, Toast.LENGTH_SHORT, true).show();
               }
            }
            @Override
            public void onFailure(Call<Status> call, Throwable t) {

                loaderClose();
                finishActivity();
                Toasty.success(getApplicationContext(), ErrorsInfo.errorNotKnowing, Toast.LENGTH_SHORT, true).show();
            }
        });
    }

    private void getIDsProducts()
    {

        final App app = (App)getApplicationContext();

        RetrofitSushi.getApi().getProductsIds(Commands.commandOrderProductsSelectByIdOrder,app.getChooseOrder().getId(), Config.KEY).enqueue(new Callback<List<ProductId>>() {

            @Override
            public void onResponse(Call<List<ProductId>> call, Response<List<ProductId>> response) {
                if(response.isSuccessful() && response.body().size()>0) {

                    List<Product> newProducts = new ArrayList<>();

                    app.getProdutsIds().clear();
                    app.getProdutsIds().addAll(response.body());


                    GLog.i("getIDsProducts    " + response.body().size());


                    for (ProductId id : app.getProdutsIds()) {
                        for (Product product : app.getProductList())
                            if (product.getId().equals(id.getId_product())) {
                                product.setCount(id.getCount());
                                newProducts.add(product);
                            }
                    }


                    mProductList.clear();
                    mProductList.addAll(newProducts);

                    mProductAdapter.update();

                }
                    else
                    {
                        getProducts();
                    }

            }
            @Override
            public void onFailure(Call<List<ProductId>> call, Throwable t) {
              t.printStackTrace();
            }
        });
    }

    private void getProducts() {


        final App app = (App) getApplicationContext();

        RetrofitSushi.getApi().getProducts(Commands.commandProductsSelectAll, Config.KEY).enqueue(new Callback<List<Product>>() {
            @Override
            public void onResponse(Call<List<Product>> call, final Response<List<Product>> response) {
                try {
                    if(response.isSuccessful())
                    {

                        GLog.i("getProducts    "+response.body().size());

                        app.getProductList().clear();
                        app.getProductList().addAll(response.body());
                        getIDsProducts();


                    }

                    GLog.i("Error get Products");
                }
                catch (NullPointerException e)
                {
                    Toasty.warning(getApplicationContext(), ErrorsInfo.errorProducts, Toast.LENGTH_SHORT, true).show();
                }

            }

            @Override
            public void onFailure(Call<List<Product>> call, Throwable t) {
                Toasty.warning(getApplicationContext(), ErrorsInfo.errorProducts, Toast.LENGTH_SHORT, true).show();
            }
        });
    }



    //region loaders
    private void loaderClose()
    {
//        mAVLoadingIndicatorView.hide();
      //  mAVLoadingIndicatorView.setVisibility(View.GONE);
     //   mRelativeLayout.setVisibility(View.VISIBLE);
    }

    private void loaderShow()
    {
//        mAVLoadingIndicatorView.show();
     //   mAVLoadingIndicatorView.setVisibility(View.VISIBLE);
//        mRelativeLayout.setVisibility(View.GONE);
    }

    @Override
    public void onBackPressed() { }
    //endregion
}
