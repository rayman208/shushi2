package com.dubrovin.packer.ui.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dubrovin.packer.R;
import com.dubrovin.packer.data.models.Order;
import com.dubrovin.packer.ui.holders.OrderViewHolder;

import java.util.List;

/**
 * Created by super on 19.02.2018.
 */

public class OrderAdapter extends RecyclerView.Adapter<OrderViewHolder> {

    private List<Order> mOrders;

    public OrderAdapter(List<Order> orders) {
        mOrders = orders;
    }

    @Override
    public OrderViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_order, parent, false);

        return new OrderViewHolder(v);
    }

    @Override
    public void onBindViewHolder(OrderViewHolder holder, int position) {
        Order order = mOrders.get(position);
        holder.setId(order.getId());
        holder.setCount(order.getCountPersons());
        holder.setDescription(order.getDescription());
        holder.setPrice(order.getPrice());
        if (order.getSamovivoz().equals("1"))
        holder.setMore("самовывоз");
    }

    @Override
    public int getItemCount() {
        return mOrders.size();
    }

    public void update()
    {
        notifyDataSetChanged();
    }
}
