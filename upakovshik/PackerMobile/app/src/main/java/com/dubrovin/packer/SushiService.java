package com.dubrovin.packer;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;


import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;

import android.provider.Settings;
import android.support.annotation.Nullable;


import com.dubrovin.packer.common.constants.Commands;
import com.dubrovin.packer.common.constants.Config;


import com.dubrovin.packer.common.utils.GLog;
import com.dubrovin.packer.data.App;
import com.dubrovin.packer.data.eventbus.models.EventMessage;
import com.dubrovin.packer.data.models.Order;

import com.dubrovin.packer.data.network.RetrofitSushi;
import com.dubrovin.packer.ui.activities.CurrentOrdersActivity;


import org.greenrobot.eventbus.EventBus;

import java.util.List;
import java.util.concurrent.TimeUnit;


import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Dubrovin on 15.12.2017.
 */

public class SushiService extends Service{
    public SushiService() {
    }

    private Thread mThread;
    private volatile boolean mFinish = false;
   // private StorageClass mStorageClass = StorageClass.getInstance();
    private NotificationManager nm;
    final private int NOTIFICATION_ID = 124;

    @Override
    public void onCreate() {
        GLog.i("onCreate SushiService");
        super.onCreate();
         //   EventBus.getDefault().register(this);
        nm = (NotificationManager)getApplicationContext().getSystemService(NOTIFICATION_SERVICE);
        someTask();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void someTask() {

        final App app = (App)getApplicationContext();

        mThread =  new Thread(new Runnable() {
            @Override
            public void run() {
                while (true)
                {
                    try
                    {
                        TimeUnit.SECONDS.sleep(10);
                        GLog.i("Thread Service slep(10) zzz...............");
                    } catch (InterruptedException e){GLog.i("error");}
                    GLog.i(mFinish);
                    try {
                        app.getProducts();
                        getListOrders();
                    }
                    catch (NullPointerException e){}

                    if(mFinish) {
                        mThread.interrupt();
                         return;
                    }
                }
            }
        });

        mThread.start();
    }





   public void postInCurrentActivity()
   {
       GLog.i("eventBus_postInCurrentActivity");
       EventBus.getDefault().post(new EventMessage("newOrders"));
   }


    @Override
    public void onDestroy() {
        GLog.i("onDestroy SushiService");


       // EventBus.getDefault().unregister(this);

        this.stopSelf();

        stopService(new Intent(getApplicationContext(), SushiService.class));
        mFinish = true;
            mThread.interrupt();
        super.onDestroy();
    }

    private void getListOrders() throws NullPointerException
    {

        final App app = (App) getApplicationContext();

        RetrofitSushi.getApi().getAllOrders(Commands.commandSelectPackOrders,app.getChooseUser().getId(), Config.KEY).enqueue(new Callback<List<Order>>() {
            @Override
            public void onResponse(Call<List<Order>> call, Response<List<Order>> response) {

                if(response.isSuccessful())
                {
                    for (Order order : response.body()) {
                        app.AddOrderToQueue(app.getChooseUser().getId(),order.getId(),null,order.getDatetimeStart(),order.getDatetimeFinish(),null,null);
                    }

                    if(!response.body().isEmpty()){

                            Notification.Builder builder = new Notification.Builder(getApplicationContext());

                            app.getOrdersList().clear();
                            app.getOrdersList().addAll(response.body());
                            postInCurrentActivity();

                            Intent intent = new Intent(getApplicationContext(),CurrentOrdersActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(),0,intent,PendingIntent.FLAG_CANCEL_CURRENT);

                            GLog.i("SushiService Notification Build");

                            builder
                                    .setContentIntent(pendingIntent)
                                    .setSmallIcon(R.mipmap.ic_launcher)
                                    .setTicker("УПАКОВЩИК, ВАЖНОЕ СООБЩЕНИЕ")
                                    .setWhen(System.currentTimeMillis())
                                    .setAutoCancel(true)
                                    .setContentTitle("Упаковщик, Новые заказы!!!")
                                    .setContentText("нажми на меня")
                                    .setVibrate(new long[] { 1000, 1000})
                                    .setSound(Settings.System.DEFAULT_NOTIFICATION_URI);

                            Notification notification= builder.build();

                            nm.notify(NOTIFICATION_ID,notification);
                        }


                    }
                }
            @Override
            public void onFailure(Call<List<Order>> call, Throwable t) {
                   t.printStackTrace();
            }
        });
    }











}

