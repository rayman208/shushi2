package com.dubrovin.packer.data.network;

import android.support.annotation.NonNull;

import com.dubrovin.packer.common.constants.Config;
import com.dubrovin.packer.data.api.SushiApi;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Dubrovin on 15.12.2017.
 */

public class RetrofitSushi {
    private RetrofitSushi() {}

    @NonNull
    private static OkHttpClient buildClient() {
        return new OkHttpClient
                .Builder()
                .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                .build();
    }

    public static SushiApi getApi() {



        Retrofit retrofit = new Retrofit.Builder()
                .client(buildClient())
                .baseUrl(Config.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return retrofit.create(SushiApi.class);
    }
}
