package com.dubrovin.packer.data.api;

import com.dubrovin.packer.data.models.Order;
import com.dubrovin.packer.data.models.PointWork;
import com.dubrovin.packer.data.models.Product;
import com.dubrovin.packer.data.models.ProductId;
import com.dubrovin.packer.data.models.Status;
import com.dubrovin.packer.data.models.User;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by Dubrovin on 15.12.2017.
 */

public interface SushiApi {
    @FormUrlEncoded
    @POST("/api.php/")
    Call<User> signIn(@Field("command") String command, @Field("parameters") String parameters, @Field("key") String key);

    @FormUrlEncoded
    @POST("/api.php/")
    Call<List<PointWork>> getPointsWork(@Field("command") String command, @Field("key") String key);

    @FormUrlEncoded
    @POST("/api.php/")
    Call<Status> addLogUpdateStartWork(@Field("command") String command, @Field("parameters") String parameters, @Field("key") String key);

    @FormUrlEncoded
    @POST("/api.php/")
    Call<List<Product>> getProducts(@Field("command") String command, @Field("key") String key);

    @FormUrlEncoded
    @POST("/api.php/")
    Call<List<Order>> getAllOrders(@Field("command") String command, @Field("parameters") String parameters, @Field("key") String key);

    @FormUrlEncoded
    @POST("/api.php/")
    Call<Status> addOrderToQueue(@Field("command") String command, @Field("parameters") String parameters, @Field("key") String key);

    @FormUrlEncoded
    @POST("/api.php/")
    Call<Status> addLogUpdateFinishWork(@Field("command") String command, @Field("parameters") String parameters, @Field("key") String key);

    @FormUrlEncoded
    @POST("/api.php/")
    Call<Status> addLogUpdateOrderStart(@Field("command") String command, @Field("parameters") String parameters, @Field("key") String key,@Field("other") String other);

    @FormUrlEncoded
    @POST("/api.php/")
    Call<Status> addLogUpdateOrderFinish(@Field("command") String command, @Field("parameters") String parameters, @Field("key") String key,@Field("other") String other);

    @FormUrlEncoded
    @POST("/api.php/")
    Call<Status> addLogUpdateOrderStartWork(@Field("command") String command, @Field("parameters") String parameters, @Field("key") String key,@Field("other") String other);

    @FormUrlEncoded
    @POST("/api.php/")
    Call<List<ProductId>> getProductsIds(@Field("command") String command, @Field("parameters") String parameters, @Field("key") String key);

    @FormUrlEncoded
    @POST("/api.php/")
    Call<Status> addLogUpdateMoveOrder(@Field("command") String command, @Field("parameters") String parameters, @Field("key") String key,@Field("other") String other);

    @FormUrlEncoded
    @POST("/api.php/")
    Call<Status> addLogUpdateFinishOrder(@Field("command") String command, @Field("parameters") String parameters, @Field("key") String key,@Field("other") String other);
}
