package com.dubrovin.packer.ui.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.dubrovin.packer.R;

/**
 * Created by super on 19.02.2018.
 */

public class OrderViewHolder extends RecyclerView.ViewHolder {


    private TextView mTextViewId;
    private TextView mTextViewPrice;
    private TextView mTextViewCount;
    private TextView mTextViewMore;
    private TextView mTextViewDescription;


    public OrderViewHolder(View itemView) {
        super(itemView);
        mTextViewId = itemView.findViewById(R.id.order_id2);
        mTextViewPrice = itemView.findViewById(R.id.order_price2);
        mTextViewCount = itemView.findViewById(R.id.order_count_person2);
        mTextViewMore = itemView.findViewById(R.id.order_more2);
        mTextViewDescription = itemView.findViewById(R.id.order_description);
    }



    public void setId(String id)
    {
        mTextViewId.setText(id);
    }

    public void setPrice(String price)
    {
        mTextViewPrice.setText(price);
    }

    public void setCount(String count)
    {
        mTextViewCount.setText(count);
    }

    public void setMore(String more)
    {
        mTextViewMore.setText(more);
    }

    public void setDescription(String description)
    {
        mTextViewDescription.setText(description);
    }

}
