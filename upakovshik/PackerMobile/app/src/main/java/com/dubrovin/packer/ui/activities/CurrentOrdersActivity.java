package com.dubrovin.packer.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.dd.processbutton.iml.SubmitProcessButton;
import com.dubrovin.packer.R;
import com.dubrovin.packer.SushiService;
import com.dubrovin.packer.common.constants.Commands;
import com.dubrovin.packer.common.constants.Config;
import com.dubrovin.packer.common.constants.ErrorsInfo;

import com.dubrovin.packer.common.constants.TypeView;
import com.dubrovin.packer.common.utils.GLog;
import com.dubrovin.packer.data.App;
import com.dubrovin.packer.data.eventbus.models.EventMessage;
import com.dubrovin.packer.data.models.Order;
import com.dubrovin.packer.data.models.Status;
import com.dubrovin.packer.data.network.RetrofitSushi;

import com.dubrovin.packer.ui.adapters.OrderAdapter;
import com.wang.avi.AVLoadingIndicatorView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;


import java.util.List;

import java.util.concurrent.TimeUnit;

import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Dubrovin on 15.12.2017.
 */

public class CurrentOrdersActivity extends AppCompatActivity {

    private RecyclerView mRecyclerViewCurrentOrders;
    private OrderAdapter mOrderAdapter;
   // private StorageClass mStorageClass = StorageClass.getInstance();
    private AVLoadingIndicatorView mAVLoadingIndicatorViewRecyclerView;

    private Boolean isWorking = true;

    TextView textViewCurrentOrders;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_current_orders);


        EventBus.getDefault().register(this);


        final App app = (App)getApplicationContext();


        GLog.i("onCreate CurrentOrdersActivity");

        mAVLoadingIndicatorViewRecyclerView = findViewById(R.id.activity_current_orders_recycler_view_loader);
        mRecyclerViewCurrentOrders = findViewById(R.id.current_orders_current_orders_recycler_view);

        final SubmitProcessButton submitProcessButtonExitSession = findViewById(R.id.current_orders_close_session_button);


        textViewCurrentOrders = findViewById(R.id.current_orders_current_orders_text_view);


        startService(new Intent(CurrentOrdersActivity.this, SushiService.class));

        loadView();
        getAllOrders();


        mOrderAdapter = new OrderAdapter(app.getOrdersList());
        mRecyclerViewCurrentOrders.setAdapter(mOrderAdapter);
        mOrderAdapter.update();


       /* if(isWorking && mStorageClass.getOrdersList()!=null && !mStorageClass.getOrdersList().isEmpty() && mStorageClass.getOrdersList().get(0)!=null)
        {
            startOrder();
        }
        else
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (true)
                {
                    if(isWorking && mStorageClass.getOrdersList()!=null && !mStorageClass.getOrdersList().isEmpty())
                    {
                        GLog.i("Two Thread Before Handler");
                        uiHandler.sendEmptyMessage(1);
                        return;
                    }
                }
            }
        }).start();*/




        submitProcessButtonExitSession.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                FinishWork(app.getChooseUser().getId());
                app.getOrdersList().clear();
                stopService(new Intent(getApplicationContext(),SushiService.class));
                Toasty.success(getApplicationContext(),ErrorsInfo.successExitSystem,Toast.LENGTH_SHORT,true).show();
                Intent intent = new Intent(CurrentOrdersActivity.this,AuthActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });
    }

   /* Handler uiHandler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    loadView();
                    String str ="Текущие заказы : "+mStorageClass.getOrdersList().size();
                    textViewCurrentOrders.setText(str);
                    mOrderAdapte2 = new OrderAdapte2(mStorageClass.getOrdersList(), TypeView.ORDERS);
                    mRecyclerViewCurrentOrders.setAdapter(mOrderAdapte2);
                    mOrderAdapte2.notifyDataSetChanged();
                    startOrder();
                    return true;
            }
            return false;
        }
    });*/



    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(EventMessage event) {
        GLog.i("eventBus_onMessageEvent");
        if(event.getInfo().equals("newOrders"))
        {
            App app = (App)getApplicationContext();
            GLog.i("eventBus_onMessageEvent");
            String str ="Текущие заказы : "+app.getOrdersList().size();
            textViewCurrentOrders.setText(str);
            mOrderAdapter.update();
            startOrder();
        }
    }


    @Override
    public void onBackPressed() { }

    private void loadView()
    {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        mRecyclerViewCurrentOrders.setLayoutManager(linearLayoutManager);
        mRecyclerViewCurrentOrders.setItemAnimator(new DefaultItemAnimator());
    }

    private void getAllOrders()
    {

        final App app = (App)getApplicationContext();

        loaderRecycelrViewShow();
        try {
            RetrofitSushi.getApi().getAllOrders(Commands.commandSelectPackOrders, app.getChooseUser().getId(), Config.KEY).enqueue(new Callback<List<Order>>() {
                @Override
                public void onResponse(Call<List<Order>> call, Response<List<Order>> response) {

                    loaderRecycelrViewHide();
                    if (response.isSuccessful()) {
                        app.getOrdersList().clear();
                        app.getOrdersList().addAll(response.body());
                        String str ="Текущие заказы : "+app.getOrdersList().size();
                        textViewCurrentOrders.setText(str);
                        mOrderAdapter.update();
                        startOrder();

                        for (Order order : app.getOrdersList()) {
                            app.AddOrderToQueue(app.getChooseUser().getId(), order.getId(), null, order.getDatetimeStart(), order.getDatetimeFinish(), null, order.getDuration());
                        }
                    }
                }

                @Override
                public void onFailure(Call<List<Order>> call, Throwable t) {
                    loaderRecycelrViewHide();
                    t.printStackTrace();
                    Toasty.warning(getApplicationContext(), ErrorsInfo.errorNotKnowing, Toast.LENGTH_SHORT, true).show();
                }
            });
        }
        catch (Exception e){e.printStackTrace();Toasty.warning(getApplicationContext(), ErrorsInfo.errorNotKnowing, Toast.LENGTH_SHORT, true).show();}
    }



    private void FinishWork(String id)
    {
        RetrofitSushi.getApi().addLogUpdateFinishWork(Commands.commandUsersUpdateFinishWork,id,Config.KEY).enqueue(new Callback<Status>() {

            @Override
            public void onResponse(Call<Status> call, Response<Status> response) {

            }

            @Override
            public void onFailure(Call<Status> call, Throwable t) {
                t.printStackTrace();
                Toasty.warning(getApplicationContext(), ErrorsInfo.errorNotKnowing, Toast.LENGTH_SHORT, true).show();
            }
        });
    }



    @Override
    protected void onDestroy() {
        EventBus.getDefault().unregister(this);
        stopService(new Intent(CurrentOrdersActivity.this,SushiService.class));
        GLog.i("OnDestroy   currentOrder");
        isWorking = false;
        super.onDestroy();
    }

    private void loaderRecycelrViewHide()
    {
        mAVLoadingIndicatorViewRecyclerView.hide();
        mAVLoadingIndicatorViewRecyclerView.setVisibility(View.GONE);
        mRecyclerViewCurrentOrders.setVisibility(View.VISIBLE);
    }

    private void loaderRecycelrViewShow()
    {
        mAVLoadingIndicatorViewRecyclerView.show();
        mAVLoadingIndicatorViewRecyclerView.setVisibility(View.VISIBLE);
        mRecyclerViewCurrentOrders.setVisibility(View.GONE);
    }

    @Override
    protected void onResume() {
        GLog.i("onResume currentOrder");
        isWorking = true;
        super.onResume();
    }

    private void startOrder()
    {

        final App app = (App)getApplicationContext();

        if(!app.getOrdersList().isEmpty())
        {
            String str ="Текущие заказы : "+app.getOrdersList().size();
            textViewCurrentOrders.setText(str);
            Toasty.info(getApplicationContext(), "началось выполнение заказа", Toast.LENGTH_SHORT, true).show();
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        TimeUnit.SECONDS.sleep(3);
                        GLog.i("Thred Start sleep Current Orders Actitvity ");
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                        if(!app.getOrdersList().isEmpty() && isWorking)
                        {
                            app.setChooseOrder(app.getOrdersList().get(0));
                            Intent intent = new Intent(CurrentOrdersActivity.this, WorkingOrderActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                        }

                }
            }).start();
        }

    }

    @Override
    protected void onPause() {
        isWorking = false;
        super.onPause();
    }

    @Override
    protected void onStop() {
        isWorking = false;
        super.onStop();
    }
}
