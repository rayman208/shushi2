package com.dubrovin.packer.data;

import android.app.Application;
import android.widget.TextView;
import android.widget.Toast;

import com.dubrovin.packer.common.constants.Commands;
import com.dubrovin.packer.common.constants.Config;
import com.dubrovin.packer.common.constants.ErrorsInfo;
import com.dubrovin.packer.common.constants.TypeView;
import com.dubrovin.packer.data.models.Order;
import com.dubrovin.packer.data.models.PointWork;
import com.dubrovin.packer.data.models.Product;
import com.dubrovin.packer.data.models.ProductId;
import com.dubrovin.packer.data.models.Status;
import com.dubrovin.packer.data.models.User;
import com.dubrovin.packer.data.network.RetrofitSushi;
import com.dubrovin.packer.ui.adapters.OrderAdapter;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by super on 19.02.2018.
 */

public class App extends Application {


    private PointWork ChoosePointWork;
    private User ChooseUser;
    private Order ChooseOrder;
    private ProductId ChooseProductId;

    private List<Product> mProductList = new ArrayList<>();
    private List<Order> mOrdersList = new ArrayList<>();
    private List<ProductId> produtsIds = new ArrayList<>();

    private List<PointWork> mPointWorkList = new ArrayList<>();


    @Override
    public void onCreate() {
        super.onCreate();
    }


    public  void getProducts()
    {
        RetrofitSushi.getApi().getProducts(Commands.commandProductsSelectAll, Config.KEY).enqueue(new Callback<List<Product>>() {
            @Override
            public void onResponse(Call<List<Product>> call, final Response<List<Product>> response) {
                try {
                    if(response.isSuccessful())
                    {
                        mProductList.clear();
                        mProductList.addAll(response.body());
                    }
                }
                catch (NullPointerException e)
                {
                    e.printStackTrace();
                    Toasty.warning(getApplicationContext(), ErrorsInfo.errorProducts, Toast.LENGTH_SHORT, true).show();
                }

            }

            @Override
            public void onFailure(Call<List<Product>> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }


    public void AddOrderToQueue(String idUser,String idOrder,String dateTimeAdd,String dateTimeStart,String dateTimeFinish,String durationAddStart,String durationStartFinish)
    {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("id_user",idUser);
            jsonObject.put("id_order",idOrder);
           // jsonObject.put("datetime_add",dateTimeAdd);
            jsonObject.put("datetime_start",dateTimeStart);
            jsonObject.put("datetime_finish",dateTimeFinish);
           // jsonObject.put("duration_add_start",durationAddStart);
            jsonObject.put("duration_start_finish",durationStartFinish);
        } catch (JSONException e) {
            return;
        }

        RetrofitSushi.getApi().addOrderToQueue(Commands.commandPackWorkInsertAddOrderToQueue,jsonObject.toString(),Config.KEY).enqueue(new Callback<Status>() {

            @Override
            public void onResponse(Call<Status> call, Response<Status> response) {

            }

            @Override
            public void onFailure(Call<Status> call, Throwable t) {
                t.printStackTrace();
                Toasty.warning(getApplicationContext(), ErrorsInfo.errorNotKnowing, Toast.LENGTH_SHORT, true).show();
            }
        });
    }


    public void getOrders()
    {
        try {
            RetrofitSushi.getApi().getAllOrders(Commands.commandSelectPackOrders, ChooseUser.getId(), Config.KEY).enqueue(new Callback<List<Order>>() {
                @Override
                public void onResponse(Call<List<Order>> call, Response<List<Order>> response) {

                    if (response.isSuccessful()) {
                        mOrdersList.clear();
                        mOrdersList.addAll(response.body());

                        for (Order order : mOrdersList) {
                            AddOrderToQueue(ChooseUser.getId(), order.getId(), "", order.getDatetimeStart(), order.getDatetimeFinish(), "", order.getDuration());
                        }
                    }
                }

                @Override
                public void onFailure(Call<List<Order>> call, Throwable t) {
                    t.printStackTrace();
                    Toasty.warning(getApplicationContext(), ErrorsInfo.errorNotKnowing, Toast.LENGTH_SHORT, true).show();
                }
            });
        }
        catch (Exception e){e.printStackTrace();Toasty.warning(getApplicationContext(), ErrorsInfo.errorNotKnowing, Toast.LENGTH_SHORT, true).show();}
    }


   //region get/set
    public ProductId getChooseProductId() {
        return ChooseProductId;
    }

    public void setChooseProductId(ProductId chooseProductId) {
        ChooseProductId = chooseProductId;
    }

    public PointWork getChoosePointWork() {
        return ChoosePointWork;
    }

    public void setChoosePointWork(PointWork choosePointWork) {
        ChoosePointWork = choosePointWork;
    }

    public User getChooseUser() {
        return ChooseUser;
    }

    public void setChooseUser(User chooseUser) {
        ChooseUser = chooseUser;
    }

    public Order getChooseOrder() {
        return ChooseOrder;
    }

    public void setChooseOrder(Order chooseOrder) {
        ChooseOrder = chooseOrder;
    }

    public List<Product> getProductList() {
        return mProductList;
    }

    public void setProductList(List<Product> productList) {
        mProductList = productList;
    }

    public List<Order> getOrdersList() {
        return mOrdersList;
    }

    public void setOrdersList(List<Order> ordersList) {
        mOrdersList = ordersList;
    }

    public List<ProductId> getProdutsIds() {
        return produtsIds;
    }

    public void setProdutsIds(List<ProductId> produtsIds) {
        this.produtsIds = produtsIds;
    }

    public List<PointWork> getPointWorkList() {
        return mPointWorkList;
    }

    public void setPointWorkList(List<PointWork> pointWorkList) {
        mPointWorkList = pointWorkList;
    }

    //endregion
}
